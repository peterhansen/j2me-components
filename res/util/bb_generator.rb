##
# bb_generator.rb
# Gera um workspace e um projeto para o BlackBerry JDE 4.1 fazer o deploy de um aplicativo BlackBerry, funcionando em todas as versões de SO a partir da 4.1.
# Deve ser executado a partir do diretário DIST do projeto.
##

require 'find'
require 'fileutils'
require 'zip/zip'
require 'zip/zipfilesystem'

# nome da config no NetBeans
CONFIG_NAME = "blackberry"
# caminho absoluto dos componentes (já pré-processados pelo NetBeans)
PATH_COMPONENTS = "C:/projetos/j2me/components/current/build/blackberry/preprocessed"
# caminho absoluto da JDE
PATH_JDE = "C:/java/Research In Motion/BlackBerry JDE 4.1.0"
PATH_JDE_BIN = "#{PATH_JDE}/bin"
PATH_JDE_LIB = "#{PATH_JDE}/lib/net_rim_api.jar"

PATH_OUTPUT = "bb_output"
PATH_OUTPUT_FULL = File.expand_path( "#{CONFIG_NAME}/#{PATH_OUTPUT}", FileUtils.pwd )

##########################################
# constantes dependentes de cada aplicativo
##########################################
APP_NAME = "Lux Gotas de Beleza"
APP_CODENAME = "#{APP_NAME}".gsub( /[ \.\-]/, '_' )
APP_VENDOR = 'Nano Games'
APP_VERSION = '1.0.1'
MIDLET_CLASS = 'screens.GameMIDlet'
MIDLET_ICON = '/64x64.png'


#def teste()
#  FileUtils.cd( "#{CONFIG_NAME}" )
#
#  path_src = "../../build/#{CONFIG_NAME}/preprocessed"
#  path_list = "list.txt"
#  path_batch = "rapc.bat"
#
#  path_libs = read_res_dirs()
#
#  File.open( path_list, 'w' ) do |file|
#    # copia os arquivos dos componentes e do aplicativo para o caminho de saída
#    Find.find( PATH_COMPONENTS ) do |path|
#      unless ( path.match( /(\.svn)/ ) )
#        unless ( FileTest.directory?( path ) )
#          puts( "#{path}" )
#          file.puts( path.gsub( '/', '\\' ) )
#        end
#      end
#    end
#
#    Find.find( path_src ) do |path|
#      unless ( path.match( /(\.svn)/ ) )
#        unless ( FileTest.directory?( path ) )
#          s = File.expand_path( path, FileUtils.pwd ).chomp()
#          puts( "#{s}" )
#          file.puts( s.gsub( '/', '\\' ) )
#        end
#      end
#    end
#
#    path_libs.each { |lib|
#      lib.chomp!()
#
#      path_full = File.expand_path( "../../#{lib}", FileUtils.pwd ).chomp()
#      Find.find( path_full ) do |path|
#        unless ( path.match( /(\.svn)/ ) )
#          unless ( FileTest.directory?( path ) )
#            puts( "#{path}" )
#            file.puts( path.gsub( '/', '\\' ) )
#          end
#        end
#      end
#    }
#
#  end
#
#  File.open( path_batch, 'w' ) do |file|
#    puts( "BLABLA" )
#    s1 = "\"#{PATH_JDE_BIN}/rapc.exe\""
#    s2 = "-import=\"#{PATH_JDE_LIB}\" codename=#{APP_CODENAME} -midlet jad=blackberry.jad \@#{path_list}"
#    file.puts( "#{s1} #{s2}" )
#  end
#
#  Kernel.exec( path_batch )
#end


def teste()
  FileUtils.cd( "#{CONFIG_NAME}" )

  path_src = "../../build/#{CONFIG_NAME}/preprocessed"
  path_libs = read_res_dirs()

  # apaga conteúdo anterior do diretário de saída
  if ( FileTest.directory?( PATH_OUTPUT ) )
    FileUtils.remove_entry_secure( PATH_OUTPUT )
  end
  FileUtils.mkdir_p( PATH_OUTPUT, :mode => 0755 )

  files = []

  # copia os arquivos dos componentes e do aplicativo para o caminho de saída
  Find.find( PATH_COMPONENTS ) do |path|
    unless ( path.match( /(\.svn)/ ) )
      s = path.sub( PATH_COMPONENTS, '' )
      p = "#{PATH_OUTPUT}/#{s}"

      if ( FileTest.directory?( path ) )
        puts "creating dir #{p}"
        FileUtils.mkdir_p( p )
        next
      else
        files << s.sub( '/', '' ).gsub( '/', '\\' )
#          file.puts( s.sub( '/', '' ).gsub( '/', '\\' ) )
        puts "copying #{path} to #{p}"
        File.copy( path, p, false )
      end
    end
  end

  # copia o código-fonte do aplicativo
  Find.find( path_src ) do |path|
    unless ( path.match( /(\.svn)/ ) )
      s = path.sub( path_src, '' )
      p = "#{PATH_OUTPUT}/#{s}"

      if ( FileTest.directory?( path ) )
        puts "creating dir #{p}"
        FileUtils.mkdir_p( p )
        next
      else
        files << s.sub( '/', '' ).gsub( '/', '\\' )
#          file.puts( s.sub( '/', '' ).gsub( '/', '\\' ) )
        puts "copying #{path} to #{p}"
        File.copy( path, p, false )
      end
    end
  end

  # copia os arquivos de recurso (imagens, textos, sons, etc.)
  path_libs.each { |lib|
    path_full = File.expand_path( "../../#{lib}", FileUtils.pwd ).chomp()

    Find.find( path_full ) do |path|
      unless ( path.match( /(\.svn)/ ) )
        s = path.sub( File.expand_path( "../../", FileUtils.pwd ).chomp(), '' ).gsub( "#{lib}/", '' )
        to = "#{PATH_OUTPUT}/#{s}".chomp()

        if ( FileTest.directory?( path ) )
          puts "creating dir #{to}"
          FileUtils.mkdir_p( to )
          next
        else
          files << s.sub( '/', '' ).gsub( '/', '\\' )
#            file.puts( s.sub( '/', '' ).gsub( '/', '\\' ) )
          puts "copying #{path} to #{to}"
          File.copy( path, to, false )
        end
      end
    end
  }

  create_jdw()
  create_jdp( files.join( "\n" ) )

#  FileUtils.cd( PATH_OUTPUT )
#  puts( "CUR DIR: #{FileUtils.pwd}")
#  File.open( path_batch, 'w' ) do |file|
#    puts( "BATCH: #{path_batch}" )
#    puts( "LIST: #{path_list}" )
#    s1 = "\"#{PATH_JDE_BIN}/rapc.exe\""
#    s2 = "-import=\"#{PATH_JDE_LIB}\" codename=#{APP_CODENAME} -midlet jad=../blackberry.jad \@#{path_list}"
#    file.puts( "#{s1} #{s2}" )
#  end
#
#  Kernel.exec( path_batch )
end


def read_res_dirs()
  temp_all = []
  used = []
  total = {}
  temp_unused = []

  str_used = "configs.#{CONFIG_NAME}.libs.classpath"
  str_unused = "configs.#{CONFIG_NAME}.extra.classpath"

  file = File.open( '../../nbproject/project.properties', 'r' )
  lines = file.readlines()

  for line in lines
    parts = line.split( /\=/ )

    #parts[ 1 ] é igual ao separador '= '
    case ( parts[ 0 ] )
      when str_used then
        paths = parts[ 1 ].split( /;/ )
        paths.each { |p|
#          puts "all: #{p.delete( "\$\{\}\r\n" )}"
          temp_all << p.delete( "\$\{\}\r\n" )
        }
      when str_unused then
        paths = parts[ 1 ].split( /;/ )
        paths.each { |p|
#          puts "-->unused: #{p.delete( "\$\{\}\r\n" )}"
          temp_unused << p.delete( "\$\{\}\r\n" )
        }
      else
        if ( parts[ 0 ].include?( 'file.reference.' ) )
          total[ parts[ 0 ] ] = parts[ 1 ]
        end
    end
  end

  puts "\n"
  temp_all = temp_all - temp_unused
  temp_all.each { |p|
    unless ( total[ p ].nil? )
      used << total[ p ].chomp()
      puts "libs: #{total[ p ]}"
    end
  }
  
  return used
end


##
# Cria o arquivo do workspace do JDE BlackBerry.
##
def create_jdw()
  puts( "CREATING JDW: #{PATH_OUTPUT_FULL}/#{APP_CODENAME}.jdw")
  File.open( "#{PATH_OUTPUT_FULL}/#{APP_CODENAME}.jdw", 'w' ) do |file|
    file.puts( "## RIM Java Development Environment\n# RIM Workspace file\n[BuildConfigurations\nDebug\nRelease\n]\n[Projects\n#{APP_CODENAME}.jdp\n]\n[ReleaseActiveProjects\n#{APP_CODENAME}.jdp\n]" )
  end

end


def create_jdp( file_list )
  puts( "CREATING JDP: #{PATH_OUTPUT_FULL}/#{APP_CODENAME}.jdp")
  File.open( "#{PATH_OUTPUT_FULL}/#{APP_CODENAME}.jdp", 'w' ) do |file|
    file.puts( "## RIM Java Development Environment\n# RIM Project file\nAlwaysBuild=0\n[AlxImports\n]\nAutoRestart=0\n[BuildOnlyFiles\n]\n[ClassProtection\n]\n[CustomBuildFiles\n]\n[CustomBuildRules\n]\n[DefFiles\n]\n[DependencyFiles\n]\n[DependsOn\n]\nExcludeFromBuildAll=0\n[Files\n#{file_list}\n]\nHaveAlxImports=0\nHaveDefs=0\nHaveImports=0\n[Icons\n#{MIDLET_ICON}\n]\n[Imports\n]\nListing=0\nMidletClass=#{MIDLET_CLASS}\nNoImport=0\nOptions=-quiet\nOutputFileName=#{APP_CODENAME}\n[PackageProtection\n]\nRibbonPosition=0\nRunOnStartup=0\nStartupTier=7\nSystemModule=0\nTitle=#{APP_NAME}\nType=1\nVendor=#{APP_VENDOR}\nVersion=#{APP_VERSION}\n" )
  end
end


teste()
#read_res_dirs()