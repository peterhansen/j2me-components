/*
 * Drawable.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;

//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//#endif

/**
 *
 * @author peter
 */
public abstract class Drawable {
 
    /** posição do drawable */
	protected final Point position = new Point();
	 
    /** indica se o drawable está visível */
	protected boolean visible = true;
	 
    /** dimensões do drawable (utilizado para fins de detecção de colisão e para definir área de clip inicial) */
	protected final Point size = new Point();
    
    /** pixel de referência (em relação ao tamanho do drawable, e não à sua posição) */
	protected Point referencePixel = new Point();
	 
    /** transformação aplicada ao drawable (padrão: TRANS_NONE). Pode haver múltiplas transformações (espelhamento
	 * horizontal e rotação de 90º, por exemplo), através do uso do operador matemético OU: |
	 */
	protected int transform;
	
	/** transformação passada às classes MIDP que utilizam transformação (valores para as mesmas transformações são diferentes) */
	protected int transformMIDP;
	
	/** 
	 * Área visível do drawable <b>nas coordenadas da tela</b>. Caso seja <code>null</code>, é utilizada a área total da tela.
	 */
	protected Rectangle viewport;
	
    /** nenhuma transformação */
	public static final byte TRANS_NONE = 0;    
    /** espelhamento vertical */
	public static final byte TRANS_MIRROR_V = 1;	
    /** espelhamento horizontal */
	public static final byte TRANS_MIRROR_H = 2;
    /** rotação de 90º no sentido horário */
	public static final byte TRANS_ROT90 = 4;
    /** rotação de 180º */
	public static final byte TRANS_ROT180 = 8;
    /** rotação de 270º no sentido horário */
	public static final byte TRANS_ROT270 = 16;
	
	protected static final byte TRANS_MASK_MIRROR = ( byte ) ( TRANS_MIRROR_H | TRANS_MIRROR_V );
	protected static final byte TRANS_MASK_ROTATE = ( byte ) ( TRANS_ROT90 | TRANS_ROT180 | TRANS_ROT270 );
	protected static final byte TRANS_MASK_ALL = ( byte ) ( TRANS_MASK_MIRROR | TRANS_MASK_ROTATE );
	
	/** Constante para definir a âncora do drawable relativa à sua posição vertical do topo (y = 0). */
	public static final byte ANCHOR_TOP		= 16;
	/** Constante para definir a âncora do drawable relativa à sua posição horizontal esquerda (x = 0). */
	public static final byte ANCHOR_LEFT	= 4;
	/** Constante para definir a âncora do drawable relativa à sua posição vertical inferior (y = size.y). */
	public static final byte ANCHOR_BOTTOM	= 32;
	/** Constante para definir a âncora do drawable relativa à sua posição horizontal da direita (x = size.x). */
	public static final byte ANCHOR_RIGHT	= 8;
	/** Constante para definir a âncora do drawable relativa à sua posição vertical central (y = size.y / 2). */
	public static final byte ANCHOR_VCENTER	= 2;
	/** Constante para definir a âncora do drawable relativa à sua posição horizontal central (x = size.x / 2). */
	public static final byte ANCHOR_HCENTER	= 1;

	/** Constante para definir a âncora do drawable relativa à sua posição superior esquerda. */
	public static final byte ANCHOR_TOP_LEFT = ANCHOR_TOP | ANCHOR_LEFT;
	/** Constante para definir a âncora do drawable relativa à sua posição superior direita. */
	public static final byte ANCHOR_TOP_RIGHT = ANCHOR_TOP | ANCHOR_RIGHT;
	/** Constante para definir a âncora do drawable relativa à sua posição inferior esquerda. */
	public static final byte ANCHOR_BOTTOM_LEFT = ANCHOR_BOTTOM | ANCHOR_LEFT;
	/** Constante para definir a âncora do drawable relativa à sua posição inferior direita. */
	public static final byte ANCHOR_BOTTOM_RIGHT = ANCHOR_BOTTOM | ANCHOR_RIGHT;
	/** Constante para definir a âncora do drawable relativa à sua posição inferior central. */
	public static final byte ANCHOR_CENTER = ANCHOR_VCENTER | ANCHOR_HCENTER;
	
	/***/
	public static final byte ANCHOR_HORIZONTAL_MASK = ANCHOR_LEFT | ANCHOR_HCENTER | ANCHOR_RIGHT;
    /***/
	public static final byte ANCHOR_VERTICAL_MASK = ANCHOR_TOP | ANCHOR_VCENTER | ANCHOR_BOTTOM;

	/** Indica se o teste de clipping deve ser feito. */
	protected boolean clipTest = true;
    
	/** Altura máxima da pilha de clip. */
	private static final byte MAX_CLIP_STACK_SIZE = 32;
	/** Pilha de áreas de clip usada pelos drawables no momento do desenho. */
    protected static final Rectangle[] clipStack = new Rectangle[ MAX_CLIP_STACK_SIZE ];
	
	protected static byte currentStackSize;
	
	/** Translação acumulada no desenho atual. */
	public static final Point translate = new Point();

	/** Indica se o sprite MIDP2 deve ser usado. Maiores informações nas classes DrawableImage e Sprite. */
	protected static final boolean USE_MIDP2_SPRITE;


	static {
//		switch ( AppMIDlet.getVendor() ) { TODO temporariamente, consideramos esse atributo false devido às alterações na classe Sprite (sprites estão sendo desenhados de forma incorreta ao serem espelhados)
//			case AppMIDlet.VENDOR_SAMSUNG:
//			case AppMIDlet.VENDOR_SIEMENS:
//				USE_MIDP2_SPRITE = true;
//			break;
//
//			default:
				USE_MIDP2_SPRITE = false;
//		}

		for ( int i = 0; i < MAX_CLIP_STACK_SIZE; ++i )
			clipStack[ i ] = new Rectangle();
	}


    /**
     * Define o estado de visibilidade do drawable.
	 * 
     * @param visible estado de visibilidade do drawable.
     */
	public void setVisible( boolean visible ) {
        this.visible = visible;
	}
	
    
    /**
     * Obtém o estado de visibilidade atual do drawable.
	 * 
     * @return boolean indicando o estado de visibilidade do drawable.
     */
	public boolean isVisible() {
		return visible;
	}
	
    
    /**
     * Posiciona o ponto top-left (0,0) do drawable na posição (x,y). Esse método não leva em consideração o ponto de 
	 * referência do drawable; para posicionar o ponto de referência em (x,y), deve-se utilizar o método <b>setRefPixelPosition</b>.
     * @param x posição horizontal onde o ponto mais à esquerda do drawable será posicionado.
     * @param y posição vertical onde o ponto mais superior do drawable será posicionado.
	 * @see #setPosition(Point)
	 * @see #setRefPixelPosition(int, int)
	 * @see #setRefPixelPosition(Point)
     */
	public void setPosition( int x, int y ) {
        position.set( x, y );
	}
	
	
	/**
	 * Posiciona o ponto top-left (0,0) do drawable na posição do Point recebido como parâmetro. Esse método não leva em 
	 * consideração o ponto de referência do drawable; para posicionar o ponto de referência em (x,y), deve-se utilizar 
	 * o método <b>setRefPixelPosition</b>. A chamada desse método equivale a <b>setPosition( p.x, p.y )</b>.
	 * @param p ponto nova posição top-left do drawable.
	 * @see #setPosition(int,int)
	 * @see #setRefPixelPosition(int, int)
	 * @see #setRefPixelPosition(Point)
	 */
	public final void setPosition( Point p ) {
		setPosition( p.x, p.y );
	}
	
    
    /**
     * Retorna a posição top-left do drawable.
	 * 
     * @return referência para a posição do drawable.
	 * @see #getPosX()
	 * @see #getPosY()
     */
	public final Point getPosition() {
		return position;
	}
	
	
	/**
	 * Retorna a posição x (esquerda) do drawable.
	 * 
	 * @return inteiro indicando a posição horizontal (esquerda) do drawable.
	 * @see #getPosition()
	 * @see #getPosY()
	 */
	public final int getPosX() {
		return position.x;
	}
	
	
	/**
	 * Retorna a posição y (topo) do drawable.
	 * 
	 * @return inteiro indicando a posição vertical (topo) do drawable.
	 * @see #getPosition()
	 * @see #getPosX()
	 */	
	public final int getPosY() {
		return position.y;
	}
	
    
    /**
     * Realiza as operações de clip e translação do drawable antes das chamadas efetivas de desenho no Graphics, levando
	 * em consideração a área de clip e posição do grupo "pai" do drawable, de forma que a área de clip não seja ultrapassada. 
	 * <p>Esse método não executa chamadas diretas de desenho no Graphics; elas devem ser feitas no método <b>paint</b>. Após a
	 * chamada de paint, a área anterior de clip e a posição de desenho do grupo são restauradas.</p>
	 * 
     * @param g Graphics onde será desenhado o drawable.
	 * @see #paint(Graphics)
     */
	public void draw( Graphics g ) {
		if ( visible ) {
			if ( clipTest ) {
				pushClip( g );
				translate.addEquals( position );

				final Rectangle clip = clipStack[ currentStackSize ];
				clip.setIntersection( translate.x, translate.y, size.x, size.y );

				if ( clip.width > 0 && clip.height > 0 ) {
					g.setClip( clip.x, clip.y, clip.width, clip.height );

					paint( g );
				}

				translate.subEquals( position );
				popClip( g );
			} else {
				// desenha direto, ignorando o teste de interseção
				translate.addEquals( position );
				paint( g );
				translate.subEquals( position );
			}
		} // fim if ( visible )        
	} // fim do método draw( Graphics )
	
    
    /**
     * Move o drawable.
	 * 
     * @param dx variação horizontal na posição do drawable.
     * @param dy variação vertical na posição do drawable.
	 * @see #move(Point)
     */
	public void move( int dx, int dy ) {
        setPosition( position.x + dx, position.y + dy );
	}
    
	 
    /**
     * Move o drawable. A chamada deste método equivale a <b>move( distance.x, distance.y )</b>.
	 * 
     * @param distance distância (x,y) a ser percorrida pelo drawable. 
	 * @see #move(int, int)
     */
	public final void move( Point distance ) {
        move( distance.x, distance.y );
	}
    
	 
    /**
     * Obtém o tamanho do drawable.
	 * 
     * @return referência para o Point que define o tamanho do drawable.
	 * @see #getWidth()
	 * @see #getHeight()
     */
	public final Point getSize() {
		return size;
	}
	
	
	/**
	 * Retorna a largura do drawable.
	 * 
	 * @return inteiro indicando a largura do drawable.
	 * @see #getSize()
	 * @see #getHeight()
	 */
	public final int getWidth() {
		return size.x;
	}
	
	
	/**
	 * Retorna a altura do drawable.
	 * 
	 * @return inteiro indicando a altura do drawable.
	 * @see #getSize()
	 * @see #getWidth()
	 */	
	public final int getHeight() {
		return size.y;
	}
	 
    
    /**
     * Define o tamanho do drawable. Chamada equivalente a <b>setSize( size.x, size.y )</b>.
	 * 
     * @param size tamanho (largura, altura) do drawable.
	 * @see #setSize(int, int)
     */
	public final void setSize( Point size ) {
        setSize( size.x, size.y );
	}
	
	
	/**
	 * Define o tamanho do drawable.
	 * 
	 * @param width largura em pixels do drawable.
	 * @param height altura em pixels do drawable.
	 * @see #setSize(Point)
	 */
	public void setSize( int width, int height ) {
		size.set( width, height );
	}
	
	
	/**
	 * Obtém o retângulo da área visível do drawable.
	 * 
	 * @return referência para o Rectangle que representa a área visível do drawable na tela. Caso seja <code>null</code>,
	 * significa que a área visível do drawable na tela é a tela inteira (conforme valor retornado por 
	 * <code>ScreenManager.getViewport()</code>.
	 * 
	 * @see #setViewport(Rectangle)
	 */
	public final Rectangle getViewport() {
		return viewport;
	}
	
	
	/**
	 * Define a área de visualização efetiva do drawable na tela.
	 * 
	 * @param viewport Rectangle que define a área visível do drawable na tela. A referência é armazenada internamente,
	 * ou seja, alterações subsequentes ao Rectangle refletirão automaticamente no viewport do drawable.
	 * 
	 * <p>Caso seja passado <code>null</code>, o drawable realiza a interseção com a área total da tela, conforme definido
	 * na classe <code>ScreenManager</code>.
	 * 
	 * @see #getViewport()
	 */
	public final void setViewport( Rectangle viewport ) {
		this.viewport = viewport;
	}
	 
    
	/**
	 * Trata apenas do desenho em si no Graphics, ou seja, chamadas de métodos como g.drawImage(), g.drawRect() e etc. 
	 * Todas as operações de clip e/ou translação são executadas antes no método draw( Graphics ).
	 * 
	 * @param g 
	 * @see #draw(Graphics)
	 */
	protected abstract void paint( Graphics g );
    
    
    /**
     * Empilha a área de clip atual.
	 * 
     * @param g Graphics cuja área de clip definida será empilhada.
     */
	public final void pushClip( Graphics g ) {
		if ( currentStackSize < 0 || currentStackSize >= clipStack.length )
			AppMIDlet.log( "PUSH CLIP ERROR! (" + currentStackSize + ")");
		
		//#if J2SE == "false"
			clipStack[ currentStackSize++ ].set( g.getClipX(), g.getClipY(), g.getClipWidth(), g.getClipHeight() );
		//#else
//# 			final java.awt.Rectangle clip = g.getClipBounds();
//# 			clipStack[ currentStackSize++ ].set( clip.x, clip.y, clip.width, clip.height );
		//#endif
		
		if ( viewport == null )
			clipStack[ currentStackSize ].set( clipStack[ currentStackSize - 1 ] );
		else
			clipStack[ currentStackSize ].setIntersection( clipStack[ currentStackSize - 1 ], viewport );
	}
	
    
    /**
     * Remove o topo da pilha de áreas de clip, e define a área de clip atual a partir dela.
	 * 
     * @param g Graphics cuja área de clip será restaurada.
     */
	public final void popClip( Graphics g ) {
		if ( currentStackSize > 0 ){
			final Rectangle clip = clipStack[ --currentStackSize ];
			g.setClip( clip.x, clip.y, clip.width, clip.height );
		} else {
			AppMIDlet.log("POP CLIP ERROR! (" + currentStackSize + ")" );
		}

	}

	/**
	 * Reinicia a pilha de desenho do Graphics e do Drawable.
	 */
	public static final void resetDrawStack( Graphics g ) {
		translate.set( 0, 0 );
		// TODO teste... currentStackSize = 0;
		g.translate( -g.getTranslateX(), -g.getTranslateY() );
	}
	
    
    /**
     * Define o pixel de referência do drawable. Esse método apenas define o ponto a ser utilizado como referência; ele não
	 * altera a posição atual do drawable.
	 * 
     * @param refPixelX ponto x de referência do drawable.
     * @param refPixelY ponto y de referência do drawable.
	 * @see #defineReferencePixel(Point)
	 * @see #setRefPixelPosition(int,int)
	 * @see #defineReferencePixel(int)
	 * @see #setRefPixelPosition(Point)
     */
	public void defineReferencePixel( int refPixelX, int refPixelY ) {
        referencePixel.set( refPixelX, refPixelY );
	}
	 
    
    /**
     * Define o pixel de referência do drawable. Esse método apenas define o ponto a ser utilizado como referência; ele não
	 * altera a posição atual do drawable. Método equivalente à chamada de <b>defineReferencePixel( refPixel.x, refPixel.y )</b>.
	 * 
     * @param refPixel ponto de referência (x,y) do drawable.
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(int)
	 * @see #setRefPixelPosition(int,int)
	 * @see #setRefPixelPosition(Point)
     */
	public final void defineReferencePixel( Point refPixel ) {
        defineReferencePixel( refPixel.x, refPixel.y );
	}
	
	
	/**
	 * Define o pixel de referência do drawable a partir de uma de suas âncoras. Esse método apenas define o ponto a ser 
	 * utilizado como referência; ele não altera a posição atual do drawable.
	 * 
	 * @param anchor inteiro indicando a âncora utilizada para definir o pixel de referência. Os valores válidos são
	 * formados a partir da combinação de indicadores verticais (ANCHOR_TOP, ANCHOR_VCENTER, ANCHOR_BOTTOM) e horizontais
	 * (ANCHOR_LEFT, ANCHOR_HCENTER, ANCHOR_RIGHT). Passar o valor 0 (zero) resulta na utilização da âncora TOP-LEFT.
	 * 
	 * @see #defineReferencePixel(Point)
	 * @see #defineReferencePixel(int,int)
	 * @see #setRefPixelPosition(int,int)
	 * @see #setRefPixelPosition(Point)
	 * @see #ANCHOR_TOP
	 * @see #ANCHOR_LEFT
	 * @see #ANCHOR_BOTTOM
	 * @see #ANCHOR_RIGHT
	 * @see #ANCHOR_VCENTER
	 * @see #ANCHOR_HCENTER
	 */
	public final void defineReferencePixel( int anchor ) {
		final Point anchorPoint = new Point();
		
		switch ( anchor & ANCHOR_VERTICAL_MASK ) {
			case ANCHOR_VCENTER:
				anchorPoint.y = size.y >> 1;
			break;
			
			case ANCHOR_BOTTOM:
				anchorPoint.y = size.y;
			break;
		}
		
		switch ( anchor & ANCHOR_HORIZONTAL_MASK ) {
			case ANCHOR_HCENTER:
				anchorPoint.x = size.x >> 1;
			break;
			
			case ANCHOR_RIGHT:
				anchorPoint.x = size.x;
			break;			
		}
		
		defineReferencePixel( anchorPoint );
	} // fim do método defineReferencePixel( int )
	 
    
    /**
     * Define a posição do ponto de referência a partir de um Point. Método equivalente à chamada de 
	 * <b>setRefPixelPosition( refPosition.x, refPosition.y )</b>.
	 * 
     * @param refPosition posição (x,y) do ponto de referência.
	 * @see #setRefPixelPosition(int,int)
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(Point)
     */
	public final void setRefPixelPosition( Point refPosition ) {
        setRefPixelPosition( refPosition.x, refPosition.y );
	}
	
    
    /**
     * Define a posição do ponto do referência.
	 * 
     * @param x posição x do ponto de referência.
     * @param y posição y do ponto de referência.
	 * @see #setRefPixelPosition(Point)
	 * @see #defineReferencePixel(int,int)
	 * @see #defineReferencePixel(Point)
     */
	public void setRefPixelPosition( int x, int y ) {
        position.set( x - referencePixel.x, y - referencePixel.y );        
	}
	
    
    /**
     * Obtém a posição do ponto de referência.
     * @return 
     */
	public Point getRefPixelPosition() {
		return new Point( position.x + referencePixel.x, position.y + referencePixel.y );
	}
	
    
    /**
     * Obtém a posição x do ponto de referência.
     * @return 
     */
	public int getRefPixelX() {
		return position.x + referencePixel.x;
	}
	 
    
    /**
     * Obtém a posição y do ponto de referência.
     * @return 
     */
	public int getRefPixelY() {
		return position.y + referencePixel.y;
	}
	 
    
	/**
	 * Verifica se um ponto está dentro da área do drawable.
	 * 
	 * @param p ponto cuja interseção com a área do drawable será testada.
	 * @return boolean indicando se o ponto está dentro dos limites do drawable.
	 * @throws NullPointerException caso p seja nulo.
	 */
	public final boolean contains( Point p ) {
		return contains( p.x, p.y );
	}
	
	
	/**
	 * Verifica se um ponto está dentro da área do drawable.
	 * 
	 * @param x posição x do ponto cuja interseção será verificada.
	 * @param y posição y do ponto cuja interseção será verificada.
	 * @return boolean indicando se o ponto está dentro dos limites do drawable.
	 * @throws NullPointerException caso p seja nulo.
	 */	
	public boolean contains( int x, int y ) {
		return isVisible() && x >= position.x && y >= position.y && x <= ( position.x + size.x ) && y <= ( position.y + size.y );
	}
	
	
	/**
	 * Obtém o drawable presente no ponto <code>p</code>. Caso o drawable seja um grupo, é feita uma varredura interna para
	 * que seja retornado o drawable mais à frente cuja área englobe o ponto <code>p</code>.
	 * @param p ponto cuja interseção com o drawable será verificada.
	 * @return referência para o drawable presente na posição (x,y), ou <code>null</code> caso o ponto não esteja
	 * dentro da área de colisão do drawable.
	 */
	public final Drawable getDrawableAt( Point p ) {
		return getDrawableAt( p.x, p.y );
	}
	
	
	/**
	 * Obtém o drawable presente na posição (x,y). Caso o drawable seja um grupo, é feita uma varredura interna para
	 * que seja retornado o drawable mais à frente cuja área englobe o ponto (x,y).
	 * 
	 * @param x posição x do ponteiro.
	 * @param y posição y do ponteiro.
	 * @return referência para o drawable presente na posição (x,y), ou <code>null</code> caso o ponto não esteja
	 * dentro da área de colisão do drawable.
	 */
	public Drawable getDrawableAt( int x, int y ) {
		return contains( x, y ) ? this : null;
	}
    
	 
    /**
     * Define a transformação atual do drawable, realizando operações de rotação e/ou espelhamento, conforme necessário.
	 * Caso a transformação envolva rotação E espelhamento, primeiro é feito o espelhamento, e então a rotação.
     * @param transform 
     * @return 
	 * @see #rotate(int)
	 * @see #mirror(int)
     */
	public boolean setTransform( int transform ) {
		final int mirrorState = getMirror();
		if ( ( mirrorState & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) )
			mirror( TRANS_MIRROR_H );

		if ( ( mirrorState & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) )
			mirror( TRANS_MIRROR_V );
		
		final int newRotation = transform & TRANS_MASK_ROTATE;
		if ( getRotation() != newRotation ) {
			switch ( getRotation() ) {
				case TRANS_NONE:
					rotate( newRotation );
				break;
				
				case TRANS_ROT90:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT270 );
						break;
						
						case TRANS_ROT180:
							rotate( TRANS_ROT90 );
						break;

						case TRANS_ROT270:
							rotate( TRANS_ROT180 );
						break;
					} // fim switch ( newRotation )
				break;

				case TRANS_ROT180:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT180 );
						break;
						
						case TRANS_ROT90:
							rotate( TRANS_ROT270 );
						break;

						case TRANS_ROT270:
							rotate( TRANS_ROT90 );
						break;
					} // fim switch ( newRotation )
				break;

				case TRANS_ROT270:
					switch ( newRotation ) {
						case TRANS_NONE:
							rotate( TRANS_ROT90 );
						break;
						
						case TRANS_ROT90:
							rotate( TRANS_ROT180 );
						break;

						case TRANS_ROT180:
							rotate( TRANS_ROT270 );
						break;
					} // fim switch ( newRotation )
				break;
			} // fim switch ( getRotation() )
		} // fim if ( getRotation() != newRotation )

		return false;
	}
	 
    
    /**
     * Obtém a transformação acumulada.
	 * 
     * @return inteiro indicando a transformação acumulada.
     */
	public int getTransform() {
		return transform;
	}
	
	
	/**
	 * Espelha o drawable em torno de um de seus eixos.
	 * 
	 * @param mirrorType tipo do espelhamento. Os valores válidos são:
	 * <ul>
	 * <li>TRANS_MIRROR_H: espelha em torno do eixo vertical, ou seja, esquerda e direita são invertidos.</li>
	 * <li>TRANS_MIRROR_V: espelha em torno do eixo horizontal, ou seja, cima e baixo são invertidos.</li>
	 * <li>TRANS_MIRROR_H | TRANS_MIRROR_V: espelha em ambos os eixos (operação equivalente a uma rotação de 180º)</li>
	 * </ul>
	 * @return boolean indicando se a operação é suportada pelo drawable.
	 * @see #setTransform(int)
	 */
	public boolean mirror( int mirrorType ) {
		mirrorType &= TRANS_MASK_MIRROR;
		final int newMirrorStatus = ( transform & TRANS_MASK_MIRROR ) ^ mirrorType;
		
		// se houver mudança no espelhamento horizontal, atualiza o pixel de referência
		if ( ( newMirrorStatus & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) )
			defineReferencePixel( size.x - referencePixel.x, referencePixel.y );
		
		// se houver mudança no espelhamento vertical, atualiza o pixel de referência
		if ( ( newMirrorStatus & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) )
			defineReferencePixel( referencePixel.x, size.y - referencePixel.y );						

		// a transformação passa a ser a união da rotação acumulada com o novo estado de espelhamento
		transform = ( transform & TRANS_MASK_ROTATE ) | newMirrorStatus;
		updateTransformMIDP();
		
		return false;
	} // fim do método mirror( int )
	
	
	/**
	 * Rotaciona o drawable, utilizando como pivô da rotação o seu pixel de referência.
	 * @param rotationType ângulo da rotação. Valores válidos:
	 * <ul>
	 * <li>TRANS_ROT90: rotaciona 90º no sentido horário (270º no sentido anti-horário).</li>
	 * <li>TRANS_ROT180: rotaciona o drawable 180º.</li>
	 * <li>TRANS_ROT270: rotaciona 270º no sentido horário (90º no sentido anti-horário).</li>
	 * </ul>
	 * @return boolean indicando se a operação de rotação é suportada pelo drawable.
	 * @see #setTransform(int)
	 */
	public boolean rotate( int rotationType ) {
		rotationType &= TRANS_MASK_ROTATE;
		
		final Point previousRef = new Point( getRefPixelPosition() );
		
		switch ( rotationType ) {
			case TRANS_ROT90:
				defineReferencePixel( size.y - referencePixel.y, referencePixel.x );
				setSize( size.y, size.x );
			break;
			
			case TRANS_ROT180:
				defineReferencePixel( size.x - referencePixel.x, size.y - referencePixel.y );				
			break;
			
			case TRANS_ROT270:
				defineReferencePixel( referencePixel.y, size.x - referencePixel.x );				
				setSize( size.y, size.x );
			break;
			
			default:
				return true;
		} // fim switch ( rotationType )
		
		// TODO atualizar área de colisão após rotação de 90º ou 270º
		int newRotation = transform & TRANS_MASK_ROTATE;		
		
		switch ( newRotation ) {
			case TRANS_NONE:
				newRotation = rotationType;
			break;
			
			case TRANS_ROT90:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_ROT180;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_ROT270;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_NONE;
					break;
				} // fim switch ( rotationType )
			break;

			case TRANS_ROT180:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_ROT270;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_NONE;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_ROT90;
					break;
				} // fim switch ( rotationType )				
			break;

			case TRANS_ROT270:
				switch ( rotationType ) {
					case TRANS_ROT90:
						newRotation = TRANS_NONE;
					break;
					
					case TRANS_ROT180:
						newRotation = TRANS_ROT90;
					break;
					
					case TRANS_ROT270:
						newRotation = TRANS_ROT180;
					break;
				} // fim switch ( rotationType )				
			break;
			
			default:
				return false;
		} // fim switch ( newRotation )
		
		setPosition( previousRef.x - referencePixel.x, previousRef.y - referencePixel.y );
		
		transform = ( transform & TRANS_MASK_MIRROR ) | newRotation;
		updateTransformMIDP();
		
		return false;
	} // fim do método rotate( int )
	
	
	/**
	 * Obtém o espelhamento atual.
	 * 
	 * @return int indicando o espelhamento.
	 * @see #mirror(int)
	 */
	public int getMirror() {
		return transform & TRANS_MASK_MIRROR;
	}
	
	
	/**
	 * Obtém a rotação atual.
	 * 
	 * @return int indicando a rotação atual.
	 * @see #rotate(int)
	 */
	public int getRotation() {
		return transform & TRANS_MASK_ROTATE;
	}
	
	
	
	/**
	 * Atualiza o valor interno de transformMIDP associado à transformação acumulada, para ser passado corretamente a
	 * métodos como <code>Graphics.drawRegion()</code>.
	 */
	protected final void updateTransformMIDP() {
		//#if J2SE == "false"
			transformMIDP = getTransformMIDP( transform );
		//#endif
	} // fim do método updateTransform()
	
    
    //#if J2SE == "false"
		/**
		 * Obtém o inteiro que representa a transformação de acordo com os valores definidos no padrão MIDP.
		 * @param tranform inteiro representado a transformação acumulada no padrão dos componentes.
		 * @return inteiro representando a transformação no padrão MIDP.
		 */
		protected static final int getTransformMIDP( int transform ) {
			switch ( transform ) {
				case TRANS_NONE:
				case TRANS_ROT180 | TRANS_MIRROR_H | TRANS_MIRROR_V:
					return javax.microedition.lcdui.game.Sprite.TRANS_NONE;

				case TRANS_MIRROR_H:
				case TRANS_MIRROR_V | TRANS_ROT180:	
					return javax.microedition.lcdui.game.Sprite.TRANS_MIRROR;

				case TRANS_MIRROR_V:
				case TRANS_MIRROR_H | TRANS_ROT180:
					return javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT180;

				case TRANS_ROT180:
				case TRANS_MIRROR_H | TRANS_MIRROR_V:
					return javax.microedition.lcdui.game.Sprite.TRANS_ROT180;

				case TRANS_ROT90:
				case TRANS_MIRROR_H | TRANS_MIRROR_V | TRANS_ROT270:
					return javax.microedition.lcdui.game.Sprite.TRANS_ROT90;

				case TRANS_ROT270:
				case TRANS_MIRROR_H | TRANS_MIRROR_V | TRANS_ROT90:
					return javax.microedition.lcdui.game.Sprite.TRANS_ROT270;

				case TRANS_MIRROR_H | TRANS_ROT270:
				case TRANS_MIRROR_V | TRANS_ROT90:
					return javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT90;

				case TRANS_MIRROR_H | TRANS_ROT90:			
				case TRANS_MIRROR_V | TRANS_ROT270:
					return javax.microedition.lcdui.game.Sprite.TRANS_MIRROR_ROT270;

				default:
					return 0;
			}		
		}
	//#endif


	/**
	 * Indica se o teste de clipping de viewport está ativado.
	 */
	public final boolean isClipTest() {
		return clipTest;
	}


	/**
	 * 
	 * @param clipTest
	 */
	public final void setClipTest( boolean clipTest ) {
		this.clipTest = clipTest;
	}

	
	/**
	 * Obtém a área de clip definida atual.
	 * @return referência para o topo da pilha de clip, ou null, caso a pilha esteja vazia.
	 * @see #pushClip(Graphics)
	 * @see #popClip(Graphics)
	 */
	public static final Rectangle getClip() {
		if ( currentStackSize > 0 )
			return clipStack[ currentStackSize - 1 ];
		
		return null;
	} // fim do método getClip()


    /**
	 * Altera a luminosidade de uma cor.
	 *
	 * @param color cor cuja alteração da luminosidade será calculada.
	 * @param factor grau de alteração da cor. Valores válidos variam de -255 a +255. Valores positivos deixam a cor
	 * mais clara, e valores negativos a tornam mais escura.
	 */
	public static final int changeColorLightness( int color, int factor ) {
		int r = color >> 16 & 0xff;
		int g = color >> 8 & 0xff;
		int b = color & 0xff;

		if ( factor > 0 ) {
			r = Math.min( 0xff, r + factor );
			g = Math.min( 0xff, g + factor );
			b = Math.min( 0xff, b + factor );
		} else {
			r = Math.max( 0, r + factor );
			g = Math.max( 0, g + factor );
			b = Math.max( 0, b + factor );
		}

		return ( ( ( r << 16 ) & 0xff0000 ) | ( ( g << 8 ) & 0xff00 ) | ( b & 0xff ) );
	}

	
}
 
