//package br.com.nanogames.components.gestures;
//
//import java.util.Vector;
//import javax.microedition.rms.RecordStore;
//
//
//public class AccelerationStreamAnalyzer {
//
//	double accepttolerance = 0.2;
//	private Gesture current; // current gesture
//	private double maxacc;
//	private double minacc;
//	Vector gesturemodel;
//	public Vector trainsequence;
//	Vector trainSave[] = new Vector[ 10 ];	// State variables
//	private boolean learning,  analyzing;
//
//
//	public AccelerationStreamAnalyzer() {
//		this.maxacc = Double.MIN_VALUE;
//		this.minacc = Double.MAX_VALUE;
//		this.learning = false;
//		this.analyzing = false;
//		this.current = new Gesture();
//		this.gesturemodel = new Vector();
//		this.trainsequence = new Vector();
//
//		for ( int i = 0; i < trainSave.length; i++ ) {
//			trainSave[i] = new Vector();
//		}
//	}
//
//
//	public void accelerationReceived( AccelerationEvent event ) {
//		if ( this.learning || this.analyzing ) {
//			if ( this.current.getCountOfData() > 0 ) {
//
//				AccelerationEvent ref = this.current.getLastData();
//				if ( event.getX() < ref.getX() - accepttolerance ||
//						event.getX() > ref.getX() + accepttolerance ||
//						event.getY() < ref.getY() - accepttolerance ||
//						event.getY() > ref.getY() + accepttolerance ||
//						event.getZ() < ref.getZ() - accepttolerance ||
//						event.getZ() > ref.getZ() + accepttolerance ) {
//					this.current.add( event );
//				}
//			} else {
//
//				this.maxacc = Double.MIN_VALUE;
//				this.minacc = Double.MAX_VALUE;
//				this.current.add( event );
//			}
//
//
//			if ( Math.abs( event.getX() ) > this.maxacc ) {
//				this.maxacc = Math.abs( event.getX() );
//				this.current.setMaxAcceleration( this.maxacc );
//			}
//			if ( Math.abs( event.getY() ) > this.maxacc ) {
//				this.maxacc = Math.abs( event.getY() );
//				this.current.setMaxAcceleration( this.maxacc );
//			}
//			if ( Math.abs( event.getZ() ) > this.maxacc ) {
//				this.maxacc = Math.abs( event.getZ() );
//				this.current.setMaxAcceleration( this.maxacc );
//			}
//
//
//			if ( Math.abs( event.getX() ) < this.minacc ) {
//				this.minacc = Math.abs( event.getX() );
//				this.current.setMinAcceleration( this.minacc );
//			}
//			if ( Math.abs( event.getY() ) < this.minacc ) {
//				this.minacc = Math.abs( event.getY() );
//				this.current.setMinAcceleration( this.minacc );
//			}
//			if ( Math.abs( event.getZ() ) < this.minacc ) {
//				this.minacc = Math.abs( event.getZ() );
//				this.current.setMinAcceleration( this.minacc );
//			}
//		}
//
//	}
//
//
//	public static final int TRAIN = -1;
//	public static final int RECOGNITION = -2;
//	public static final int CLOSE = -3;
//
//
//	public int handleStartEvent( int event ) {
//
//
//		if ( ( !this.analyzing && !this.learning ) &&
//				event == TRAIN ) {
//
//			this.learning = true;
//
//		}
//
//
//		if ( ( !this.analyzing && !this.learning ) &&
//				event == RECOGNITION ) {
//
//			this.analyzing = true;
//
//		}
//
//
//		if ( ( !this.analyzing && !this.learning ) &&
//				event == CLOSE ) {
//
//			if ( this.trainsequence.size() > 0 ) {
//
//				this.learning = true;
//
//				gesturemodel.addElement( new GestureModel( gesturemodel.size() ) );
//				( ( GestureModel ) this.gesturemodel.lastElement() ).train( this.trainsequence );
//				trainSave[gesturemodel.size() - 1] = this.trainsequence;
//
//				this.trainsequence = new Vector();
//
//				this.learning = false;
//				return -2;
//			}
//		}
//		return -1;
//	}
//
//
//	public int handleStopEvent( int event ) {
//		if ( this.learning && event == TRAIN ) {
//			if ( this.current.getCountOfData() > 0 ) {
//
//				Gesture gesture = new Gesture( this.current );
//				this.trainsequence.addElement( gesture );
//				this.current = new Gesture();
//				this.learning = false;
//				return TRAIN;
//			} else {
//
//				this.learning = false;
//			}
//		} else if ( this.analyzing && event == RECOGNITION ) {
//			if ( this.current.getCountOfData() > 0 ) {
//
//				Gesture gesture = new Gesture( this.current );
//				int gest = this.analyze( gesture );
//				this.current = new Gesture();
//				this.analyzing = false;
//				return gest;
//			} else {
//
//				this.analyzing = false;
//			}
//		}
//		return -1;
//	}
//
//
//	public int analyze( Gesture g ) {
//
//		double sum = 0;
//		for ( int i = 0; i < this.gesturemodel.size(); i++ ) {
//			sum += ( ( GestureModel ) this.gesturemodel.elementAt( i ) ).getDefaultProbability() *
//					( ( GestureModel ) this.gesturemodel.elementAt( i ) ).matches( g );
//		}
//
//		int recognized = -1;
//		double recogprob = Integer.MIN_VALUE;
//		double probgesture = 0;
//		double probmodel = 0;
//		for ( int i = 0; i < this.gesturemodel.size(); i++ ) {
//			double tmpgesture = ( ( GestureModel ) this.gesturemodel.elementAt( i ) ).matches( g );
//			double tmpmodel = ( ( GestureModel ) this.gesturemodel.elementAt( i ) ).getDefaultProbability();
//
//			if ( ( ( tmpmodel * tmpgesture ) / sum ) > recogprob ) {
//				probgesture = tmpgesture;
//				probmodel = tmpmodel;
//				recogprob = ( ( tmpmodel * tmpgesture ) / sum );
//				recognized = i;
//			}
//		}
//
//
//		if ( recogprob > 0 && probmodel > 0 && probgesture > 0 && sum > 0 ) {
//
//			return recognized;
//		} else {
//
//			return -1;
//		}
//
//	}
//
//
//	public void reset() {
//		if ( this.gesturemodel.size() > 0 ) {
//			this.gesturemodel = new Vector();
//
//		}
//	}
//
//
//	RecordStore linksDB = null;
//
//
//	public String loadGuestures() {
//		int erroNum = 0;
//		try {
//			linksDB = RecordStore.openRecordStore( "ge_lk", true );
//			erroNum = 1;
//			for ( int i = 0; i < 10; i++ ) {
//				erroNum += 10 * i;
//				byte[] recData = new byte[ 5 ];
//				int len;
//				int recIndex = i + 1;
//				if ( linksDB.getNumRecords() < recIndex ) {
//					break;
//				}
//				if ( linksDB.getRecordSize( recIndex ) > recData.length ) {
//					recData = new byte[ linksDB.getRecordSize( recIndex ) ];
//				}
//				len = linksDB.getRecord( recIndex, recData, 0 );
//				String str = new String( recData, 0, len );
//				erroNum++;
//				String[] aux = new String[ 4 ];
//				int nums = 0;
//				for ( int j = 0; j < aux.length; j++ ) {
//					aux[j] = new String();
//				}
//				if ( str.length() > 0 ) {
//					handleStartEvent( TRAIN );
//				}
//				for ( int j = 0; j < str.length(); j++ ) {
//					if ( ':' == str.charAt( j ) ) {
//						nums++;
//					} else if ( '&' == str.charAt( j ) ) {
//						accelerationReceived( new AccelerationEvent( Double.parseDouble( aux[0] ), Double.parseDouble( aux[1] ), Double.parseDouble( aux[2] ), Double.parseDouble( aux[3] ) ) );
//						nums = 0;
//						for ( int k = 0; k < aux.length; k++ ) {
//							aux[k] = new String();
//						}
//					} else if ( '#' == str.charAt( j ) ) {
//
//						handleStopEvent( TRAIN );
//
//						if ( j + 1 < str.length() ) {
//							handleStartEvent( TRAIN );
//						}
//					} else if ( '*' == str.charAt( j ) ) {
//						break;
//					} else {
//						aux[nums] += str.charAt( j );
//					}
//				}
//				if ( str.length() > 0 ) {
//					handleStartEvent( CLOSE );
//					handleStopEvent( CLOSE );
//				}
//				erroNum++;
//			}
//			linksDB.closeRecordStore();
//			return "ok" + String.valueOf( gesturemodel.size() );
//		} catch ( Exception e1 ) {
//			AppMIDlet.log( e1 );;
//			return "Erro open" + String.valueOf( erroNum );
//		}
//
//	}
//
//
//	public String saveGuestures() {
//		try {
//			RecordStore.deleteRecordStore( "ge_lk" );
//		} catch ( Exception e2 ) {
//			AppMIDlet.log( e2 );;
//		}
//		try {
//			//RecordStore.deleteRecordStore("ge_lk");
//			linksDB = RecordStore.openRecordStore( "ge_lk", true );
//			for ( int i = 0; i < trainSave.length; i++ ) {
//				String str = "";
//				Vector s = ( Vector ) trainSave[i];
//
//				for ( int j = 0; j < s.size(); j++ ) {
//					Vector t = ( ( Gesture ) s.elementAt( j ) ).getData();
//					for ( int k = 0; k < t.size(); k++ ) {
//						str += String.valueOf( ( ( AccelerationEvent ) t.elementAt( k ) ).getX() ) + ":" + String.valueOf( ( ( AccelerationEvent ) t.elementAt( k ) ).getY() ) +
//								":" + String.valueOf( ( ( AccelerationEvent ) t.elementAt( k ) ).getZ() ) + ":" + String.valueOf( ( ( AccelerationEvent ) t.elementAt( k ) ).getAbsValue() ) + "&";
//					}
//					str += "#";
//				}
//				//str += "*";
//				byte[] rec = str.getBytes();
//				linksDB.addRecord( rec, 0, rec.length );
//			}
//			linksDB.closeRecordStore();
//			return "ok";
//		} catch ( Exception e2 ) {
//			AppMIDlet.log( e2 );;
//			return "error";
//		}
//
//	}
//
//
//}
//
