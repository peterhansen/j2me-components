//package br.com.nanogames.components.gestures;
//
//import java.util.Hashtable;
//import java.util.Vector;
//
//
//
//public class Quantizer {
//
//	private double radius;
//	private int states;
//	private java.util.Hashtable map = new java.util.Hashtable();
//
//
//	public Quantizer( int states ) {
//		this.states = states;
//	}
//
//
//	public void trainCenteroids( Gesture gesture ) {
//		Vector data = gesture.getData();
//		double pi = Math.PI;
//		this.radius = ( gesture.getMaxAcceleration() + gesture.getMinAcceleration() ) / 2;
//
//
//		// x , z , y
//		if ( this.map.isEmpty() ) {
//			this.map.put( new Integer( 0 ), new double[]{ this.radius, 0.0, 0.0 } );
//			this.map.put( new Integer( 1 ), new double[]{ Math.cos( pi / 4 ) * this.radius, 0.0, Math.sin( pi / 4 ) * this.radius } );
//			this.map.put( new Integer( 2 ), new double[]{ 0.0, 0.0, this.radius } );
//			this.map.put( new Integer( 3 ), new double[]{ Math.cos( pi * 3 / 4 ) * this.radius, 0.0, Math.sin( pi * 3 / 4 ) * this.radius } );
//			this.map.put( new Integer( 4 ), new double[]{ -this.radius, 0.0, 0.0 } );
//			this.map.put( new Integer( 5 ), new double[]{ Math.cos( pi * 5 / 4 ) * this.radius, 0.0, Math.sin( pi * 5 / 4 ) * this.radius } );
//			this.map.put( new Integer( 6 ), new double[]{ 0.0, 0.0, -this.radius } );
//			this.map.put( new Integer( 7 ), new double[]{ Math.cos( pi * 7 / 4 ) * this.radius, 0.0, Math.sin( pi * 7 / 4 ) * this.radius } );
//
//
//			this.map.put( new Integer( 8 ), new double[]{ 0.0, this.radius, 0.0 } );
//			this.map.put( new Integer( 9 ), new double[]{ 0.0, Math.cos( pi / 4 ) * this.radius, Math.sin( pi / 4 ) * this.radius } );
//
//			this.map.put( new Integer( 10 ), new double[]{ 0.0, Math.cos( pi * 3 / 4 ) * this.radius, Math.sin( pi * 3 / 4 ) * this.radius } );
//			this.map.put( new Integer( 11 ), new double[]{ 0.0, -this.radius, 0.0 } );
//			this.map.put( new Integer( 12 ), new double[]{ 0.0, Math.cos( pi * 5 / 4 ) * this.radius, Math.sin( pi * 5 / 4 ) * this.radius } );
//
//			this.map.put( new Integer( 13 ), new double[]{ 0.0, Math.cos( pi * 7 / 4 ) * this.radius, Math.sin( pi * 7 / 4 ) * this.radius } );
//		}
//
//
//		int[][] g_alt = new int[ this.map.size() ][ data.size() ];
//		int[][] g = new int[ this.map.size() ][ data.size() ];
//
//		do {
//
//			g_alt = this.copyarray( g );
//			g = this.deriveGroups( gesture );
//
//
//			for ( int i = 0; i < this.map.size(); i++ ) {
//				double zaX = 0;
//				double zaY = 0;
//				double zaZ = 0;
//				int ne = 0;
//				for ( int j = 0; j < data.size(); j++ ) {
//					if ( g[i][j] == 1 ) {
//						zaX += ( ( AccelerationEvent ) data.elementAt( j ) ).getX();
//						zaY += ( ( AccelerationEvent ) data.elementAt( j ) ).getY();
//						zaZ += ( ( AccelerationEvent ) data.elementAt( j ) ).getZ();
//						ne++;
//					}
//				}
//				if ( ne > 1 ) {
//					double[] newcenteroid = { ( zaX / ( double ) ne ),
//						( zaY / ( double ) ne ),
//						( zaZ / ( double ) ne )
//					};
//					this.map.put( new Integer( i ), newcenteroid );
//
//				}
//			}
//
//		} while ( !equalarrays( g_alt, g ) );
//
//
//
//	}
//
//
//	public int[][] deriveGroups( Gesture gesture ) {
//		Vector data = gesture.data;
//		int[][] groups = new int[ this.map.size() ][ data.size() ];
//
//
//
//[][] d = new double[ this.map.size() ][ data.size() ];
//		double[] curr = new double[ 3 ];
//		double[] vector = new double[ 3 ];
//		for ( int i = 0; i < this.map.size(); i++ ) {
//			double[] ref = ( double[] ) this.map.get( new Integer( i ) );
//			for ( int j = 0; j < data.size(); j++ ) {
//
//				curr[0] = ( ( AccelerationEvent ) data.elementAt( j ) ).getX();
//				curr[1] = ( ( AccelerationEvent ) data.elementAt( j ) ).getY();
//				curr[2] = ( ( AccelerationEvent ) data.elementAt( j ) ).getZ();
//
//				vector[0] = ref[0] - curr[0];
//				vector[1] = ref[1] - curr[1];
//				vector[2] = ref[2] - curr[2];
//				d[i][j] = Math.sqrt( ( vector[0] * vector[0] ) + ( vector[1] * vector[1] ) + ( vector[2] * vector[2] ) );
//
//			}
//
//		}
//
//
//		for ( int j = 0; j < data.size(); j++ ) {
//			double smallest = Double.MAX_VALUE;
//			int row = 0;
//			for ( int i = 0; i < this.map.size(); i++ ) {
//				if ( d[i][j] < smallest ) {
//					smallest = d[i][j];
//					row = i;
//				}
//				groups[i][j] = 0;
//			}
//			groups[row][j] = 1;
//		}
//
//
//
//		return groups;
//
//	}
//
//
//	public int[] getObservationSequence( Gesture gesture ) {
//		int[][] groups = this.deriveGroups( gesture );
//		Vector sequence = new Vector();
//
//
//
//		for ( int j = 0; j < groups[0].length; j++ ) {
//			for ( int i = 0; i < groups.length; i++ ) {
//				if ( groups[i][j] == 1 ) {
//
//					sequence.addElement( new Integer( i ) );
//					break;
//				}
//			}
//		}
//
//
//		while ( sequence.size() < this.states ) {
//			sequence.addElement( sequence.elementAt( sequence.size() - 1 ) );
//
//		}
//
//
//		int[] out = new int[ sequence.size() ];
//		for ( int i = 0; i < sequence.size(); i++ ) {
//
//			out[i] = ( ( Integer ) sequence.elementAt( i ) ).intValue();
//		}
//
//		return out;
//	}
//
//
//	private int[][] copyarray( int[][] alt ) {
//		int[][] neu = new int[ alt.length ][ alt[0].length ];
//		for ( int i = 0; i < alt.length; i++ ) {
//			for ( int j = 0; j < alt[i].length; j++ ) {
//				neu[i][j] = alt[i][j];
//			}
//		}
//		return neu;
//	}
//
//
//	private boolean equalarrays( int[][] one, int[][] two ) {
//		for ( int i = 0; i < one.length; i++ ) {
//			for ( int j = 0; j < one[i].length; j++ ) {
//				if ( !( one[i][j] == two[i][j] ) ) {
//					return false;
//				}
//			}
//		}
//		return true;
//	}
//
//
//}
//
