///*
// * wiigee - accelerometerbased gesture recognition
// * Copyright (C) 2007, 2008 Benjamin Poppinga
// *
// * Developed at University of Oldenburg
// * Contact: benjamin.poppinga@informatik.uni-oldenburg.de
// *
// * This file is part of wiigee.
// *
// * wiigee is free software; you can redistribute it and/or modify
// * it under the terms of the GNU Lesser General Public License as published by
// * the Free Software Foundation; either version 2 of the License, or
// * (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU Lesser General Public License for more details.
// *
// * You should have received a copy of the GNU Lesser General Public License along
// * with this program; if not, write to the Free Software Foundation, Inc.,
// * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
// */
//package br.com.nanogames.components.gestures;
//
//import java.util.Vector;
//
//
///**
// * This class represents ONE movement trajectory in a
// * concrete instance.
// *
// * @author Benjamin 'BePo' Poppinga
// */
//public class Guesture {
//
//	/** The maximal acceleration this gesture has got. */
//	private double maxacc;
//	/** The minimal acceleration this gesture has got. */
//	private double minacc;
//	/** The complete trajectory as WiimoteAccelerationEvents
//	 * as a vector. It's a vector because we don't want to
//	 * loose the chronology of the stored events.
//	 */
//	Vector data;
//
//
//	/**
//	 * Create an empty Gesture.
//	 */
//	public Guesture() {
//		this.maxacc = Double.MIN_VALUE;
//		this.data = new Vector();
//	}
//
//
//	/**
//	 * Make a deep copy of another Gesture object.
//	 *
//	 * @param original Another Gesture object
//	 */
//	public Guesture( Gesture original ) {
//		this.data = new Vector();
//		Vector origin = original.getData();
//		for ( int i = 0; i < origin.size(); i++ ) {
//			this.add( ( AccelerationEvent ) origin.elementAt( i ) );
//		}
//		this.maxacc = original.getMaxAcceleration();
//		this.minacc = original.getMinAcceleration();
//	}
//
//
//	/**
//	 * Adds a new acceleration event to this gesture.
//	 *
//	 * @param event The WiimoteAccelerationEvent to add.
//	 */
//	public void add( AccelerationEvent event ) {
//		this.data.addElement( event );
//	}
//
//
//	/**
//	 * Returns the last acceleration added to this gesture.
//	 *
//	 * @return the last acceleration event added.
//	 */
//	public AccelerationEvent getLastData() {
//		return ( AccelerationEvent ) this.data.elementAt( this.data.size() - 1 );
//	}
//
//
//	/**
//	 * Returns the whole chronological sequence of accelerations as
//	 * a vector.
//	 *
//	 * @return chronological sequence of accelerations.
//	 */
//	public Vector getData() {
//		return this.data;
//	}
//
//
//	public int getCountOfData() {
//		return this.data.size();
//	}
//
//
//	public void setMaxAcceleration( double maxacc ) {
//		this.maxacc = maxacc;
//	}
//
//
//	public double getMaxAcceleration() {
//		return this.maxacc;
//	}
//
//
//	public void setMinAcceleration( double minacc ) {
//		this.minacc = minacc;
//	}
//
//
//	public double getMinAcceleration() {
//		return this.minacc;
//	}
//
//
//}
//
