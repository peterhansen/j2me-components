/**
 * BasicMenu.java
 * 
 * Created on 30/Jan/2009, 14:55:42
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.events.Event;

/**
 *
 * @author Peter
 */
public abstract class BasicMenu extends NanoOnlineContainer {
	
	protected byte backEntry;

	
	public BasicMenu( int totalItems ) throws Exception {
		this( totalItems, -1 );
	}
	
	
	public BasicMenu( int totalItems, int backEntry ) throws Exception {
		super( totalItems );
		
		this.backEntry = ( byte ) backEntry;
	}
	
	
	public BasicMenu( int[] items ) throws Exception {
		this( items, -1 );
	}
	
	
	public BasicMenu( int[] items, int backEntry ) throws Exception {
		super( items.length );
		
		this.backEntry = ( byte ) backEntry;

		final String[] titles = new String[ items.length ];
		for ( byte i = 0; i < titles.length; ++i )
			titles[ i ] = NanoOnline.getText( items[ i ] );
		
		addItems( titles );
	}
	
	
	public BasicMenu( String[] items, int backEntry ) throws Exception {
		super( items.length );

		this.backEntry = ( byte ) backEntry;
		
		addItems( items );
	}
	
	
	protected final void addItems( String[] items ) throws Exception {
		for ( byte i = 0; i < items.length; ++i ) {
			insertDrawable( NanoOnline.getButton( items[ i ], i, this ) );
		}
	}
	

	public void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				buttonPressed( sourceId );
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						buttonPressed( backEntry );
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )
	
	
	protected abstract void buttonPressed( int index );

	
	public final byte getBackEntry() {
		return backEntry;
	}

}
