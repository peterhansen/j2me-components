/**
 * NanoOnline.java
 * 
 * Created on 30/Out/2008, 16:49:16
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.online.newsfeeder.NewsFeederScreen;
import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.form.SimpleScrollBar;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.borders.TitledBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.util.DynamicByteArray;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Serializable;

//#if JAVA_VERSION == "ANDROID"
//# import java.util.zip.GZIPInputStream;
//#else
    //#if BLACKBERRY_API == "true"
    //# import net.rim.device.api.compress.GZIPInputStream;
    //#else
    import com.tinyline.util.GZIPInputStream;
    //#endif
//#endif

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

//#if AD_MANAGER == "true"
//# 	import br.com.nanogames.components.online.ad.AdManager;
//# 	import br.com.nanogames.components.online.ad.ResourceManager;
//#endif

//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//# import java.awt.Image;
//#endif

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.form.TouchKeyPad;
import java.util.Vector;
//#endif

/**
 *
 * @author Peter
 */
public final class NanoOnline implements NanoOnlineConstants, EventListener {

	/** Assinatura para reconhecimento de um cliente acessando o Nano Online. */
	public static final byte[] NANO_ONLINE_SIGNATURE = { ( byte ) 137, 78, 65, 78, 79, 13, 10, 26, 10 };
	
	/** Versão do cliente Nano Online. */
	public static final String VERSION = "0.0.4";
	
	/** Mutex utilizado para sincronizar o método setScreen(int). */
	protected final Mutex mutexScreen = new Mutex();
	
	/** Índice da tela ativa atualmente. */
	protected int currentScreen = -1;
	
	/***/
	private static String[] texts;
	
	/***/
	private static NanoOnline instance;
	
	/***/
	private final ImageFont[] FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	
	/** Perfil de usuário atualmente ativo. */
	private static Customer currentCustomer = new Customer();

	/***/
	private static Customer[] customers;
	
	/***/
	private static byte language = LANGUAGE_DEFAULT;
	
	/** Form utilizado para mostrar as páginas (contém ainda uma barra de título e outra de status). */
	private final Form form;

	/** Barra de progresso (inferior). */
	private final ProgressBar progressBar;

	private final TitleBar titleBar;

	private static String appShortName;
	
	private int nextScreenIndex;

	/***/
	private static boolean submittingNewRecord;

	public static final byte RANKING_TYPES_ALL			= 0;
	public static final byte RANKING_TYPES_LOCAL_ONLY	= 1;
	public static final byte RANKING_TYPES_GLOBAL_ONLY	= 2;

	private static byte rankingTypes = RANKING_TYPES_ALL;

	
	static {
		//#if DEBUG == "true"
//# 			// apaga as referências estáticas (emulador não reinicia de fato a execução...)
//# 			customers = null;
//# 			instance = null;
		//#endif

		// cria a base de dados do Nano Online e do News Feeder
		try {
			AppMIDlet.createDatabase( DATABASE_NAME, DATABASE_TOTAL_SLOTS );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "21");
			//#endif
		}
	}
	
	
	private NanoOnline( byte language ) throws Exception {
		if ( instance != null )
			unload();
		instance = this;
		
		loadProfiles();
		
		FONTS[ FONT_TEXT ] = ImageFont.createMultiSpacedFont( PATH_NANO_ONLINE_IMAGES + "font_0" );
		FONTS[ FONT_WHITE ] = FONTS[ FONT_TEXT ];

		final PaletteChanger p = new PaletteChanger( PATH_NANO_ONLINE_IMAGES + "font_0.png" );
		final Image img = p.createImage( new PaletteMap[] {
			new PaletteMap( COLOR_FONT_BLACK, COLOR_FONT_WHITE ),
			new PaletteMap( COLOR_FONT_WHITE, COLOR_FONT_BLACK )
		}
		);
		FONTS[ FONT_BLACK ] = ImageFont.createMultiSpacedFont( img, PATH_NANO_ONLINE_IMAGES + "font_0.bin" );

		FONTS[ FONT_SMALL ] = ImageFont.createMultiSpacedFont( "/splash/font_credits" );
		FONTS[ FONT_SMALL ].setCharExtraOffset( 1 );

		form = new Form();
		progressBar = new ProgressBar();
		form.setStatusBar( progressBar, false );

		//#if TOUCH == "true"
			form.setTouchKeyPad( new TouchKeyPad( getFont( FONT_BLACK ), new DrawableImage( PATH_NANO_ONLINE_IMAGES + "clear.png" ), new DrawableImage( PATH_NANO_ONLINE_IMAGES + "shift.png" ) ) );
		//#endif

		titleBar = new TitleBar();
		form.setTitleBar( titleBar );
	}


	/**
	 * 
	 * @param language
	 * @param appShortName
	 */
	public static final void init( byte language, String appShortName ) throws Exception {
		NanoOnline.language = language;
		NanoOnline.appShortName = appShortName;
		
		texts = new String[ TEXTS_TOTAL ];
		NanoOnline.language = language;
		AppMIDlet.loadTexts( texts, PATH_NANO_ONLINE_IMAGES + language + ".dat" );
	}


	/**
	 * Equivalente à chamada de <code>load( backScreenIndex, SCREEN_MAIN_MENU )</code>.
	 * @param backScreenIndex
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Form load( int backScreenIndex ) throws Exception {
		return load( backScreenIndex, SCREEN_MAIN_MENU );
	}
	
	
	/**
	 * 
	 * @param backScreenIndex
	 * @param screenIndex
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Form load( int backScreenIndex, int screenIndex ) throws Exception {
		if ( instance == null )
			instance = new NanoOnline( language );

		// armazena a tela chamada ao sair do Nano Online
		instance.nextScreenIndex = backScreenIndex;
		
		setScreen( screenIndex );
		
		return instance.form;
	}
	
	
	public static final void unload() {
		if ( instance != null ) {
			// necessário para não receber mais eventos após anular as referências internas
			if ( ScreenManager.getKeyListener() == instance.form )
				ScreenManager.setKeyListener( null );
			
			if ( ScreenManager.getScreenListener() == instance.form )
				ScreenManager.setScreenListener( null );

			//#if TOUCH == "true"
				if ( ScreenManager.getPointerListener() == instance.form )
					ScreenManager.setPointerListener( null );
			//#endif
			
			// chamando destroy() explicitamente reduz o risco de vazamento de memória
			instance.form.destroy();
			instance.progressBar.destroy();
			instance.titleBar.destroy();
				
			instance = null;
			
			AppMIDlet.gc();			
		}
	}
	
	
	protected static final void setScreen( int index ) {
		setScreen( index, -1 );
	}
	
	
	/**
	 * Troca a tela ativa atualmente, tratando a sincronização entre 2 possíveis chamadas concorrentes do método. Este
	 * método <b>não</b> troca efetivamente a tela ativa no <code>ScreenManager</code> - essa troca deve ser feita dentro 
	 * do método <code>changeScreen</code>, que deve ser estendido pelas aplicações.
	 * 
	 * @param index índice da nova tela ativa.
	 * @see #changeScreen(int)
	 */
	protected static final void setScreen( int index, int backIndex ) {
		if ( instance.mutexScreen.acquire() ) {
			instance.changeScreen( index, backIndex );
		}
		//#if DEBUG == "true"
//# 		// TODO o que fazer no caso de não conseguir o acesso ao mutex?
//# 		else {
//# 			System.out.println( "AppMIDLet.setScreen: mutexScreen.acquire() retornou false." );
//# 		}
		//#endif	
	} // fim do método setScreen( int )
	
	
	private synchronized final void changeScreen( int index, final int backIndex ) {
		try {
			Container nextScreen = null;

			final byte SOFT_KEY_REMOVE = -1;
			final byte SOFT_KEY_DONT_CHANGE = -2;

			byte indexSoftRight = SOFT_KEY_REMOVE;
			byte indexTitle = TEXT_BACK;

			switch ( index ) {
				case SCREEN_MAIN_MENU:
					setSubmittingNewRecord( false );
					
					nextScreen = CreateMainMenu();
					indexSoftRight = TEXT_EXIT;
					indexTitle = TEXT_NANO_ONLINE;
				break;
				
				case SCREEN_LOGIN:
				case SCREEN_SYNC_CUSTOMER_INFO:
					nextScreen = new LoginScreen( ProfileScreen.getProfileIndex(), index, backIndex );
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_LOGIN;
				break;

				case SCREEN_LOAD_PROFILE:
					nextScreen = new LoginScreen( Customer.ID_NONE, -1, backIndex );
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_IMPORT_PROFILE;
				break;

				case SCREEN_PROFILE_SELECT:
					nextScreen = new ProfileSelector( backIndex );
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_PROFILES;
				break;

				case SCREEN_REGISTER_PROFILE:
					nextScreen = new ProfileScreen( true, true, backIndex );
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_PROFILES;
				break;

				case SCREEN_PROFILE_VIEW:
				case SCREEN_PROFILE_EDIT:
					nextScreen = new ProfileScreen( false, index == SCREEN_PROFILE_EDIT, backIndex );
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_PROFILES;
				break;

				case SCREEN_HELP_MENU:
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_HELP_TITLE;
					
					final int[] helpEntries = new int[] { 
						TEXT_HELP_TITLE_COMMANDS,
						TEXT_HELP_TITLE_RANKING,
						TEXT_HELP_TITLE_TAXES,
						TEXT_HELP_TITLE_PRIVACY,
						TEXT_HELP_TITLE_SUPPORT,
						TEXT_BACK,
					};

					nextScreen = new BasicMenu( helpEntries, helpEntries.length - 1 ) {
						protected final void buttonPressed( int index ) {
							switch ( index ) {
								case 0:
								case 1:
								case 2:
								case 3:
								case 4:
									NanoOnline.setScreen( SCREEN_HELP_COMMANDS + index, SCREEN_HELP_MENU );
								break;

								case 5:
									NanoOnline.setScreen( backIndex >= 0 ? backIndex : SCREEN_MAIN_MENU );
								break;
							}
						}
					};
					( ( BasicMenu ) nextScreen ).setBackIndex( SCREEN_MAIN_MENU );
				break;

				case SCREEN_HELP_COMMANDS:
				case SCREEN_HELP_RANKING:
				case SCREEN_HELP_SUPPORT:
				case SCREEN_HELP_TAXES:
				case SCREEN_HELP_PRIVACY:
					nextScreen = CreateHelpScreen( getText( TEXT_HELP_TEXT_COMMANDS + index - SCREEN_HELP_COMMANDS ) );
					indexSoftRight = TEXT_BACK;
					indexTitle = ( byte ) ( TEXT_HELP_TITLE_COMMANDS + index - SCREEN_HELP_COMMANDS );
				break;

				case SCREEN_NEW_RECORD:
					setSubmittingNewRecord( true );
					
					nextScreen = CreateNewRecordScreen();
					indexSoftRight = TEXT_OK;
					indexTitle = TEXT_RECORDS;
				break;
				
				case SCREEN_RECORDS:
					switch ( rankingTypes ) {
						case RANKING_TYPES_GLOBAL_ONLY:
							nextScreen = new RankingMenu( RankingMenu.RANKING_MENU_GLOBAL, SCREEN_MAIN_MENU );
						break;

						case RANKING_TYPES_LOCAL_ONLY:
							nextScreen = new RankingMenu( RankingMenu.RANKING_MENU_LOCAL, SCREEN_MAIN_MENU );
						break;

						default:
							nextScreen = new RankingMenu( RankingMenu.RANKING_MENU_ALL, SCREEN_MAIN_MENU );
					}
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_RECORDS;
				break;
				
				case SCREEN_ENTER_LOCAL_NAME:
					nextScreen = new LocalRankingScreen();
					indexSoftRight = TEXT_BACK;
					indexTitle = TEXT_PROFILES;
				break;

				case SCREEN_NEWS:
					nextScreen = new NewsFeederScreen( SCREEN_MAIN_MENU );
					indexTitle = TEXT_NEWS;
					indexSoftRight = TEXT_BACK;
				break;
				
				//#if DEBUG == "true"
//# 				default:
//# 					throw new IllegalArgumentException( "Invalid screen index: " + index );
				//#endif
			} // fim switch ( index )

			switch ( indexSoftRight ) {
				case SOFT_KEY_REMOVE:
					getProgressBar().setSoftKey( null );
				case SOFT_KEY_DONT_CHANGE:
				break;
				
				default:
					getProgressBar().setSoftKey( getText( indexSoftRight ) );
			}

			//#if DEBUG == "true"
//# 				if ( nextScreen == null )
//# 					throw new NullPointerException( "Nano Online: nextScreen is null." );
			//#endif
			
			getProgressBar().clearProgressBar();
			
			nextScreen.setId( index );
			form.setContentPane( nextScreen );
			setTitle( indexTitle );

			currentScreen = index;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			//# 				AppMIDlet.log( e , "22");
			//#endif

			return;
		} finally {
			mutexScreen.release();
		}
	}


	public static final void setTitle( int titleIndex ) {
		setTitle( getText( titleIndex ) );
	}


	/**
	 * Define o título da barra superior.
	 * @param title título da barra superior.
	 */
	public static final void setTitle( String title ) {
		instance.titleBar.setText( title );
	}
	
	
	/**
	 * Descarrega o Nano Online e volta à tela anterior ao Nano Online.
	 */
	public static final void exit() {
		// armazena a referência antes de unload(), pois instance é anulado
		final int nextScreen = instance.nextScreenIndex;
		unload();
		AppMIDlet.setScreen( nextScreen );
	}
	
	
	/**
	 * 
	 * @param index
	 * @return
	 */
	public static final String getText( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
				return instance.texts[ index ];
		
		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			System.out.println( "invalid text index: " + index );
//# 			AppMIDlet.log( e , "23");
//# 			return null;
//# 		}
		//#endif
	}
	

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static final ImageFont getFont( int index ) {
		//#if DEBUG == "true"
//# 		try {
		//#endif
		
		return instance.FONTS[ index ];

		//#if DEBUG == "true"
//# 		} catch ( Exception e ) {
//# 			AppMIDlet.log( e , "24");
//# 			return null;
//# 		}
		//#endif		
	}


	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final ScrollBar getScrollBarV() throws Exception  {
		return new NanoOnlineScrollBar( ScrollBar.TYPE_VERTICAL );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final ScrollBar getScrollBarH() throws Exception  {
		return new NanoOnlineScrollBar( ScrollBar.TYPE_HORIZONTAL );
	}


	/**
	 * Obtém o idioma atual do Nano Online.
	 * @return
	 */
	public static final byte getLanguage() {
		return language;
	}


	public static final Border getSimpleBorder( boolean white ) throws Exception {
		return new NanoOnlineSimpleBorder( white );
	}


	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Border getBorder() throws Exception {
		return getBorder( BORDER_COLOR_TYPE_DEFAULT );
	}
	
	
	public static final Border getBorder( int colorType ) throws Exception {
		return new NanoOnlineBorder( colorType );
	}


	// TODO criar bordas a partir de arquivos descritores
	private static final class NanoOnlineBorder extends LineBorder {
		
		private static final int[] COLOR_PRESSED_BORDER		= { 0x003f46, 0xee7b00 };
		private static final int[] COLOR_PRESSED_FILL		= { 0x038392, 0xff9b00 };
		private static final int[] COLOR_FOCUSED_BORDER		= { 0x038392, 0xff9b00 };
		private static final int[] COLOR_FOCUSED_FILL		= { 0x7dbfc7, 0xffc300 };
		private static final int[] COLOR_ERROR				= { 0xff0000, 0x0000ff };
		private static final int[] COLOR_WARNING			= { 0xaaaa00, 0x00aaaa };
		private static final int[] COLOR_UNFOCUSED_BORDER	= { 0x818181, 0xffc300 };
		private static final int[] COLOR_UNFOCUSED_FILL		= { 0xbebebe, 0xffda4b };
		
		private byte colorType;
		

		public NanoOnlineBorder() throws Exception {
			this( BORDER_COLOR_TYPE_DEFAULT );
		}

		
		public NanoOnlineBorder( int colorType ) throws Exception {
			super( 0x818181, LineBorder.TYPE_ROUND_RAISED );
			
			setColorType( colorType );
		}
		
		
		public final void setColorType( int colorType ) {
			this.colorType = ( byte ) colorType;
			setState( getState() );
		}
		
		
		public final byte getColorType() {
			return colorType;
		}

		
		public final void setState( int state ) {
			super.setState( state );

			switch ( state ) {
				case STATE_PRESSED:
					setColor( COLOR_PRESSED_BORDER[ colorType ] );
					setFillColor( COLOR_PRESSED_FILL[ colorType ] );
				break;

				case STATE_ERROR:
					setColor( COLOR_ERROR[ colorType ] );
				break;

				case STATE_WARNING:
					setColor( COLOR_WARNING[ colorType ] );
				break;

				case STATE_FOCUSED:
					setColor( COLOR_FOCUSED_BORDER[ colorType ] );
					setFillColor( COLOR_FOCUSED_FILL[ colorType ] );
				break;

				case STATE_UNFOCUSED:
				default:
					setColor( COLOR_UNFOCUSED_BORDER[ colorType ] );
					setFillColor( COLOR_UNFOCUSED_FILL[ colorType ] );
				break;
			}
		}


		public final Border getCopy() throws Exception {
			return new NanoOnlineBorder( colorType );
		}

	}
	

	/**
	 * 
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final LineBorder getRankingBorder( int type ) throws Exception {
		final int COLOR_SELECTED = type == 0 ? 0xbff7fd : 0x98f1fa;
		final LineBorder border = new LineBorder( COLOR_SELECTED, LineBorder.TYPE_SIMPLE ) {
			public final void setState( int state ) {
				switch ( state ) {
					case STATE_FOCUSED:
						setColor( 0x90d5dc );
						setFillColor( 0x90d5dc );
					break;

					default:
						setColor( COLOR_SELECTED );
						setFillColor( COLOR_SELECTED );
				}
			}
		};

		return border;
	}


	private static final Border getTextBoxBorder() throws Exception {
		final LineBorder l = new LineBorder();

		l.setType( LineBorder.TYPE_SIMPLE );

		return l;
	}


	private static final class NanoOnlineSimpleBorder extends LineBorder {

		private static final int COLOR_PRESSED_BORDER	= 0x003f46;
		private static final int COLOR_PRESSED_FILL		= 0x038392;
		private static final int COLOR_FOCUSED_BORDER	= 0x034362;
		private static final int COLOR_FOCUSED_FILL		= 0x7dbfc7;
		private static final int COLOR_ERROR			= 0xff0000;
		private static final int COLOR_WARNING			= 0xaaaa00;
		private static final int COLOR_UNFOCUSED_BORDER	= 0x818181;
		private static final int COLOR_UNFOCUSED_FILL	= 0xbebebe;

		private final boolean white;


		public NanoOnlineSimpleBorder() throws Exception {
			this( false );
		}


		public NanoOnlineSimpleBorder( boolean white ) throws Exception {
			super( white ? 0xffffff : COLOR_UNFOCUSED_FILL, LineBorder.TYPE_ROUND_SIMPLE );

			this.white = white;
		}


		public final void setState( int state ) {
			super.setState( state );

			switch ( state ) {
				case STATE_PRESSED:
					setColor( COLOR_PRESSED_BORDER );
					setFillColor( COLOR_PRESSED_FILL );
				break;

				case STATE_ERROR:
					setColor( COLOR_ERROR );
				break;

				case STATE_WARNING:
					setColor( COLOR_WARNING );
				break;

				case STATE_FOCUSED:
					setColor( COLOR_FOCUSED_BORDER );
					setFillColor( COLOR_FOCUSED_FILL );
				break;

				case STATE_UNFOCUSED:
				default:
					setColor( COLOR_UNFOCUSED_BORDER );
					setFillColor( white ? 0xffffff : COLOR_UNFOCUSED_FILL );
				break;
			}
		}


		public final Border getCopy() throws Exception {
			return new NanoOnlineSimpleBorder( white );
		}

	}


	/**
	 * 
	 * @param titleIndex
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final TitledBorder getTitledBorder( int titleIndex ) throws Exception {
		return getTitledBorder( getText( titleIndex ) );
	}


	public static final TitledBorder getTitledBorder( int titleIndex, Border b ) throws Exception {
		return getTitledBorder( getText( titleIndex ), b );
	}


	public static final TitledBorder getTitledBorder( String title ) throws Exception {
		return getTitledBorder( title, getBorder() );
	}


	public static final TitledBorder getTitledBorder( String title, Border b ) throws Exception {
		return new TitledBorder( new Label( getFont( FONT_BLACK ), title ), b );
	}


	public static final Button getCheckBoxButton() throws Exception {
		final Button button = new Button( getFont( FONT_BLACK ), " " ) {
			protected final void setState( int state ) {
				super.setState( state );
				switch ( state ) {
					case STATE_PRESSED:
						setFont( getFont( FONT_WHITE ) );
					break;

					default:
						setFont( getFont( FONT_BLACK ) );
				}
			}

		};

		button.setBorder( new NanoOnlineSimpleBorder( true ) );

		return button;
	}


	public static final Button getButton( int textIndex, int id, EventListener listener ) throws Exception {
		return getButton( getText( textIndex ), id, listener );
	}


	public static final Button getButton( String text, int id, EventListener listener ) throws Exception {
		final Button button = new Button( getFont( FONT_BLACK ), text ) {
			protected final void setState( int state ) {
				super.setState( state );
				switch ( state ) {
					case STATE_PRESSED:
						setFont( getFont( FONT_WHITE ) );
					break;

					default:
						setFont( getFont( FONT_BLACK ) );
				}
			}


			public final void setFocus( boolean focus ) {
				super.setFocus( focus );

				if ( focus ) {
					label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
				} else {
					label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_NONE );
					label.setTextOffset( 0 );
				}
			}
			
			
			public final void setSize( int width, int height ) {
				if ( label != null ) {
					super.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ), 
								   label.getHeight() + ( getBorder() == null ? 0 : getBorder().getBorderHeight() ) );
				}
			}

		};
		button.setId( id );

		if ( listener != null )
			button.addEventListener( listener );
		button.setBorder( getBorder() );
		button.setSize( Math.max( ScreenManager.SCREEN_WIDTH * 7 / 10, BUTTON_MIN_WIDTH ), button.getHeight() + button.getBorder().getBorderHeight() );

		return button;
	}


	/**
	 * 
	 * @param maxChars
	 * @param inputMode
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final TextBox getTextBox( int id, int maxChars, byte inputMode, int titleIndex ) throws Exception {
		return getTextBox( id, maxChars, inputMode, getText( titleIndex ) );
	}


	public static final TextBox getTextBox( int id, int maxChars, byte inputMode, String title ) throws Exception {
		final TextBox textBox = new TextBox( getFont( FONT_BLACK ), null, maxChars, inputMode, true );

		textBox.setId( id );
		textBox.setCaret( getCaret() );
		textBox.setBorder( getTitledBorder( title, new NanoOnlineSimpleBorder() ) );
		textBox.addEventListener( instance.titleBar );

		return textBox;
	}
	

	/**
	 *
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Drawable getCaret() throws Exception {
		return new DrawableImage( PATH_NANO_ONLINE_IMAGES + "d.png" );
	}
	

	/**
	 * Obtém a referência para a barra de progresso do Nano Online.
	 * @return referência para a barra de progresso do Nano Online.
	 */
	public static final ProgressBar getProgressBar() {
		return instance.progressBar;
	}
	

	/**
	 *
	 * @return
	 */
	public static final Form getForm() {
		return instance.form;
	}


	public static final byte getRankingTypes() {
		return rankingTypes;
	}


	public static final void setRankingTypes( byte types ) {
		NanoOnline.rankingTypes = types;
	}


	protected static final boolean isSubmittingNewRecord() {
		return submittingNewRecord;
	}


	protected static final void setSubmittingNewRecord( boolean submittingNewRecord ) {
		NanoOnline.submittingNewRecord = submittingNewRecord;
	}
	

	/**
	 * Obtém o perfil de usuário ativo atualmente.
	 * @return referência para o <code>Customer</code> ativo. Obs.: o usuário pode ser inválido, ou seja, com id menor que 0.
	 */
	public static final Customer getCurrentCustomer() {
		loadProfiles();
		
		return currentCustomer;
	}
	

	/**
	 *
	 * @param index
	 * @return
	 */
	public static final Customer getCustomer( int index ) {
		loadProfiles();
		
		try {
			return customers[ index ];
		} catch ( Exception e ) {
			return null;
		}
	}
	

	/**
	 *
	 * @param id
	 * @return
	 */
	public static final Customer getCustomerById( int id ) {
		loadProfiles();
		
		for ( byte i = 0; i < customers.length; ++i ) {
			if ( customers[ i ].getId() == id )
				return customers[ i ];
		}
		
		return null;
	}
	

	/**
	 * 
	 * @return
	 */
	public static final Customer[] getCustomers() {
		loadProfiles();
		return customers;
	}


	/**
	 * 
	 * @param id
	 * @return
	 */
	public static final boolean isCustomerSaved( int id ) {
		return getCustomerById( id ) != null;
	}
	

	/**
	 *
	 * @param index
	 */
	public static final void setCurrentCustomerIndex( int index ) {
		if ( index < 0 || index > customers.length )
			currentCustomer = new Customer();
		else
			currentCustomer = customers[ index ];

		saveProfiles();
	}


	/**
	 *
	 * @param id
	 * @return 
	 */
	public static final boolean setCurrentCustomerById( int id ) {
		for ( byte i = 0; i < customers.length; ++i ) {
			if ( customers[ i ].getId() == id ) {
				setCurrentCustomerIndex( i );
				return true;
			}
		}

		return false;
	}


	/**
	 * Obtém a lista de jogadores cujos prazos para validação de e-mail estão possivelmente encerrados (de acordo com a
	 * informação no aparelho).
	 *
	 * @return array dos ids possivelmente expirados, ou null caso não haja perfis nessa situação.
	 */
	public static final int[] getExpiredCustomerIds() {
		loadProfiles();

		if ( customers != null ) {
			final int[] temp = new int[ customers.length ];
			byte count = 0;

			for ( byte i = 0; i < customers.length; ++i ) {
				if ( customers[ i ].hasExpirationDate() )
					temp[ count++ ] = customers[ i ].getId();
			}

			final int[] ret = new int[ count ];
			System.arraycopy( temp, 0, ret, 0, count );

			return ret;
		}

		return null;
	}


	/**
	 * 
	 * @param output
	 * @throws java.io.IOException
	 */
	public static final void writeGlobalData( DataOutputStream output ) throws Exception {
		writeGlobalData( output, currentCustomer == null ? Customer.ID_NONE : currentCustomer.getId() );
	}


	/**
	 * Escreve o cabeçalho genérico de uma conexão ao Nano Online.
	 * @param output
	 * @throws java.io.IOException
	 */
	public static final void writeGlobalData( DataOutputStream output, int customerId ) throws Exception {
		// grava o identificador global de acesso
		output.write( NANO_ONLINE_SIGNATURE );

		output.writeByte( ID_NANO_ONLINE_VERSION );
		output.writeUTF( VERSION );

		output.writeByte( ID_APP );
		output.writeUTF( appShortName );

		output.writeByte( ID_CUSTOMER_ID );
		output.writeInt( customerId );

		output.writeByte( ID_LANGUAGE );
		output.writeByte( language );

		output.writeByte( ID_APP_VERSION );
		output.writeUTF( AppMIDlet.getMIDletVersion() );
		
		output.writeByte( ID_CLIENT_LOCAL_TIME );
		output.writeLong( System.currentTimeMillis() );

		// TODO adicionar taxa de atualização de anúncios, notícias, etc., para evitar que todo acesso resulte em envio de informações extras
		
		// grava as informações do news feeder "por baixo dos panos"
		NewsFeederManager.writeGlobalData( output );

		// TODO enviar rankings automaticamente ao conectar o Nano Online

		//#if AD_MANAGER == "true"
//# 			// grava as informações do gerenciador de anúncios
//# 			AdManager.writeGlobalData( output ); //TODO
		//#endif

		// informa ao servidor os perfis possivelmente expirados, para verificar se já foram validados
		// TODO enviar somente uma vez por dia ou por sessão, para evitar troca desnecessária de informações
		final int[] expiredCustomers = getExpiredCustomerIds();
		if ( expiredCustomers != null && expiredCustomers.length > 0 ) {
			output.writeByte( ID_EXPIRED_PROFILE_IDS );
			//#if DEBUG == "true"
//# 				System.out.println( "ENVIANDO " + expiredCustomers.length + " PERFIS EXPIRADOS:" );
			//#endif
			output.writeByte( expiredCustomers.length );
			for ( byte i = 0; i < expiredCustomers.length; ++i ) {
				//#if DEBUG == "true"
//# 					System.out.println( i + " -> " + expiredCustomers[ i ] );
				//#endif
				output.writeInt( expiredCustomers[ i ] );
			}
		}

		// o identificador dos dados específicos sempre devem ser o último header genérico
		output.writeByte( ID_SPECIFIC_DATA );
	}
	

	/**
	 * 
	 * @param data
	 * @return
	 * @throws java.lang.Exception
	 */
	public static final Hashtable readGlobalData( byte[] data ) throws Exception {
		// a partir da versão 0.0.4, os dados recebidos estão comprimidos com GZip
		final DataInputStream input = new DataInputStream( new GZIPInputStream( new ByteArrayInputStream( data ) ) );
		final Hashtable t = new Hashtable();
		
		try {
			while ( input.available() > 0 ) {
				final byte id = input.readByte();

				//#if DEBUG == "true"
//# 					System.out.println ( "NanoOnline.readGlobalData -> LEU ID: " + id );
				//#endif

				switch ( id ) {
					case ID_APP:
					case ID_RETURN_CODE:
						t.put( new Byte( id ), new Short( input.readShort() ) );
					break;

					case ID_CUSTOMER_ID:
						t.put( new Byte( id ), new Integer( input.readInt() ) );
					break;

					case ID_LANGUAGE:
						t.put( new Byte( id ), new Byte( input.readByte() ) );
					break;

					case ID_APP_VERSION:
					case ID_NANO_ONLINE_VERSION:
					case ID_ERROR_MESSAGE:
						t.put( new Byte( id ), input.readUTF() );
					break;

					case ID_NEWS_FEEDER_DATA:
						NewsFeederManager.readGlobalData( input );
					break;

					//#if AD_MANAGER == "true"
//# 						case ID_AD_SERVER_DATA:
//# 							AdManager.readGlobalData( input );
//# 						break;
//#
//# 						case ID_RESOURCE_DATA:
//# 							ResourceManager.parseParams( input );
//# 						break;
					//#endif

					case ID_EXPIRED_PROFILE_IDS:
						// atualiza a situação dos perfis, caso tenha havido mudança nas datas de expiração
						final byte totalProfiles = input.readByte();
						//#if DEBUG == "true"
//# 							System.out.println( "LENDO " + totalProfiles + " PERFIS EXPIRADOS:" );
						//#endif

						for ( byte i = 0; i < totalProfiles; ++i ) {
							final int customerId = input.readInt();
							final long validateBefore = input.readLong();

							//#if DEBUG == "true"
//# 								System.out.println( i + " -> " + customerId + ", " + validateBefore );
							//#endif
								
							getCustomerById( customerId ).setValidateBefore( validateBefore );
						}
						saveProfiles();
					break;

					case ID_SPECIFIC_DATA:
						final DynamicByteArray d = new DynamicByteArray();
						d.readInputStream( input );
						t.put( new Byte( id ), d.getData() );
					break;
				}
			} 
		} catch ( EOFException e ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "25");
			//#endif
		}

		//#if DEBUG == "true"
//# 			final Enumeration e = t.keys();
//# 			while ( e.hasMoreElements() ) {
//# 				Object next = e.nextElement();
//# 				System.out.println( "BLA: " + next + " -> " + t.get( next ) );
//# 			}
		//#endif
		
		return t;
	}
	

	/**
	 * Carrega os perfis de jogadores salvos no aparelho.
	 */
	private static final void loadProfiles() {
		if ( customers == null ) {
			try {
				AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_PROFILES, new Serializable() {

					public final void write( DataOutputStream output ) throws Exception {
					}


					public final void read( DataInputStream input ) throws Exception {
						// lê a quantidade de perfis ativos
						final byte TOTAL_PROFILES = input.readByte();
						customers = new Customer[ TOTAL_PROFILES ];

						if ( TOTAL_PROFILES > 0 ) {
							// lê o perfil ativo atualmente
							final byte CURRENT_PROFILE_INDEX = input.readByte();
							
							for ( byte i = 0; i < customers.length; ++i ) {
								customers[ i ] = new Customer();
								customers[ i ].read( input );
							}

							if ( CURRENT_PROFILE_INDEX >= 0 )
								currentCustomer = customers[ CURRENT_PROFILE_INDEX ];
						}
					}
				} );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "26");
				//#endif

				// verifica se todos os perfis foram carregados
				if ( customers == null ) {
					customers = new Customer[ 0 ];
				} else {
					for ( byte i = 0; i < customers.length; ++i ) {
						if ( customers[ i ] == null ) {
							removeProfile( i, false );
						}
					}
				}
				
				saveProfiles();
			}
		}
	}	
	
	
	/**
	 * Insere um novo perfil de usuário no final do array de perfis, e salva as alterações no RMS. Caso o perfil
	 * já esteja no aparelho, apenas grava os perfis já existentes.
	 * @param c
	 * @throws ArrayStoreException caso já tenha atingido o limite de perfis gravados no celular.
	 */
	public static final void addProfile( Customer c ) {
		//#if DEBUG == "true"
//# 		if ( c == null )
//# 			throw new IllegalArgumentException( "customer can't be null" );
//# 			System.out.println( "add Customer: " + c.getId() + ", " + c.getNickname() );
		//#endif
		
		loadProfiles();
		
		// verifica se o perfil já está gravado no aparelho para evitar entradas duplicadas
		for ( byte i = 0; i < customers.length; ++i ) {
			if ( customers[ i ].getId() == c.getId() ) {
				// atualiza os dados do usuário
				customers[ i ] = c;
				setCurrentCustomerIndex( i );
				saveProfiles();
				return;
			}
		}

		final Customer[] temp = customers;
		if ( temp.length == MAX_PROFILES ) {
			throw new ArrayStoreException();
		}
		customers = new Customer[ temp.length + 1 ];
		System.arraycopy( temp, 0, customers, 0, temp.length );
		
		customers[ customers.length - 1 ] = c;

		setCurrentCustomerIndex( customers.length - 1 );
		saveProfiles();
	}
	

	/**
	 * Remove um perfil que estava salvo no aparelho.
	 * @param c perfil que será removido do aparelho.
	 */
	public static final void removeProfile( Customer c, boolean saveProfiles ) {
		for ( byte i = 0; i < customers.length; ++i ) {
			if ( customers[ i ].getId() == c.getId() ) {
				removeProfile( i, saveProfiles );
				return;
			}
		}		
	}
	

	/**
	 * Remove um perfil que estava salvo no aparelho.
	 * @param index índice do perfil no array de perfis salvos no aparelho.
	 */
	public static final void removeProfile( int index, boolean saveProfiles ) {
		for ( int j = index; j < customers.length - 1; ++j ) {
			customers[ j ] = customers[ j + 1 ];
		}
		final Customer[] temp = customers;
		customers = new Customer[ temp.length - 1 ];
		System.arraycopy( temp, 0, customers, 0, customers.length );

		setCurrentCustomerIndex( getCurrentProfileIndex() );

		if ( saveProfiles )
			saveProfiles();
	}


	private static final byte getCurrentProfileIndex() {
		for ( byte i = 0; i < customers.length; ++i ) {
			if ( customers[ i ].getId() == currentCustomer.getId() )
				return i;
			break;
		}

		return -1;
	}
	

	/**
	 * Salva os perfis de usuários no aparelho.
	 */
	public static final void saveProfiles() {
		try {
			AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_PROFILES, new Serializable() {

				public final void write( DataOutputStream output ) throws Exception {
					//#if DEBUG == "true"
//# 						System.out.println( "Gravando " + ( customers == null ? 0 : customers.length ) + "perfil(s) de usuários" );
					//#endif
					
					// quantidade de perfis ativos
					output.writeByte( customers.length );
					// índice do perfil ativo
					output.writeByte( getCurrentProfileIndex() );

					for ( byte i = 0; i < customers.length; ++i ) {
						//#if DEBUG == "true"
//# 								System.out.println( "Perfil #" + i + ":" );
						//#endif
						customers[ i ].write( output );
					}
				}


				public final void read( DataInputStream input ) throws Exception {
				}
			} );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e , "27" );
			//#endif				
		}
	}


	// <editor-fold defaultstate="collapsed" desc="IDS DE EVENTOS">
	// ENTRADAS DA TELA DE NOVO RECORDE
	private static final byte ENTRY_NEW_RECORD_BUTTON_USE_PROFILE		= 0;
	private static final byte ENTRY_NEW_RECORD_BUTTON_CHOOSE_PROFILE	= 1;
	private static final byte ENTRY_NEW_RECORD_BUTTON_SAVE_LOCAL		= 2;
	private static final byte ENTRY_NEW_RECORD_BUTTON_CREATE_PROFILE	= 3;
	private static final byte ENTRY_NEW_RECORD_BUTTON_LOAD_EXISTING		= 4;
	
	/** Id genérico para as telas de ajuda (voltam ao menu de ajuda). */
	private static final byte ENTRY_HELP_TEXT							= 5;

	// ENTRADAS DO MENU PRINCIPAL
	private static final byte ENTRY_MAIN_MENU_PROFILES					= 6;
	private static final byte ENTRY_MAIN_MENU_RECORDS					= ENTRY_MAIN_MENU_PROFILES + 1;
	private static final byte ENTRY_MAIN_MENU_NEWS						= ENTRY_MAIN_MENU_RECORDS + 1;
	private static final byte ENTRY_MAIN_MENU_HELP						= ENTRY_MAIN_MENU_NEWS + 1;
	private static final byte ENTRY_MAIN_MENU_EXIT						= ENTRY_MAIN_MENU_HELP + 1;

	// </editor-fold>

	
	private static final NanoOnlineContainer CreateNewRecordScreen() throws Exception {
		// TODO para poder criar telas diretamente daqui, é necessário que cada entrada tenha seu próprio id
		final NanoOnlineContainer newRecordScreen = new NanoOnlineContainer( 10 );
		newRecordScreen.setId( SCREEN_NEW_RECORD );

		final ImageFont font = getFont( FONT_BLACK );

		final FormText text = new FormText( font );
		if ( customers == null ) {
			// não há jogadores salvos no aparelho
			text.setText( getText( TEXT_NO_PROFILES_FOUND ) );
			newRecordScreen.insertDrawable( text );

			final Button buttonCreate = getButton( TEXT_CREATE_PROFILE, ENTRY_NEW_RECORD_BUTTON_CREATE_PROFILE, instance );
			newRecordScreen.insertDrawable( buttonCreate );

			final Button buttonLoad = getButton( TEXT_IMPORT_PROFILE, ENTRY_NEW_RECORD_BUTTON_LOAD_EXISTING, instance );
			newRecordScreen.insertDrawable( buttonLoad );
		} else {
			// há perfis salvos no aparelho - mostra o perfil atual e opções para escolher/registrar outro
			text.setText( getText( TEXT_CURRENT_PROFILE ) + ": " + getCurrentCustomer().getNickname() );
			newRecordScreen.insertDrawable( text );

			final Button buttonUse = getButton( TEXT_USE_THIS_PROFILE, ENTRY_NEW_RECORD_BUTTON_USE_PROFILE, instance );
			newRecordScreen.insertDrawable( buttonUse );

			final Button buttonChoose = getButton( TEXT_CHOOSE_PROFILE, ENTRY_NEW_RECORD_BUTTON_CHOOSE_PROFILE, instance );
			newRecordScreen.insertDrawable( buttonChoose );

			final Button buttonLoad = getButton( TEXT_IMPORT_PROFILE, ENTRY_NEW_RECORD_BUTTON_LOAD_EXISTING, instance );
			newRecordScreen.insertDrawable( buttonLoad );
		}

		text.setFocusable( false );

		if ( getRankingTypes() != RANKING_TYPES_GLOBAL_ONLY ) {
			final Button buttonSaveLocal = getButton( TEXT_SAVE_LOCAL_RECORD, ENTRY_NEW_RECORD_BUTTON_SAVE_LOCAL, instance );
			newRecordScreen.insertDrawable( buttonSaveLocal );
		}

		return newRecordScreen;
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ENTRY_NEW_RECORD_BUTTON_USE_PROFILE:
						try {
						//TODO	RankingScreen.setHighScore( RankingScreen.getLastType(), NanoOnline.getCurrentCustomer().getId(), RankingScreen.getLastScore(), RankingScreen.getLastDecrescent(), RankingScreen.getLastCumulative() );
//							setScreen( SCREEN_RECORDS, ( ( NanoOnlineContainer ) evt.source ).getBackIndex() ); TODO necessário?
							setScreen( SCREEN_RECORDS, SCREEN_NEW_RECORD );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								AppMIDlet.log( e , "28");
							//#endif
//							( ( NanoOnlineContainer ) evt.source ).onBack(); TODO necessário?
						}
					break;

					case ENTRY_NEW_RECORD_BUTTON_CHOOSE_PROFILE:
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT, SCREEN_NEW_RECORD );
					break;

					case ENTRY_NEW_RECORD_BUTTON_SAVE_LOCAL:
						NanoOnline.setScreen( SCREEN_ENTER_LOCAL_NAME, SCREEN_NEW_RECORD );
					break;

					case ENTRY_NEW_RECORD_BUTTON_CREATE_PROFILE:
						NanoOnline.setScreen( SCREEN_REGISTER_PROFILE, SCREEN_NEW_RECORD );
					break;

					case ENTRY_NEW_RECORD_BUTTON_LOAD_EXISTING:
						NanoOnline.setScreen( SCREEN_LOAD_PROFILE, SCREEN_NEW_RECORD );
					break;

					// MENU PRINCIPAL
					case ENTRY_MAIN_MENU_PROFILES:
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT, isSubmittingNewRecord() ? SCREEN_NEW_RECORD : SCREEN_MAIN_MENU );
					break;

					case ENTRY_MAIN_MENU_RECORDS:
						NanoOnline.setScreen( SCREEN_RECORDS, SCREEN_MAIN_MENU );
					break;

					case ENTRY_MAIN_MENU_NEWS:
						NanoOnline.setScreen( SCREEN_NEWS, SCREEN_MAIN_MENU );
					break;

					case ENTRY_MAIN_MENU_HELP:
						NanoOnline.setScreen( SCREEN_HELP_MENU, SCREEN_MAIN_MENU );
					break;

					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_MAIN_MENU_EXIT:
						exit();
					break;
				}
			break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( sourceId ) {
					case ENTRY_HELP_TEXT:
						switch ( key ) {
							case ScreenManager.KEY_BACK:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.FIRE:
							case ScreenManager.KEY_NUM5:
							case ScreenManager.KEY_SOFT_LEFT:
								setScreen( SCREEN_HELP_MENU );
							break;
						}
					break;

					case ENTRY_MAIN_MENU_PROFILES:
					case ENTRY_MAIN_MENU_RECORDS:
					case ENTRY_MAIN_MENU_NEWS:
					case ENTRY_MAIN_MENU_HELP:
					case ENTRY_MAIN_MENU_EXIT:
						switch ( ( ( Integer ) evt.data ).intValue() ) {
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.KEY_BACK:
							case ScreenManager.KEY_CLEAR:
								NanoOnline.exit();
							break;
						}
					break;

					default:
						switch ( key ) {
							case ScreenManager.KEY_SOFT_RIGHT:
							case ScreenManager.KEY_CLEAR:
							case ScreenManager.KEY_BACK:
								evt.source.keyPressed( ScreenManager.FIRE );
							break;
						} // fim switch ( key )
				}
			break;
		} // fim switch ( evt.eventType )			
	} // fim do método eventPerformed( Event )


	private static final NanoOnlineContainer CreateHelpScreen( String text ) throws Exception {
		final NanoOnlineContainer helpScreen = new NanoOnlineContainer( 1, new BorderLayout() );

		helpScreen.setBackIndex( SCREEN_HELP_MENU );

		final FormText formText = new FormText( NanoOnline.getFont( FONT_BLACK ) );
		formText.setId( ENTRY_HELP_TEXT );
		formText.addEventListener( instance );
		formText.setSize( Short.MAX_VALUE, Short.MAX_VALUE );
		formText.setScrollBarV( NanoOnline.getScrollBarV() );
		formText.setText( text );

		formText.setMaxLines( 0 );
		helpScreen.insertDrawable( formText, BorderLayout.CENTER );

		return helpScreen;
	}


	private static final NanoOnlineContainer CreateMainMenu() throws Exception {
		final NanoOnlineContainer mainMenu = new NanoOnlineContainer( 10 );

		final byte[] TEXTS = { TEXT_PROFILES, TEXT_RECORDS, TEXT_NEWS, TEXT_HELP_TITLE, TEXT_EXIT };

		for ( byte i = ENTRY_MAIN_MENU_PROFILES, text = 0; i <= ENTRY_MAIN_MENU_EXIT; ++i, ++text ) {
			final Component c = NanoOnline.getButton( TEXTS[ text ], i, instance );
			mainMenu.insertDrawable( c );
		}

		if ( NewsFeederScreen.hasUnreadEntries() ) {
			mainMenu.getComponentAt( ENTRY_MAIN_MENU_NEWS - ENTRY_MAIN_MENU_PROFILES ).setBorder( NanoOnline.getBorder( BORDER_COLOR_TYPE_HIGHLIGHT ) );
		}
		
		return mainMenu;
	}

}
