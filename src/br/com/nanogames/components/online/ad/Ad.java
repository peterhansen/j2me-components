//#if AD_MANAGER == "true"
/**
 * Ad.java
 *
 * Created on 1:37:11 PM.
 *
 */
package br.com.nanogames.components.online.ad;

import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;


/**
 *
 * @author Peter
 */
public final class Ad implements Serializable {

	/** Id da versão do anúncio (conforme informado pelo servidor). */
	private int version_id;

	/** Id da versão do canal do anúncio (conforme informado pelo servidor). */
	private int adChannelVersionId;

	/** Data de expiração do anúncio. */
	private long expirationDate;

	/** Ids dos recursos utilizados. */
	private int[] resourceIds = new int[ 0 ];


	public Ad() {
	}

	
	public Ad( byte[] data ) throws Exception {
		ByteArrayInputStream byteInput = null;
		DataInputStream dataInput = null;

		try {
			byteInput = new ByteArrayInputStream( data );
			dataInput = new DataInputStream( byteInput );
			read( dataInput );
		} finally {
			if ( byteInput != null ) {
				try {
					byteInput.close();
				} catch ( Exception e ) {}
			}
			if ( dataInput != null ) {
				try {
					dataInput.close();
				} catch ( Exception e ) {}
			}
		}
	}

	
	public final Resource[] getResources() {
		final Resource[] resources = new Resource[ resourceIds.length ];
		for ( short i = 0; i < resources.length; ++i )
			resources[ i ] = ResourceManager.getResource( resourceIds[ i ] );

		return resources;
	}


	public final int[] getResourceIds() {
		return resourceIds;
	}


	private final void setResourceIds( int[] resourceIds ) {
		this.resourceIds = resourceIds;
	}


	/**
	 *
	 * @return
	 */
	public final int getAdChannelVersionId() {
		return adChannelVersionId;
	}


	/**
	 *
	 * @param adChannelId
	 */
	public final void setAdChannelVersionId( int adChannelId ) {
		this.adChannelVersionId = adChannelId;
	}


	/**
	 *
	 * @return
	 */
	public final long getExpirationDate() {
		return expirationDate;
	}


	/**
	 *
	 * @param expirationDate
	 */
	public final void setExpirationDate( long expirationDate ) {
		this.expirationDate = expirationDate;
	}


	/**
	 *
	 * @return
	 */
	public final int getVersionId() {
		return version_id;
	}


	/**
	 *
	 * @param id
	 */
	public final void setVersionId( int id ) {
		this.version_id = id;
	}


	/**
	 * 
	 * @param output
	 * @throws java.lang.Exception
	 */
	public final void write( DataOutputStream output ) throws Exception {
		output.writeInt( version_id );
		output.writeInt( adChannelVersionId );
		output.writeLong( expirationDate );
		
		output.writeShort( resourceIds.length );
		for ( short i = 0; i < resourceIds.length; ++i ) {
			output.writeInt( resourceIds[ i ] );
		}
	}


	/**
	 * 
	 * @param input
	 * @throws java.lang.Exception
	 */
	public final void read( DataInputStream input ) throws Exception {
		version_id = input.readInt();
		adChannelVersionId = input.readInt();
		expirationDate = input.readLong();

		resourceIds = new int[ input.readShort() ];
		for ( short i = 0; i < resourceIds.length; ++i ) {
			resourceIds[ i ] = input.readInt();
		}
	}


	/**
	 * 
	 * @param input
	 * @throws java.lang.Exception
	 */
	protected final void parseParams( DataInputStream input ) throws Exception {
		byte paramId;

		boolean readingData = true;

		do {
			paramId = input.readByte();
			//#if DEBUG == "true"
//# 				System.out.println( "LEU ID DE AD: " + paramId );
			//#endif

			switch ( paramId ) {
				case AdManager.PARAM_AD_CHANNEL_VERSION_ID:
					setAdChannelVersionId( input.readInt() );
				break;

				case AdManager.PARAM_AD_VERSION_ID:
					setVersionId( input.readInt() );
				break;

				case AdManager.PARAM_AD_EXPIRATION_TIME:
					setExpirationDate( input.readLong() );
				break;

				case AdManager.PARAM_AD_N_RESOURCES:
					// lê os ids dos recursos-filhos
					final short totalChildren = input.readShort();
					final int[] newChildrenIds = new int[ totalChildren ];

					for ( short i = 0; i < totalChildren; ++i ) {
						newChildrenIds[ i ] = input.readInt();
					}

					setResourceIds( newChildrenIds );
					//#if DEBUG == "true"
//# 						System.out.print( "RECURSOS: " );
//# 						for ( int i = 0; i < newChildrenIds.length; ++i ) {
//# 							System.out.print( newChildrenIds[ i ] + ", " );
//# 						}
					//#endif
				break;

				case AdManager.PARAM_AD_END:
					//#if DEBUG == "true"
//# 						System.out.println( "FIM DA LEITURA DO ANÚNCIO #" + getVersionId() );
					//#endif
					readingData = false;
				break;

				default:
					//#if DEBUG == "true"
//# 						throw new IllegalArgumentException( "paramId inválido: " + paramId );
					//#else
						throw new IllegalArgumentException();
					//#endif
			}
		} while ( readingData );
	}
	

}

//#endif