/**
 * NanoOnlineScrollBar.java
 * 
 * Created on Aug 4, 2009, 4:13:59 PM
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.Pattern;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

/**
 *
 * @author Peter
 */
public final class NanoOnlineScrollBar extends ScrollBar {

	/** Dimensão mínima (altura ou largura) da barra de scroll. */
	public static final byte MIN_DIMENSION = 5;

	/** Dimensão padrão (altura ou largura) da barra de scroll. */
	public static final byte DEFAULT_DIMENSION = 8;

	/** Dimensão padrão (altura ou largura) da barra de scroll para aparelhos com touchscreen. */
	public static final byte DEFAULT_DIMENSION_TOUCHSCREEN = 16;

	private static final int COLOR_FULL_LEFT_OUT	= 0x003942;
	private static final int COLOR_FULL_FILL		= 0x3998a9;
	private static final int COLOR_FULL_RIGHT		= 0x106e79;
	private static final int COLOR_FULL_LEFT		= 0x6ce2fc;
	private static final int COLOR_PAGE_OUT			= 0x002b30;
	private static final int COLOR_PAGE_FILL		= 0x6ce2fc;
	private static final int COLOR_PAGE_LEFT_1		= 0xb0f1ff;
	private static final int COLOR_PAGE_LEFT_2		= 0xebffff;


	private final int colorFullLeftOut;
	private final int colorFullFill;
	private final int colorFullRight;
	private final int colorFullLeft;
	private final int colorPageOut;
	private final int colorPageFill;
	private final int colorPageLeft1;
	private final int colorPageLeft2;


	public NanoOnlineScrollBar( byte type ) {
		this( type, COLOR_FULL_LEFT_OUT, COLOR_FULL_FILL, COLOR_FULL_RIGHT, COLOR_FULL_LEFT, COLOR_PAGE_OUT, COLOR_PAGE_FILL, COLOR_PAGE_LEFT_1, COLOR_PAGE_LEFT_2 );
	}


	public NanoOnlineScrollBar( byte type, int colorFullLeftOut, int colorFullFill, int colorFullRight, int colorFullLeft, int colorPageOut, int colorPageFill, int colorPageLeft1, int colorPageLeft2 ) {
		super( 2, type );

		this.colorFullLeftOut = colorFullLeftOut;
		this.colorFullFill = colorFullFill;
		this.colorFullRight = colorFullRight;
		this.colorFullLeft = colorFullLeft;
		this.colorPageOut = colorPageOut;
		this.colorPageFill = colorPageFill;
		this.colorPageLeft1 = colorPageLeft1;
		this.colorPageLeft2 = colorPageLeft2;

		barFull = new Pattern( colorFullFill );
		barFull.setPosition( 2, 1 );
		insertDrawable( barFull );

		barPage = new Pattern( colorPageFill );
		barPage.setPosition( 1, 1 );
		insertDrawable( barPage );

		//#if TOUCH == "true"
			final int DIMENSION = ScreenManager.getInstance().hasPointerEvents() ? DEFAULT_DIMENSION_TOUCHSCREEN : DEFAULT_DIMENSION;
		//#else
//# 			final int DIMENSION = DEFAULT_DIMENSION;
		//#endif

		switch ( type ) {
			case TYPE_HORIZONTAL:
				barPage.setSize( 0, DIMENSION - 2 );
				barFull.setSize( 0, DIMENSION - 3 );
			break;

			case TYPE_VERTICAL:
				barPage.setSize( DIMENSION - 2, 0 );
				barFull.setSize( DIMENSION - 3, 0 );
			break;
		}
		size.set( DIMENSION, DIMENSION );
	}

	
	public final void refreshScroll( int currentValue, int currentPosition, int maxValue ) {
		switch ( type ) {
			case TYPE_HORIZONTAL:
				barPage.setSize( Math.max( MIN_DIMENSION, ( currentValue * getWidth() / maxValue ) + 1 ), barPage.getHeight() );
				barPage.setPosition( currentPosition * getWidth() / maxValue, barPage.getPosY() );
			break;

			case TYPE_VERTICAL:
				barPage.setSize( barPage.getWidth(), Math.max( MIN_DIMENSION, ( currentValue * getHeight() / maxValue ) + 1 ) );
				barPage.setPosition( barPage.getPosX(), currentPosition * getHeight() / maxValue );
			break;
		}
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		switch ( type ) {
			case TYPE_HORIZONTAL:
				barPage.setSize( width - 2, barPage.getHeight() );
				barFull.setSize( width - 3, barFull.getHeight() );
			break;

			case TYPE_VERTICAL:
				barPage.setSize( barPage.getWidth(), height - 2 );
				barFull.setSize( barFull.getWidth(), height - 2 );
			break;
		}
	}


	protected void paint( Graphics g ) {
		// barra total

		final int X1 = translate.x + 1;
		final int Y1 = translate.y + 1;

		// borda superior externa
		//#if J2SE == "false"
			g.setColor( colorFullLeftOut );
		//#else
//# 			g.setColor( new Color( colorFullLeftOut ) );
		//#endif
		g.drawRect( X1, translate.y, barFull.getWidth(), 0 );

		// borda esquerda
		g.drawRect( translate.x, Y1, 0, barFull.getHeight() - 1 );

		// borda direita
		//#if J2SE == "false"
			g.setColor( colorFullRight );
		//#else
//# 			g.setColor( new Color( colorFullRight ) );
		//#endif
		g.drawRect( translate.x + 2 + barFull.getWidth(), Y1, 0, barFull.getHeight() - 1 );

		// interior esquerdo
		//#if J2SE == "false"
			g.setColor( colorFullLeft );
		//#else
//# 			g.setColor( new Color( colorFullLeft ) );
		//#endif
		g.drawRect( X1, Y1, 0, barFull.getHeight() - 1 );

		super.paint( g );

		// barra da página
		final int y = translate.y + barPage.getPosY();

		// interior esquerdo
		//#if J2SE == "false"
			g.setColor( colorPageLeft1 );
		//#else
//# 			g.setColor( new Color( colorPageLeft1 ) );
		//#endif
		g.drawRect( X1, y + 1, barPage.getWidth(), barPage.getHeight() - 2 );
		//#if J2SE == "false"
			g.setColor( colorPageLeft2 );
		//#else
//# 			g.setColor( new Color( colorPageLeft2 ) );
		//#endif
		g.drawRect( translate.x + 2, y + 2, barPage.getWidth() - 1, barPage.getHeight() - 3 );

		// bordas superior e inferior
		//#if J2SE == "false"
			g.setColor( colorPageOut );
		//#else
//# 			g.setColor( new Color( colorPageOut ) );
		//#endif
		g.drawRect( X1, y, barPage.getWidth() - 1, 0 );
		g.drawRect( X1, y + barPage.getHeight(), barPage.getWidth(), 0 );

		// borda esquerda
		g.drawRect( translate.x, y + 1, 0, barPage.getHeight() - 2 );

		// borda direita
		//#if J2SE == "false"
			g.setColor( colorPageOut );
		//#else
//# 			g.setColor( new Color( colorPageOut ) );
		//#endif
		g.drawRect( X1 + barPage.getWidth(), y + 1, 0, barPage.getHeight() - 2 );

		// borda inferior externa da página
		//#if J2SE == "false"
			g.setColor( colorFullLeftOut );
		//#else
//# 			g.setColor( new Color( colorFullLeftOut ) );
		//#endif
		g.drawRect( X1, y + barPage.getHeight() + 1, barPage.getWidth() - 1, 0 );

		// borda inferior externa
		//#if J2SE == "false"
			g.setColor( colorFullLeftOut );
		//#else
//# 			g.setColor( new Color( colorFullLeftOut ) );
		//#endif
		g.drawRect( X1, Y1 + barFull.getHeight(), barFull.getWidth(), 0 );

	}

}
