/**
 * InfoBar.java
 * 
 * Created on Jul 13, 2009, 1:00:00 PM
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.DrawableBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

/**
 *
 * @author Peter
 */

public final class TitleBar extends Container implements NanoOnlineConstants, EventListener {

	/** Label que indica o status atual. */
	protected final MarqueeLabel label;
	
	/** Contâiner do label que indica o status atual. */
	protected final DrawableComponent labelComponent;

	private static final short TEXT_BOX_VISIBLE_TIME = 2000;

	private final FormLabel textBoxEntryLabel;

	private short accTextBoxTime;

	private static final byte BORDER_THICKNESS = 5;


	public TitleBar() throws Exception {
		super( 10, new BorderLayout() );

		setPreferredSize( size );

		label = new MarqueeLabel( NanoOnline.getFont( FONT_DEFAULT ), null );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		labelComponent = new DrawableComponent( label );
		insertDrawable( labelComponent, BorderLayout.CENTER );

		final DrawableBorder b = new DrawableBorder( new Pattern( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "bt.png" ) ) );
		b.set( BORDER_THICKNESS, 1, BORDER_THICKNESS, 1 );

		labelComponent.setBorder( b );

		textBoxEntryLabel = new FormLabel( NanoOnline.getFont( FONT_WHITE ), null );
	}


	public final String getUIID() {
		return "TitleBar";
	}


	public final void setText( int textIndex ) {
		setText( NanoOnline.getText( textIndex ).toUpperCase() );
	}


	public final void setText( String text ) {
		label.setText( text.toUpperCase(), false );
		label.setTextOffset( 0 );
	}


	public final void eventPerformed( Event evt ) {
		switch ( evt.eventType ) {
			case Event.EVT_TEXTBOX_CASE_TYPE_CHANGED:
			case Event.EVT_TEXTBOX_INPUT_MODE_CHANGED:
				textBoxEntryLabel.setText( ( ( TextBox ) evt.source ).getCaseTypeString() );
				textBoxEntryLabel.setPosition( getWidth() - textBoxEntryLabel.getWidth() - LAYOUT_GAP_X,
											   ( getHeight() - textBoxEntryLabel.getHeight() ) >> 1 );

				textBoxEntryLabel.setVisible( true );
				accTextBoxTime = TEXT_BOX_VISIBLE_TIME;
			break;
		}
	}


	public final void update( int delta ) {
		super.update( delta );

		if ( accTextBoxTime > 0 ) {
			accTextBoxTime -= delta;

			if ( accTextBoxTime <= 0 ) {
				textBoxEntryLabel.setVisible( false );
			}
		}
	}


	protected final void paint( Graphics g ) {
		super.paint( g );

		textBoxEntryLabel.draw( g );
	}

}
