/**
 * ConnectionListener.java
 * 
 * Created on 31/Out/2008, 14:43:01
 *
 */

package br.com.nanogames.components.online;

/**
 *
 * @author Peter
 */
public interface ConnectionListener {
	
	/** ID de informação recebida pelo listener: conexão foi aberta.
	 * <p>extraData: <code>null</code></p> */
	public static final byte INFO_CONNECTION_OPENED		= 0;
	
	/** ID de informação recebida pelo listener: o OutputStream foi aberto.
	 * <p>extraData: <code>null</code></p> */
	public static final byte INFO_OUTPUT_STREAM_OPENED	= INFO_CONNECTION_OPENED + 1;
	
	/** ID de informação recebida pelo listener: dados foram gravados no OutputStream.
	 * <p>extraData: <code>null</code></p> */
	public static final byte INFO_DATA_WRITTEN			= INFO_OUTPUT_STREAM_OPENED + 1;
	
	/** ID de informação recebida pelo listener: recebeu o código de resposta da conexão.
	 * <p>extraData: Integer contendo o código de resposta.</p> */
	public static final byte INFO_RESPONSE_CODE			= INFO_DATA_WRITTEN + 1;
	
	/** ID de informação recebida pelo listener: abriu o InputStream para leitura da resposta da conexão.
	 * <p>extraData: <code>null</code></p> */
	public static final byte INFO_INPUT_STREAM_OPENED	= INFO_RESPONSE_CODE + 1;
	
	/** ID de informação recebida pelo listener: recebeu o content-length da resposta.
	 * <p>extraData: Integer contendo o tamanho da resposta. Importante: esse valor pode não representar 
	 * o tamanho real da resposta (pode retornar 0 ou -1, por exemplo). </p>
	 */
	public static final byte INFO_CONTENT_LENGTH_TOTAL	= INFO_INPUT_STREAM_OPENED + 1;
	
	/** ID de informação recebida pelo listener: atualizou a quantidade de bytes da resposta efetivamente lidos.
	 * <p>extraData: Integer com o valor total de bytes lidos da resposta da conexão.</p>
	 */
	public static final byte INFO_CONTENT_LENGTH_READ	= INFO_CONTENT_LENGTH_TOTAL + 1;
	
	/** ID de informação recebida pelo listener: conexão encerrada com sucesso.<p>extraData: <code>null</code></p> */
	public static final byte INFO_CONNECTION_ENDED		= INFO_CONTENT_LENGTH_READ + 1;
	
	// códigos de erro
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_HTTP_CONNECTION_NOT_SUPPORTED	= 0;
	
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_CANT_GET_RESPONSE_CODE			= ERROR_HTTP_CONNECTION_NOT_SUPPORTED + 1;
	
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_URL_BAD_FORMAT					= ERROR_CANT_GET_RESPONSE_CODE + 1;
	
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_CONNECTION_EXCEPTION				= ERROR_URL_BAD_FORMAT + 1;
	
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_CANT_CLOSE_INPUT_STREAM			= ERROR_CONNECTION_EXCEPTION + 1;
	
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_CANT_CLOSE_CONNECTION			= ERROR_CANT_CLOSE_INPUT_STREAM + 1;
	
	/** ID de erro recebido pelo listener:
	 * <p>extraData: referência para a Exception. </p> */
	public static final byte ERROR_CANT_CLOSE_OUTPUT_STREAM			= ERROR_CANT_CLOSE_CONNECTION + 1;	

	
	/**
	 * 
	 * @param id id da conexão chamadora.
	 * @param data dados recebidos de resposta da conexão.
	 */
	public void processData( int id, byte[] data );
	
	
	/**
	 * 
	 * @param id id da conexão chamadora.
	 * @param infoIndex identificador do tipo de informação recebido. Os valores válidos estão definidos na interface <code>ConnectionListener</code>.
	 * @param extraData objeto encapsulando informações extras, caso existam. Verificar os valores possíveis para cada tipo
	 * de informação na classe <code>ConnectionListener</code>.
	 */
	public void onInfo( int id, int infoIndex, Object extraData );
	
	
	/**
	 * 
	 * @param id id da conexão chamadora.
	 * @param errorIndex identificador do tipo de erro recebido. Os valores válidos estão definidos na interface <code>ConnectionListener</code>.
	 * @param extraData objeto encapsulando informações extras, caso existam. Verificar os valores possíveis para cada tipo
	 * de erro na classe <code>ConnectionListener</code>.
	 */
	public void onError( int id, int errorIndex, Object extraData );
	
}
