/**
 * ProfileScreen.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */
package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.RadioButton;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;


/**
 *
 * @author Peter
 */
public final class ProfileScreen extends NanoOnlineContainer implements ConnectionListener {

	// <editor-fold defaultstate="collapsed" desc="CÓDIGOS DE RETORNO DO REGISTRO DO USUÁRIO">

	// ATENÇÃO: ESSES CÓDIGOS DEVEM *SEMPRE* ESTAR DE ACORDO COM OS CÓDIGOS RETORNADOS PELO SERVIDOR!

	// obs.: codigo de retorno zero é OK (RC_OK)
	/** Código de retorno do registro do usuário: erro: apelido já em uso. */
	private static final byte RC_ERROR_NICKNAME_IN_USE = 1;
	/** Código de retorno do registro do usuário: erro: formato do apelido inválido. */
	private static final byte RC_ERROR_NICKNAME_FORMAT = 2;
	/** Código de retorno do registro do usuário: erro: comprimento do apelido inválido. */
	private static final byte RC_ERROR_NICKNAME_LENGTH = 3;
	/** Código de retorno do registro do usuário: erro: formato do e-mail inválido. */
	private static final byte RC_ERROR_EMAIL_FORMAT = 4;
	/** Código de retorno do registro do usuário: erro: comprimento do e-mail inválido. */
	private static final byte RC_ERROR_EMAIL_LENGTH = 5;
	/** Código de retorno do registro do usuário: erro: password muito fraca. */
	private static final byte RC_ERROR_PASSWORD_WEAK = 6;
	/** Código de retorno do registro do usuário: erro: password e confirmação diferentes. */
	private static final byte RC_ERROR_PASSWORD_CONFIRMATION = 7;
	/** Código de retorno do registro do usuário: erro: comprimento do 1º nome inválido. */
	private static final byte RC_ERROR_FIRST_NAME_LENGTH = 8;
	/** Código de retorno do registro do usuário: erro: comprimento do sobrenome inválido. */
	private static final byte RC_ERROR_LAST_NAME_LENGTH = 9;
	/** Código de retorno do registro do usuário: erro: gênero inválido. */
	private static final byte RC_ERROR_GENRE = 10;
	/** Código de retorno do registro do usuário: erro: comprimento do gênero inválido. */
	private static final byte RC_ERROR_GENRE_LENGTH = 11;
	/** Código de retorno do registro do usuário: erro: formato do número de telefone inválido. */
	private static final byte RC_ERROR_PHONE_NUMBER_FORMAT = 12;
	/** Código de retorno do registro do usuário: erro: comprimento do número de telefone inválido. */
	private static final byte RC_ERROR_PHONE_NUMBER_LENGTH = 13;
	/** Código de retorno do registro do usuário: erro: CPF inválido. */
	private static final byte RC_ERROR_CPF_FORMAT = 14;
	/**
	 * Código de retorno do registro do usuário: erro: e-mail já em uso.
	 * @since Nano Online 0.0.31
	 */
	private static final byte RC_ERROR_EMAIL_ALREADY_IN_USE = 15;
	// </editor-fold>

	// <editor-fold desc="IDS DOS PARÂMETROS DE CONEXÃO USADOS PARA CADASTRAR USUÁRIOS E LER DADOS AO IMPORTAR USUÁRIO">
	// observação: devem SEMPRE corresponder aos ids usados no servidor!
	public static final byte PARAM_NICKNAME = 0;
	public static final byte PARAM_FIRST_NAME = PARAM_NICKNAME + 1;
	public static final byte PARAM_LAST_NAME = PARAM_FIRST_NAME + 1;
	public static final byte PARAM_PHONE_NUMBER = PARAM_LAST_NAME + 1;
	public static final byte PARAM_EMAIL = PARAM_PHONE_NUMBER + 1;
	public static final byte PARAM_CPF = PARAM_EMAIL + 1;
	public static final byte PARAM_PASSWORD = PARAM_CPF + 1;
	public static final byte PARAM_PASSWORD_CONFIRM = PARAM_PASSWORD + 1;
	// indica o fim da leitura ou gravação dos dados do usuário
	/** @since Nano Online 0.0.3 */
	public static final byte PARAM_INFO_END = PARAM_PASSWORD_CONFIRM + 1;
	/** @since Nano Online 0.0.3 */
	public static final byte PARAM_GENDER = PARAM_INFO_END + 1;
	/** @since Nano Online 0.0.3 */
	public static final byte PARAM_BIRTHDAY = PARAM_GENDER + 1;
	/** 
	 * Parâmetro de comunicação com o servidor: id do usuário. Esse id foi adicionado pois não necessariamente o usuário
	 * sendo editado é o usuário ativo no Nano Online, o que poderia gerar inconsistências na comunicação com o servidor.
	 * @since Nano Online 0.0.3
	 */
	public static final byte PARAM_CUSTOMER_ID = PARAM_BIRTHDAY + 1;
	/** 
	 * Parâmetro de comunicação com o servidor: instante da última modificação nos dados do usuário.
	 * @since Nano Online 0.0.3
	 */
	public static final byte PARAM_TIMESTAMP = PARAM_CUSTOMER_ID + 1;

	/**
	 * Parâmetro de comunicação com o servidor: hora limite para validação do e-mail.
	 * @since Nano Online 0.0.31
	 */
	public static final byte PARAM_VALIDATE_BEFORE = PARAM_TIMESTAMP + 1;

	/**
	 * Parâmetro de comunicação com o servidor: instante da última validação realizada pelo jogador
	 * @since Nano Online 0.0.31
	 */
	public static final byte PARAM_VALIDATED_AT = PARAM_VALIDATE_BEFORE + 1;
	// </editor-fold>

	// índices das entradas de dados (não necessariamente correspondem aos ids dos parâmetros de conexão
	private static final byte ENTRY_NANO_ID = -1;
	private static final byte ENTRY_NICKNAME = ENTRY_NANO_ID + 1;
	private static final byte ENTRY_FIRST_NAME = ENTRY_NICKNAME + 1;
	private static final byte ENTRY_LAST_NAME = ENTRY_FIRST_NAME + 1;
	private static final byte ENTRY_PHONE_NUMBER = ENTRY_LAST_NAME + 1;
	private static final byte ENTRY_EMAIL = ENTRY_PHONE_NUMBER + 1;
	private static final byte ENTRY_BIRTHDAY_FIELD_1 = ENTRY_EMAIL + 1;
	private static final byte ENTRY_BIRTHDAY_FIELD_2 = ENTRY_BIRTHDAY_FIELD_1 + 1;
	private static final byte ENTRY_BIRTHDAY_FIELD_3 = ENTRY_BIRTHDAY_FIELD_2 + 1;
	private static final byte ENTRY_PASSWORD = ENTRY_BIRTHDAY_FIELD_3 + 1;
	private static final byte ENTRY_PASSWORD_CONFIRM = ENTRY_PASSWORD + 1;
	private final byte ENTRY_BIRTHDAY_DAY;
	private final byte ENTRY_BIRTHDAY_MONTH;
	private final byte ENTRY_BIRTHDAY_YEAR;

	/** Checkbox para lembrar (ou não) a password do jogador automaticamente. */
	private static final byte ENTRY_REMEMBER_PASSWORD = ENTRY_PASSWORD_CONFIRM + 1;

	/***/
	private static final byte ENTRY_BUTTON_OK = ENTRY_REMEMBER_PASSWORD + 1;
	private static final byte ENTRY_BUTTON_EDIT = ENTRY_BUTTON_OK + 1;
	private static final byte ENTRY_BUTTON_USE = ENTRY_BUTTON_EDIT + 1;

	/***/
	private static final byte ENTRY_BUTTON_CANCEL				= ENTRY_BUTTON_USE + 1;
	private static final byte ENTRY_BUTTON_ERASE				= ENTRY_BUTTON_CANCEL + 1;
	private static final byte ENTRY_BUTTON_BACK					= ENTRY_BUTTON_ERASE + 1;
	private static final byte ENTRY_BUTTON_ERASE_CONFIRM		= ENTRY_BUTTON_BACK + 1;
	private static final byte ENTRY_BUTTON_ERASE_BACK			= ENTRY_BUTTON_ERASE_CONFIRM + 1;
	private static final byte ENTRY_BUTTON_RESEND_CONFIRMATION	= ENTRY_BUTTON_ERASE_BACK + 1;
	private static final byte ENTRY_BUTTON_RESEND_CONFIRM		= ENTRY_BUTTON_RESEND_CONFIRMATION + 1;
	private static final byte ENTRY_TEXT_EMAIL_VALIDATION		= ENTRY_BUTTON_RESEND_CONFIRM + 1;
	private static final byte ENTRY_BUTTON_FORGOT_PASSWORD		= ENTRY_TEXT_EMAIL_VALIDATION + 1;

	/** Quantidade total de entradas da tela. */
	private static final byte ENTRIES_TOTAL						= ENTRY_BUTTON_FORGOT_PASSWORD + 3;

	/***/
	private static final byte EDIT_LABELS_TOTAL = ENTRY_PASSWORD_CONFIRM + 1;
	private static final byte GENDER_INDEX_NOT_INFORMED = 0;
	private static final byte GENDER_INDEX_FEMALE = 1;
	private static final byte GENDER_INDEX_MALE = 2;

	/***/
	private final TextBox[] textBoxes = new TextBox[ EDIT_LABELS_TOTAL ];
	/** Indica se a password do usuário deve ser lembrada automaticamente. */
	private final CheckBox checkBoxRememberPassword;
	/***/
	private final RadioButton radioGender;
	/***/
	private final Button buttonEditOK;
	/***/
	private final Button buttonUse;
	/***/
	private final Button buttonErase;
	/***/
	private final Button buttonCancel;
	/***/
	private final Button buttonResendConfirmation;
	private final FormText textEmailValidation;
	/***/
	private int currentConnection = -1;
	/***/
	private static byte profileIndex = -1;
	/***/
	private final boolean newProfile;
	/***/
	private int lastAction = -1;
	/** Endereço de e-mail anterior do usuário - caso haja alteração, ele deve ser revalidado. */
	private final String previousEmail;
	

	/**
	 * 
	 * @param profileIndex
	 * @throws java.lang.Exception
	 */
	public ProfileScreen( boolean newProfile, boolean editable, int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );

		this.newProfile = newProfile;
		setBackIndex( backIndex >= 0 ? backIndex : SCREEN_PROFILE_SELECT );

		final Customer customer = ( !newProfile && profileIndex >= 0 ) ? NanoOnline.getCustomer( profileIndex ) : new Customer();
		previousEmail = customer.getEmail();

		// se o usuario ja existir, mostra seu Nano ID
		if ( !newProfile && profileIndex >= 0 ) {
			final FormLabel labelId = new FormLabel( NanoOnline.getFont( FONT_BLACK ), NanoOnline.getText( TEXT_PROFILE_ID ) + customer.getId() );
			labelId.addEventListener( this );
			labelId.setFocusable( true );
			insertDrawable( labelId );
			
			final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 0 );
			text.setText( NanoOnline.getText( TEXT_REQUIRED_FIELDS ) );
			text.addEventListener( this );
			text.setFocusable( newProfile || profileIndex < 0 );
			insertDrawable( text );
		}


		final byte[] INPUT_MODE = { TextBox.INPUT_MODE_ANY,
									TextBox.INPUT_MODE_ANY,
									TextBox.INPUT_MODE_ANY,
									TextBox.INPUT_MODE_NUMBERS,
									TextBox.INPUT_MODE_EMAIL,
									TextBox.INPUT_MODE_NUMBERS,
									TextBox.INPUT_MODE_NUMBERS,
									TextBox.INPUT_MODE_NUMBERS,
									TextBox.INPUT_MODE_PASSWORD,
									TextBox.INPUT_MODE_PASSWORD };

		final byte[] titles = { TEXT_NICKNAME,
								TEXT_FIRST_NAME,
								TEXT_LAST_NAME,
								TEXT_PHONE_NUMBER,
								TEXT_EMAIL,
								-1,
								-1,
								-1,
								TEXT_PASSWORD,
								TEXT_PASSWORD_CONFIRM };

		final boolean[] necessary = {   true,
										false,
										false,
										false,
										true,
										true,
										true,
										true,
										true,
										true, };

		final String timeFormat = NanoOnline.getText( TEXT_TIME_FORMAT ).toLowerCase();
		final int indexDay = timeFormat.indexOf( "d" );
		final int indexMonth = timeFormat.indexOf( "m" );
		final int indexYear = timeFormat.indexOf( "y" );

		if ( indexDay < indexMonth ) {
			// DMY, DYM, YDM
			if ( indexMonth < indexYear ) {
				// DMY
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_3;
			} else if ( indexDay < indexYear ) {
				// DYM
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_2;
			} else {
				// YDM
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_1;
			}
		} else {
			// MDY, MYD, YMD
			if ( indexMonth < indexYear ) {
				// MDY
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_3;
			} else if ( indexDay < indexYear ) {
				// MYD
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_1;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_2;
			} else {
				// YMD
				ENTRY_BIRTHDAY_DAY = ENTRY_BIRTHDAY_FIELD_3;
				ENTRY_BIRTHDAY_MONTH = ENTRY_BIRTHDAY_FIELD_2;
				ENTRY_BIRTHDAY_YEAR = ENTRY_BIRTHDAY_FIELD_1;
			}
		}

		final byte[] length = { Customer.NICKNAME_MAX_CHARS,
			Customer.FIRST_NAME_MAX_CHARS,
			Customer.LAST_NAME_MAX_CHARS,
			Customer.PHONE_NUMBER_MAX_CHARS,
			Customer.EMAIL_MAX_CHARS,
			( byte ) ( ENTRY_BIRTHDAY_YEAR == ENTRY_BIRTHDAY_FIELD_1 ? 4 : 2 ),
			( byte ) ( ENTRY_BIRTHDAY_YEAR == ENTRY_BIRTHDAY_FIELD_2 ? 4 : 2 ),
			( byte ) ( ENTRY_BIRTHDAY_YEAR == ENTRY_BIRTHDAY_FIELD_3 ? 4 : 2 ),
			Customer.NICKNAME_MAX_CHARS,
			Customer.NICKNAME_MAX_CHARS };

		// adiciona os campos para entrada do aniversário
		final Container containerBirthday = new Container( 5 ) {
			public final Point calcPreferredSize( Point maximumSize ) {
				return new Point( ScreenManager.SCREEN_WIDTH,
								  ScreenManager.SCREEN_WIDTH > 300 ? 60 : 34 ); // TODO corrigir erro de cálculo automático de tamanho
			}
		};
		final FlowLayout birthdayLayout = new FlowLayout( FlowLayout.AXIS_HORIZONTAL, ANCHOR_BOTTOM );
		birthdayLayout.start.set( 0, LAYOUT_GAP_Y );
		birthdayLayout.gap.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );
		containerBirthday.setLayout( birthdayLayout );
		containerBirthday.setBorder( NanoOnline.getTitledBorder( TEXT_BIRTHDAY, null ) );

		FormText temp = null;
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			TextBox textBox = null;

			if ( i < ENTRY_BIRTHDAY_FIELD_1 || i > ENTRY_BIRTHDAY_FIELD_3 ) {
				textBox = NanoOnline.getTextBox( i, length[i], INPUT_MODE[i],
						( necessary[ i ] && !newProfile ) ? NanoOnline.getText( titles[i] ) + "(*)" : NanoOnline.getText( titles[i] ) );
				textBoxes[i] = textBox;

				if ( necessary[ i ] || !newProfile )
					insertDrawable( textBox );

				switch ( i ) {
					case ENTRY_NICKNAME:
						textBox.setText( customer.getNickname(), false );
						break;

					case ENTRY_EMAIL:
						textBox.setText( customer.getEmail(), false );

						if ( !newProfile ) {
							// insere também informação a respeito da validação de e-mail
							temp = new FormText( NanoOnline.getFont( FONT_BLACK ), 3 );

							temp.setId( ENTRY_TEXT_EMAIL_VALIDATION );
							temp.addEventListener( this );
							insertDrawable( temp );
						}
					break;

					case ENTRY_PASSWORD_CONFIRM:
					case ENTRY_PASSWORD:
						if ( !customer.isRememberPassword() ) {
							// só preenche com a password se o jogador tiver marcado essa opção
							break;
						}
						textBox.setText( customer.getPassword(), false );
						break;

					case ENTRY_FIRST_NAME:
						textBox.setText( customer.getFirstName(), false );
						break;

					case ENTRY_LAST_NAME:
						textBox.setText( customer.getLastName(), false );
						break;

					case ENTRY_PHONE_NUMBER:
						textBox.setText( customer.getPhoneNumber(), false );
						break;
				}
			} else {
				if ( !newProfile && i == ENTRY_BIRTHDAY_FIELD_1 ) {
					insertDrawable( containerBirthday );
				}

				// caixas de texto da data de nascimento
				textBox = new TextBox( NanoOnline.getFont( FONT_BLACK ), null, length[i], TextBox.INPUT_MODE_NUMBERS, true );
				textBoxes[i] = textBox;
				textBox.setCaret( NanoOnline.getCaret() );
				textBox.setBorder( NanoOnline.getSimpleBorder( false ) );
				containerBirthday.insertDrawable( textBox );

				if ( i < ENTRY_BIRTHDAY_FIELD_3 ) {
					containerBirthday.insertDrawable( new FormLabel( NanoOnline.getFont( FONT_BLACK ), "/" ) );
				}
			}

			textBox.addEventListener( this );
			textBox.setPasswordMask( PASSWORD_MASK_DEFAULT );
			textBox.setEnabled( newProfile );
		}

		textEmailValidation = temp;
		refreshEmailValidationText();

		if ( customer.getBirthDay() != Customer.BIRTHDAY_DEFAULT_VALUE ) {
			final Date date = new Date( customer.getBirthDay() );
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime( date );

			int value = calendar.get( Calendar.DAY_OF_MONTH );
			textBoxes[ENTRY_BIRTHDAY_DAY].setText( ( value < 10 ? "0" : "" ) + String.valueOf( value ), false );
			// falta de padrão nas constantes do Java exige o + 1...
			value = calendar.get( Calendar.MONTH ) + 1;
			textBoxes[ENTRY_BIRTHDAY_MONTH].setText( ( value < 10 ? "0" : "" ) + String.valueOf( value ), false );
			textBoxes[ENTRY_BIRTHDAY_YEAR].setText( String.valueOf( calendar.get( Calendar.YEAR ) ), false );
		}

		// adiciona o checkbox "lembrar minha senha"
		checkBoxRememberPassword = new CheckBox( NanoOnline.getCheckBoxButton(), NanoOnline.getFont( FONT_BLACK ), NanoOnline.getText( TEXT_REMEMBER_PASSWORD ), true );
		checkBoxRememberPassword.setChecked( customer.isRememberPassword() );
		checkBoxRememberPassword.setEnabled( newProfile );
		insertDrawable( checkBoxRememberPassword );

		// mostra o radio button para indicar o gênero
		radioGender = new RadioButton( NanoOnline.getCheckBoxButton(), NanoOnline.getFont( FONT_BLACK ),
				new String[]{ NanoOnline.getText( TEXT_GENDER_NOT_INFORMED ),
					NanoOnline.getText( TEXT_GENDER_FEMALE ),
					NanoOnline.getText( TEXT_GENDER_MALE ) }, 0 );
		( ( FlowLayout ) radioGender.getLayout() ).start.set( LAYOUT_GAP_X, LAYOUT_GAP_Y );

		radioGender.setBorder( NanoOnline.getTitledBorder( TEXT_GENRE, NanoOnline.getSimpleBorder( false ) ) );
		switch ( customer.getGender() ) {
			case Customer.GENDER_FEMALE:
				radioGender.setCheckedIndex( 1 );
				break;

			case Customer.GENDER_MALE:
				radioGender.setCheckedIndex( 2 );
				break;

			case Customer.GENDER_NOT_INFORMED:
			default:
				radioGender.setCheckedIndex( 0 );
				break;
		}
		radioGender.setEnabled( newProfile );

		if ( !newProfile )
			insertDrawable( radioGender );

		// se o perfil já existir, mostra a opção "usar este perfil"
		if ( newProfile ) {
			buttonUse = null;
		} else {
			buttonUse = NanoOnline.getButton( TEXT_SET_AS_CURRENT_PROFILE, ENTRY_BUTTON_USE, this );
			insertDrawable( buttonUse );
		}

		buttonEditOK = NanoOnline.getButton( newProfile ? TEXT_OK : TEXT_EDIT, newProfile ? ENTRY_BUTTON_OK : ENTRY_BUTTON_EDIT, this );
		insertDrawable( buttonEditOK );

		if ( newProfile ) {
			buttonErase = null;
			buttonResendConfirmation = null;
		} else {
			// se o perfil já existir, mostra a opção "apagar"
			buttonErase = NanoOnline.getButton( TEXT_ERASE_PROFILE, ENTRY_BUTTON_ERASE, this );
			insertDrawable( buttonErase );

			insertDrawable( NanoOnline.getButton( TEXT_FORGOT_PASSWORD, ENTRY_BUTTON_FORGOT_PASSWORD, this ) );

			// adiciona botão para reenvio do e-mail de confirmação
			buttonResendConfirmation = NanoOnline.getButton( TEXT_RESEND_CONFIRMATION_TITLE, ENTRY_BUTTON_RESEND_CONFIRMATION, this );
			if ( customer.hasExpirationDate() )
				insertDrawable( buttonResendConfirmation );
		}

		buttonCancel = NanoOnline.getButton( newProfile ? TEXT_CANCEL : TEXT_BACK, newProfile ? ENTRY_BUTTON_CANCEL : ENTRY_BUTTON_BACK, this );
		insertDrawable( buttonCancel );

		if ( newProfile || editable ) {
			setEditable();
		}
	}


	public final void processData( int id, byte[] data ) {
		onConnectionEnded( false );

		//#if DEBUG == "true"
//# 			System.out.println( "processData: " + ( data == null ? -1 : data.length ) );
		//#endif

		if ( data != null ) {
			DataInputStream input = null;
			try {
				final Customer customer = newProfile ? new Customer() : NanoOnline.getCustomer( profileIndex );
				final Hashtable table = NanoOnline.readGlobalData( data );

				// o código de retorno é obrigatório
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				byte errorEntryIndex = -1;
				byte errorText = -1;

				switch ( returnCode ) {
					case RC_OK:
						// grava as informações do usuário no RMS e mostra a tela de confirmação
						final byte[] specificData = ( byte[] ) table.get( new Byte( ID_SPECIFIC_DATA ) );
						input = new DataInputStream( new ByteArrayInputStream( specificData ) );

						String email = "";
						int textConfirmation = -1;
						switch ( lastAction ) {
							case ENTRY_BUTTON_FORGOT_PASSWORD:
								try {
									input = new DataInputStream( new ByteArrayInputStream( specificData ) );
									byte paramId = -1;

									do {
										paramId = input.readByte();

										switch ( paramId ) {
											case ProfileScreen.PARAM_EMAIL:
												email = input.readUTF();
											break;
										}
									} while ( paramId != ProfileScreen.PARAM_INFO_END );
								} finally {
									if ( input != null ) {
										try {
											input.close();
										} catch ( Exception e ) {
										}
									}
								}

								removeAllComponents();

								final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 5 );
								text.setText( NanoOnline.getText( TEXT_NEW_PASSWORD_SENT ) + email + "." );
								text.addEventListener( this );
								insertDrawable( text );

								buttonCancel.setText( NanoOnline.getText( TEXT_BACK ) );
								insertDrawable( buttonCancel );
								refreshLayout();

								NanoOnline.getProgressBar().showConfirmation( TEXT_NEW_PASSWORD_SENT_CONFIRMATION );
							return;

							case ENTRY_BUTTON_RESEND_CONFIRM:
								textConfirmation = TEXT_VALIDATION_EMAIL_RESENT;

								customer.parseParams( specificData );
								email = customer.getEmail();
							break;

							default:
								customer.setFirstName( textBoxes[ENTRY_FIRST_NAME].getText() );
								customer.setLastName( textBoxes[ENTRY_LAST_NAME].getText() );
								customer.setNickname( textBoxes[ENTRY_NICKNAME].getText() );
								customer.setEmail( textBoxes[ENTRY_EMAIL].getText() );
								customer.setPhoneNumber( textBoxes[ENTRY_PHONE_NUMBER].getText() );
								customer.setPassword( textBoxes[ENTRY_PASSWORD].getText() );
								customer.setRememberPassword( checkBoxRememberPassword.isChecked() );
								final long time = AppMIDlet.getTime( Integer.valueOf( textBoxes[ENTRY_BIRTHDAY_DAY].getText() ).intValue(),
																	 Integer.valueOf( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() ).intValue(),
																	 Integer.valueOf( textBoxes[ENTRY_BIRTHDAY_YEAR].getText() ).intValue() );
								customer.setBirthDay( time );

								switch ( radioGender.getCheckedIndex() ) {
									case GENDER_INDEX_NOT_INFORMED:
										customer.setGender( Customer.GENDER_NOT_INFORMED );
										break;

									case GENDER_INDEX_FEMALE:
										customer.setGender( Customer.GENDER_FEMALE );
										break;

									case GENDER_INDEX_MALE:
										customer.setGender( Customer.GENDER_MALE );
										break;
								}

								// lê os dados restantes que o servidor enviou
								customer.parseParams( specificData );

								email = customer.getEmail();

								if ( newProfile ) {
									textConfirmation = TEXT_CUSTOMER_REGISTERED;
									NanoOnline.addProfile( customer );
								} else {
									textConfirmation = TEXT_CUSTOMER_UPDATED;
									NanoOnline.saveProfiles();
								}
							break;
						}

						if ( newProfile || previousEmail.compareTo( email.toLowerCase() ) != 0 ) {
							// informa ao usuário sobre a necessidade de validar o e-mail
							removeAllComponents();

							final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 5 );
							text.setText( NanoOnline.getText( TEXT_VALIDATION_EMAIL_SENT_1 ) +
										  email +
										  NanoOnline.getText( TEXT_VALIDATION_EMAIL_SENT_2 ) +
										  AppMIDlet.formatTime( customer.getValidateBefore(), NanoOnline.getText( TEXT_TIME_FORMAT ) ) );

							text.addEventListener( this );
							insertDrawable( text );

							buttonCancel.setText( NanoOnline.getText( TEXT_BACK ) );
							insertDrawable( buttonCancel );
							refreshLayout();
						} else {
							// como não houve alteração no e-mail, apenas volta para a tela anterior (normalmente a tela
							// de perfis ou a tela de novo recorde)
							onBack();
						}

						NanoOnline.getProgressBar().showConfirmation( textConfirmation );
					break;

					case RC_SERVER_INTERNAL_ERROR:
						errorText = TEXT_ERROR_SERVER_INTERNAL;
						break;

					case RC_APP_NOT_FOUND:
						errorText = TEXT_ERROR_APP_NOT_FOUND;
						break;

					case RC_DEVICE_NOT_SUPPORTED:
						errorText = TEXT_ERROR_DEVICE_NOT_SUPPORTED;
						break;

					case RC_ERROR_NICKNAME_IN_USE:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_REGISTER_ERROR_NICKNAME_IN_USE;
						break;

					case RC_ERROR_NICKNAME_FORMAT:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_REGISTER_ERROR_NICKNAME_FORMAT;
						break;

					case RC_ERROR_NICKNAME_LENGTH:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_REGISTER_ERROR_NICKNAME_LENGTH;
						break;

					case RC_ERROR_EMAIL_FORMAT:
						errorEntryIndex = ENTRY_EMAIL;
						errorText = TEXT_REGISTER_ERROR_EMAIL_FORMAT;
						break;

					case RC_ERROR_EMAIL_LENGTH:
						errorEntryIndex = ENTRY_EMAIL;
						errorText = TEXT_REGISTER_ERROR_EMAIL_LENGTH;
						break;

					case RC_ERROR_PASSWORD_WEAK:
						errorEntryIndex = ENTRY_PASSWORD;
						errorText = TEXT_REGISTER_ERROR_PASSWORD_WEAK;
						break;

					case RC_ERROR_PASSWORD_CONFIRMATION:
						errorEntryIndex = ENTRY_PASSWORD_CONFIRM;
						errorText = TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION;
					break;

					case RC_ERROR_FIRST_NAME_LENGTH:
						errorEntryIndex = ENTRY_FIRST_NAME;
						errorText = TEXT_REGISTER_ERROR_NAME_FORMAT;
					break;

					case RC_ERROR_EMAIL_ALREADY_IN_USE:
						errorEntryIndex = ENTRY_EMAIL;
						errorText = TEXT_REGISTER_ERROR_EMAIL_ALREADY_IN_USE;
					break;
				}

				if ( errorText >= 0 ) {
					if ( errorEntryIndex >= 0 ) {
						textBoxes[errorEntryIndex].getBorder().setState( Border.STATE_ERROR );
						NanoOnline.getForm().requestFocus( textBoxes[errorEntryIndex] );
					}

					NanoOnline.getProgressBar().showError( errorText );
				}

			//#if DEBUG == "true"
//# 					System.out.print( "data(" + data.length + "): " );
//# 					for ( int i = 0; i < data.length; ++i ) {
//# 						System.out.print( ( int ) data[ i ] );
//# 						System.out.print( ", " );
//# 					}
//# 					System.out.println();
//# 					System.out.println( new String( data ) );
			//#endif
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 				AppMIDlet.log( ex , "30");
				//#endif
			} finally {
				if ( input != null ) {
					try {
						input.close();
					} catch ( IOException ex ) {
					}
				}
			}
		}
		NanoOnline.getProgressBar().processData( id, data );
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onInfo( id, infoIndex, extraData );
			switch ( infoIndex ) {
				case ConnectionListener.INFO_CONNECTION_ENDED:
					onConnectionEnded( false );
					break;
			}
		}
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onError( id, errorIndex, extraData );
			onConnectionEnded( false );
		}
	}


	/**
	 * Valida os campos de texto antes de enviar ao servidor.
	 * 
	 * @return <code>true</code>, caso estejam todos preenchidos da maneira correta, e <code>false</code> caso contrário.
	 */
	private final boolean validate() {
		byte firstError = -1;
		byte errorMessage = -1;

		// remove caracteres vazios no começo e no final de cada textbox
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			String text = textBoxes[i].getText().trim();
			// remove espaços duplos nos textos
			int doubleSpaceIndex = text.indexOf( "  " );
			while ( doubleSpaceIndex >= 0 ) {
				// não é necessário verificar o limite da string em doubleSpaceIndex + 1 porque é garantido que isso
				// seja válido (afinal, é um espaço duplo)
				text = text.substring( 0, doubleSpaceIndex ) + text.substring( doubleSpaceIndex + 1 );
				doubleSpaceIndex = text.indexOf( "  " );
			}
			textBoxes[i].setText( text, false );
		}

		// verifica o tamanho e formato do campo "apelido"
		String text = textBoxes[ENTRY_NICKNAME].getText();
		if ( text.length() < Customer.NICKNAME_MIN_CHARS ) {
			firstError = ENTRY_NICKNAME;
			errorMessage = TEXT_REGISTER_ERROR_NICKNAME_LENGTH;
			textBoxes[ENTRY_NICKNAME].getBorder().setState( Border.STATE_ERROR );
		} else {
			// garante que primeiro caracter é válido (a-z, A-Z)
			if ( !isAlpha( text.charAt( 0 ) ) ) {
				firstError = ENTRY_NICKNAME;
				errorMessage = TEXT_REGISTER_ERROR_NICKNAME_FORMAT;
				textBoxes[ENTRY_NICKNAME].getBorder().setState( Border.STATE_ERROR );
			}
		}

		// verifica o tamanho e formato do campo "número de telefone"
		text = textBoxes[ENTRY_PHONE_NUMBER].getText();
		if ( text.length() > 0 ) {
			if ( text.length() < Customer.PHONE_NUMBER_MIN_CHARS ) {
				if ( firstError < 0 ) {
					firstError = ENTRY_PHONE_NUMBER;
					errorMessage = TEXT_REGISTER_ERROR_PHONE_LENGTH;
				}
				textBoxes[ENTRY_NICKNAME].getBorder().setState( Border.STATE_ERROR );
			} else {
				if ( hasCharType( text, ImageFont.CHAR_TYPE_REGULAR ) ||
						hasCharType( text, ImageFont.CHAR_TYPE_ACCENT_LOWER_CASE ) ||
						hasCharType( text, ImageFont.CHAR_TYPE_ACCENT_UPPER_CASE ) ) {
					if ( firstError < 0 ) {
						firstError = ENTRY_PHONE_NUMBER;
						errorMessage = TEXT_REGISTER_ERROR_PHONE_FORMAT;
					}
				}
				textBoxes[ENTRY_NICKNAME].getBorder().setState( Border.STATE_ERROR );
			}
		}

		// verifica o tamanho e formato do campo "e-mail"
		text = textBoxes[ENTRY_EMAIL].getText();
		if ( text.length() >= Customer.EMAIL_MIN_CHARS ) {
			// garante que primeiro caracter seja válido (a-z, A-Z, 0-9) e que não há caracteres acentuados nem espaços no e-mail
			final int indexAt = text.indexOf( '@' );
			if ( !( isAlpha( text.charAt( 0 ) ) || ImageFont.getCharType( text.charAt( 0 ) ) == ImageFont.CHAR_TYPE_NUMERIC ) ||
					text.indexOf( ' ' ) >= 0 ||
					indexAt < 1 ||
					indexAt == text.length() - 1 ||
					indexAt != text.lastIndexOf( '@' ) || // evita que texto tenha dois ou mais caracteres '@'
					text.indexOf( "@." ) >= 0 ||
					text.indexOf( ".@" ) >= 0 ||
					text.indexOf( ".." ) >= 0 ||
					hasCharType( text, ImageFont.CHAR_TYPE_ACCENT_LOWER_CASE ) ||
					hasCharType( text, ImageFont.CHAR_TYPE_ACCENT_UPPER_CASE ) ) {
				if ( firstError < 0 ) {
					firstError = ENTRY_EMAIL;
					errorMessage = TEXT_REGISTER_ERROR_EMAIL_FORMAT;
				}
				textBoxes[ENTRY_EMAIL].getBorder().setState( Border.STATE_ERROR );
			}
		} else {
			if ( firstError < 0 ) {
				firstError = ENTRY_EMAIL;
				errorMessage = TEXT_REGISTER_ERROR_EMAIL_LENGTH;
			}
			textBoxes[ENTRY_EMAIL].getBorder().setState( Border.STATE_ERROR );
		}

		// verifica se a data de nascimento é válida
		if ( textBoxes[ENTRY_BIRTHDAY_DAY].getText().length() > 0 &&
				textBoxes[ENTRY_BIRTHDAY_MONTH].getText().length() > 0 &&
				textBoxes[ENTRY_BIRTHDAY_YEAR].getText().length() > 0 ) {
			int year = Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_YEAR].getText() );
			if ( textBoxes[ENTRY_BIRTHDAY_YEAR].getText().length() < 4 ) {
				// trata casos onde o jogador não insere todos os dígitos do ano
				final Date date = new Date( System.currentTimeMillis() );
				final Calendar calendar = Calendar.getInstance();
				calendar.setTime( date );

				if ( year >= calendar.get( Calendar.YEAR ) - 2000 ) {
					year = 1900 + year;
				} else {
					// considera que é mais provável jogador ter menos de 10 anos de idade do que mais de 100
					year = 2000 + year;
				}
				textBoxes[ENTRY_BIRTHDAY_YEAR].setText( String.valueOf( year ), false );
			}

			switch ( AppMIDlet.isValidBirthday( Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_DAY].getText() ),
					Integer.parseInt( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() ),
					year ) ) {
				case 1:
					if ( firstError < 0 ) {
						firstError = ENTRY_BIRTHDAY_DAY;
						errorMessage = TEXT_REGISTER_ERROR_INVALID_DAY;
					}
					textBoxes[ENTRY_BIRTHDAY_DAY].getBorder().setState( Border.STATE_ERROR );
					break;

				case 2:
					if ( firstError < 0 ) {
						firstError = ENTRY_BIRTHDAY_MONTH;
						errorMessage = TEXT_REGISTER_ERROR_INVALID_MONTH;
					}
					textBoxes[ENTRY_BIRTHDAY_MONTH].getBorder().setState( Border.STATE_ERROR );
					break;
			}
		} else {
			if ( firstError < 0 ) {
				if ( textBoxes[ENTRY_BIRTHDAY_DAY].getText().length() <= 0 ) {
					firstError = ENTRY_BIRTHDAY_DAY;
					errorMessage = TEXT_REGISTER_ERROR_INVALID_DAY;
					textBoxes[ENTRY_BIRTHDAY_DAY].getBorder().setState( Border.STATE_ERROR );
				} else if ( textBoxes[ENTRY_BIRTHDAY_MONTH].getText().length() <= 0 ) {
					firstError = ENTRY_BIRTHDAY_MONTH;
					errorMessage = TEXT_REGISTER_ERROR_INVALID_MONTH;
					textBoxes[ENTRY_BIRTHDAY_MONTH].getBorder().setState( Border.STATE_ERROR );
				} else {
					firstError = ENTRY_BIRTHDAY_YEAR;
					errorMessage = TEXT_REGISTER_ERROR_INVALID_YEAR;
					textBoxes[ENTRY_BIRTHDAY_YEAR].getBorder().setState( Border.STATE_ERROR );
				}
			}
		}

		// verifica o tamanho e formato dos campos "password" e "confirmação da password"
		text = textBoxes[ENTRY_PASSWORD].getText();
		final String confirm = textBoxes[ENTRY_PASSWORD_CONFIRM].getText();
		if ( text.length() >= Customer.PASSWORD_MIN_CHARS ) {
			final boolean hasAlpha = hasCharType( text, ImageFont.CHAR_TYPE_REGULAR ) || hasCharType( text, ImageFont.CHAR_TYPE_ACCENT_UPPER_CASE ) || hasCharType( text, ImageFont.CHAR_TYPE_ACCENT_LOWER_CASE );
			final boolean hasDigit = hasCharType( text, ImageFont.CHAR_TYPE_NUMERIC );

			if ( hasAlpha && hasDigit ) {
				if ( text.compareTo( confirm ) != 0 ) {
					// passwords não conferem
					if ( firstError < 0 ) {
						firstError = ENTRY_PASSWORD_CONFIRM;
						errorMessage = TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION;
					}
					textBoxes[ENTRY_PASSWORD_CONFIRM].getBorder().setState( Border.STATE_ERROR );
				}
			} else {
				// password muito fraca
				if ( firstError < 0 ) {
					firstError = ENTRY_PASSWORD;
					errorMessage = TEXT_REGISTER_ERROR_PASSWORD_WEAK;
				}
				textBoxes[ENTRY_PASSWORD].getBorder().setState( Border.STATE_ERROR );
			}
		} else {
			// password muito curta
			if ( firstError < 0 ) {
				firstError = ENTRY_PASSWORD;
				errorMessage = TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT;
			}
			textBoxes[ENTRY_PASSWORD].getBorder().setState( Border.STATE_ERROR );
		}

		if ( firstError >= 0 ) {
			textBoxes[firstError].getBorder().setState( Border.STATE_ERROR );
			NanoOnline.getProgressBar().showError( errorMessage );
			NanoOnline.getForm().requestFocus( textBoxes[firstError] );
		}

		// se o índice ainda for negativo, é porque não houve erro na validação
		return firstError < 0;
	}


	private final boolean isAlpha( char c ) {
		switch ( ImageFont.getCharType( c ) ) {
			case ImageFont.CHAR_TYPE_ACCENT_LOWER_CASE:
			case ImageFont.CHAR_TYPE_ACCENT_UPPER_CASE:
			case ImageFont.CHAR_TYPE_REGULAR:
				return true;

			default:
				return false;
		}
	}


	private final boolean hasCharType( String text, byte type ) {
		final char[] chars = text.toCharArray();

		for ( byte i = 0; i < chars.length; ++i ) {
			if ( ImageFont.getCharType( chars[i] ) == type ) {
				return true;
			}
		}

		return false;
	}


	/**
	 * 
	 */
	private final void onConnectionEnded( boolean forceCancel ) {
		if ( forceCancel ) {
			NanoConnection.cancel( currentConnection );
		}
		currentConnection = -1;

		setInConnection( false );
	}


	public final void setFocus( boolean focus ) {
		super.setFocus( focus );

		if ( currentConnection < 0 && focus ) {
			onConnectionEnded( false );
		}
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				lastAction = sourceId;
				
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
						onBack();
						evt.consume();
					break;

					case ENTRY_BUTTON_EDIT:
						// atualiza informações do servidor antes de permitir edição
						NanoOnline.setScreen( SCREEN_SYNC_CUSTOMER_INFO, SCREEN_PROFILE_VIEW );
						evt.consume();
					break;

					case ENTRY_BUTTON_ERASE:
						removeAllComponents();
						try {
							final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 5 );
							text.setText( NanoOnline.getText( TEXT_ERASE_PROFILE_CONFIRM ) );
							text.addEventListener( this );
							insertDrawable( text );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								AppMIDlet.log( e , "31");
							//#endif

							NanoOnline.setScreen( SCREEN_PROFILE_VIEW );
						}

						buttonEditOK.setId( ENTRY_BUTTON_ERASE_CONFIRM );
						buttonEditOK.setText( NanoOnline.getText( TEXT_CONFIRM ), false );
						buttonCancel.setId( ENTRY_BUTTON_ERASE_BACK );

						insertDrawable( buttonEditOK );
						insertDrawable( buttonCancel );

						refreshLayout();
						evt.consume();
					break;

					case ENTRY_BUTTON_ERASE_BACK:
						NanoOnline.setScreen( SCREEN_PROFILE_VIEW );
						evt.consume();
					break;

					case ENTRY_BUTTON_ERASE_CONFIRM:
						NanoOnline.removeProfile( profileIndex, true );
						NanoOnline.setScreen( SCREEN_PROFILE_SELECT );
						break;

					case ENTRY_BUTTON_USE:
						final Customer c = NanoOnline.getCustomer( profileIndex );
						if ( c.isRememberPassword() ) {
							NanoOnline.setCurrentCustomerIndex( profileIndex );
							NanoOnline.setScreen( SCREEN_PROFILE_SELECT );
						} else {
							NanoOnline.setScreen( SCREEN_LOGIN );
						}
						break;

					case ENTRY_BUTTON_OK:
						if ( validate() ) {
							try {
								final ByteArrayOutputStream b = new ByteArrayOutputStream();
								final DataOutputStream out = new DataOutputStream( b );

								// escreve os dados globais antes de adicionar as informações específicas para registro do usuário
								final Customer customer = newProfile ? new Customer() : NanoOnline.getCustomer( profileIndex );
								NanoOnline.writeGlobalData( out, customer.getId() );

								out.writeByte( PARAM_NICKNAME );
								out.writeUTF( textBoxes[ENTRY_NICKNAME].getText() );

								out.writeByte( PARAM_EMAIL );
								out.writeUTF( textBoxes[ENTRY_EMAIL].getText() );

								out.writeByte( PARAM_PASSWORD );
								out.writeUTF( textBoxes[ENTRY_PASSWORD].getText() );

								out.writeByte( PARAM_FIRST_NAME );
								out.writeUTF( textBoxes[ENTRY_FIRST_NAME].getText() );

								out.writeByte( PARAM_LAST_NAME );
								out.writeUTF( textBoxes[ENTRY_LAST_NAME].getText() );

								final long time = AppMIDlet.getTime( Integer.valueOf( textBoxes[ENTRY_BIRTHDAY_DAY].getText() ).intValue(),
																	 Integer.valueOf( textBoxes[ENTRY_BIRTHDAY_MONTH].getText() ).intValue(),
																	 Integer.valueOf( textBoxes[ENTRY_BIRTHDAY_YEAR].getText() ).intValue() );

								out.writeByte( PARAM_BIRTHDAY );
								out.writeLong( time );

								out.writeByte( PARAM_GENDER );
								switch ( radioGender.getCheckedIndex() ) {
									case GENDER_INDEX_FEMALE:
										out.writeChar( Customer.GENDER_FEMALE );
										break;

									case GENDER_INDEX_MALE:
										out.writeChar( Customer.GENDER_MALE );
										break;

									case GENDER_INDEX_NOT_INFORMED:
									default:
										out.writeChar( Customer.GENDER_NOT_INFORMED );
										break;
								}

								out.writeByte( PARAM_PHONE_NUMBER );
								out.writeUTF( textBoxes[ENTRY_PHONE_NUMBER].getText() );

								// se o usuário já estiver registrado, apenas atualiza suas informações
								if ( customer.isRegistered() ) {
									out.writeByte( PARAM_CUSTOMER_ID );
									out.writeInt( customer.getId() );
									out.writeByte( PARAM_TIMESTAMP );
									out.writeLong( customer.getTimeStamp() );

									out.writeByte( PARAM_INFO_END );
									out.flush();
									currentConnection = NanoConnection.post( NANO_ONLINE_URL + "customers/refresh", b.toByteArray(), this, false );
								} else {
									out.writeByte( PARAM_INFO_END );
									out.flush();
									currentConnection = NanoConnection.post( NANO_ONLINE_URL + "customers/add", b.toByteArray(), this, false );
								}
								setInConnection( true );
							} catch ( Exception ex ) {
								//#if DEBUG == "true"
//# 									AppMIDlet.log( ex , "32");
								//#endif
							}
						}
						break;

					case ENTRY_BUTTON_CANCEL:
					case ENTRY_BUTTON_BACK:
						onBack();
						evt.consume();
					break;

					case ENTRY_BUTTON_FORGOT_PASSWORD:
						try {
							final ByteArrayOutputStream b = new ByteArrayOutputStream();
							final DataOutputStream out = new DataOutputStream( b );

							// escreve os dados globais antes de adicionar as informações específicas para registro do usuário
							NanoOnline.writeGlobalData( out );

							out.writeByte( ENTRY_NICKNAME );
							out.writeUTF( textBoxes[ ENTRY_NICKNAME ].getText() );

							out.flush();
							currentConnection = NanoConnection.post( NANO_ONLINE_URL + "customers/remember_password", b.toByteArray(), this, false );

							setInConnection( true );
						} catch ( Exception ex ) {
							//#if DEBUG == "true"
//# 								AppMIDlet.log( ex , "33");
							//#endif
						}
					break;

					case ENTRY_BUTTON_RESEND_CONFIRMATION:
						removeAllComponents();
						try {
							final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 5 );
							text.setText( NanoOnline.getText( TEXT_VALIDATION_EMAIL_SENT_1 ) + NanoOnline.getCustomer( profileIndex ).getEmail() );
							text.addEventListener( this );
							insertDrawable( text );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 								AppMIDlet.log( e , "34");
							//#endif

							NanoOnline.setScreen( SCREEN_PROFILE_VIEW );
						}

						buttonEditOK.setId( ENTRY_BUTTON_RESEND_CONFIRM );
						buttonEditOK.setText( NanoOnline.getText( TEXT_CONFIRM ), false );
						buttonCancel.setId( ENTRY_BUTTON_ERASE_BACK );

						insertDrawable( buttonEditOK );
						insertDrawable( buttonCancel );

						refreshLayout();
					break;

					case ENTRY_BUTTON_RESEND_CONFIRM:
						try {
							final ByteArrayOutputStream b = new ByteArrayOutputStream();
							final DataOutputStream out = new DataOutputStream( b );

							// escreve os dados globais antes de adicionar as informações específicas para registro do usuário
							final Customer customer = NanoOnline.getCustomer( profileIndex );
							NanoOnline.writeGlobalData( out, customer.getId() );

							out.writeByte( PARAM_CUSTOMER_ID );
							out.writeInt( customer.getId() );

							// se o usuário já estiver registrado, apenas atualiza suas informações
							currentConnection = NanoConnection.post( NANO_ONLINE_URL + "customers/revalidate", b.toByteArray(), this, false );
							setInConnection( true );
						} catch ( Exception ex ) {
							//#if DEBUG == "true"
//# 									AppMIDlet.log( ex , "35");
							//#endif
						}
					break;
				}
			break;

			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
			break;

			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				switch ( sourceId ) {
					case ENTRY_EMAIL:
						refreshEmailValidationText();
					case ENTRY_NICKNAME:
					case ENTRY_FIRST_NAME:
					case ENTRY_LAST_NAME:
//					case ENTRY_CPF:
					case ENTRY_PHONE_NUMBER:
					case ENTRY_PASSWORD:
					case ENTRY_PASSWORD_CONFIRM:
						if ( evt.source.getBorder().getState() != Border.STATE_ERROR ) {
							evt.source.getBorder().setState( evt.eventType == Event.EVT_FOCUS_GAINED ? Border.STATE_FOCUSED : Border.STATE_UNFOCUSED );
						}

						// se estiver no modo editável, altera o label da soft key direita ao dar foco a um dos textBox
						if ( buttonCancel.getId() == ENTRY_BUTTON_CANCEL && currentConnection < 0 ) {
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CLEAR ) );
						}
						break;

					default:
						if ( currentConnection < 0 ) {
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
						}
				}
				break;

			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();

				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
						break;
				} // fim switch ( key )
			break;

			case Event.EVT_TEXTBOX_LEFT_ON_START:
				evt.consume();
				evt.source.keyReleased( 0 );
				evt.source.keyPressed( ScreenManager.UP );
			break;

			case Event.EVT_TEXTBOX_RIGHT_ON_END:
				evt.consume();
				evt.source.keyReleased( 0 );
				evt.source.keyPressed( ScreenManager.DOWN );
			break;

			case Event.EVT_TEXTBOX_FILLED:
				evt.consume();
				evt.source.keyReleased( 0 );
				getComponentForm().keyPressed( ScreenManager.DOWN );
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	protected final void onBack() {
		if ( currentConnection >= 0 ) {
			onConnectionEnded( true );
		} else {
			super.onBack();
		}
	}


	/**
	 * 
	 * @param inConnection
	 */
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[i].setEnabled( !inConnection );
		}
		buttonEditOK.setEnabled( !inConnection );
		buttonCancel.setEnabled( !inConnection );

		radioGender.setEnabled( !inConnection );
		checkBoxRememberPassword.setEnabled( !inConnection );

		if ( inConnection ) {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CANCEL ) );
		} else {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
		}
	}


	/**
	 * 
	 */
	private final void setEditable() {
		buttonEditOK.setText( NanoOnline.getText( TEXT_OK ), false );
		buttonEditOK.setId( ENTRY_BUTTON_OK );

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[i].setEnabled( true );
		}

		buttonCancel.setText( NanoOnline.getText( TEXT_CANCEL ), false );
		buttonCancel.setId( ENTRY_BUTTON_CANCEL );

		textBoxes[ENTRY_NICKNAME].requestFocus();

		radioGender.setEnabled( true );
		checkBoxRememberPassword.setEnabled( true );

		if ( !newProfile ) {
			removeDrawable( buttonUse );
			removeDrawable( buttonErase );
			removeDrawable( buttonResendConfirmation );
			refreshLayout();
		}
	}


	/**
	 * Atualiza o label indicando o status de validação do e-mail do usuário.
	 */
	private final void refreshEmailValidationText() {
		if ( textEmailValidation != null ) {
			final Customer customer = NanoOnline.getCustomer( profileIndex );
			if ( customer.isExpired() ) {
				textEmailValidation.setText( NanoOnline.getText( TEXT_EMAIL_VALIDATION_EXPIRED ) );
			} else {
				if ( customer.hasExpirationDate() )
					textEmailValidation.setText( NanoOnline.getText( TEXT_VALIDATE_EMAIL_BEFORE ) + AppMIDlet.formatTime( customer.getValidateBefore(), NanoOnline.getText( TEXT_TIME_FORMAT ) ) );
				else if ( !previousEmail.toLowerCase().equals( textBoxes[ ENTRY_EMAIL ].getText().toLowerCase() ))
					textEmailValidation.setText( NanoOnline.getText( TEXT_REMEMBER_TO_VALIDATE ) );
				else
					textEmailValidation.setText( NanoOnline.getText( TEXT_EMAIL_VALIDATED ) );
			}
			// o refreshLayout não é estritamente necessário, mas sem ele pode ocorrer do texto precisar fazer scroll para
			// ser exibido por completo
			refreshLayout();
		}
	}


	/**
	 * 
	 * @param index
	 */
	public static final void setProfileIndex( int index ) {
		profileIndex = ( byte ) index;
	}


	public static final byte getProfileIndex() {
		return profileIndex;
	}


	public static final int getProfileId() {
		final Customer c = NanoOnline.getCustomer( profileIndex );

		if ( c == null ) {
			return Customer.ID_NONE;
		}

		return c.getId();
	}


}
