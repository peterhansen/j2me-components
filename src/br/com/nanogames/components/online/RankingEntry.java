/**
 * RankingEntry.java
 * 
 * Created on 1/Fev/2009, 12:12:34
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author Peter
 */
public final class RankingEntry implements Serializable {

	/** Pontuação do jogador. */
	private long score;

	/** Id do perfil do jogador. */
	private int profileId = Customer.ID_NONE;

	/** Apelido do jogador (armazena aqui também pois o nickname normalmente se refere a perfis não salvos no aparelho). */
	private String nickname = "";

	/** Posição da entrada no ranking online. A contagem é natural, ou seja, o 1º lugar do ranking é o número 1, e não zero. */
	private int position = -1;
	
	/** Momento da realização do recorde. */
	private long timeStamp = Long.MIN_VALUE;


	public final String getNickname() {
		return nickname;
	}


	public final void setNickname( String nickname ) {
		this.nickname = nickname;
	}


	public final int getProfileId() {
		return profileId;
	}


	public final void setProfileId( int profileId ) {
		this.profileId = profileId;
	}


	public final long getScore() {
		return score;
	}


	public final void setScore( long score ) {
		this.score = score;
	}


	public final int getPosition() {
		return position;
	}


	public final void setPosition( int position ) {
		this.position = position;
	}
	
	
	public final long getTimeStamp() {
		return timeStamp;
	}


	public final void setTimeStamp( long timeStamp ) {
		this.timeStamp = timeStamp;
	}
	

	public final void write( DataOutputStream output ) throws Exception {
		//#if DEBUG == "true"
//# 			//System.out.println( "Gravando entrada de ranking:\n\tscore: " + getScore() + "\n\tnickname: " + getNickname() + "\n\tprofile id: " + getProfileId() + "\n\tsubmitted: " + isSubmitted() );
		//#endif		
		output.writeLong( getScore() );
		output.writeUTF( getNickname() );
		output.writeInt( getProfileId() );
		output.writeInt( getPosition() );
		output.writeLong( getTimeStamp() );
	}


	public final void read( DataInputStream input ) throws Exception {
		//#if DEBUG == "true"
//# 			//System.out.println( "Lendo entrada de ranking..." );
		//#endif
		
		setScore( input.readLong() );
		setNickname( input.readUTF() );
		setProfileId( input.readInt() );
		setPosition( input.readInt() );
		setTimeStamp( input.readLong() );
		
		//#if DEBUG == "true"
//# 			//System.out.println( "Leu entrada de ranking:\n\tscore: " + getScore() + "\n\tnickname: " + getNickname() + "\n\tprofile id: " + getProfileId() + "\n\tsubmitted: " + isSubmitted() );
		//#endif			
	}


	/**
	 * 
	 * @param entry
	 */
	public final void copyFrom( RankingEntry entry ) {
		setNickname( entry.getNickname() );
		setProfileId( entry.getProfileId() );
		setScore( entry.getScore() );
		setPosition( entry.getPosition() );
	}


	//#if DEBUG == "true"
//# 		public final String toString() {
//# 			return getPosition() + " -> " + getNickname() + "(" + getProfileId() + "): " + getScore() + " / " + getTimeStamp();
//# 		}
	//#endif
	
	
}
