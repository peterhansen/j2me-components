/**
 * RankingFormatter.java
 * 
 * Created on 1/Fev/2009, 11:59:33
 *
 */

package br.com.nanogames.components.online;

/**
 * Essa interface é utilizada para inicializar tabelas de ranking local, e também para formatar a exibição da pontuação
 * na tabela de ranking. É importante que os valores iniciais da tabela respeitem a ordem (crescente ou decrescente) do
 * ranking, para evitar tabelas inconsistentes.
 * @author Peter
 */
public interface RankingFormatter {

	/** Tipo de ranking: melhor pontuação, decrescente. */
	public static final byte RANKING_TYPE_BEST_SCORE_DECRESCENT	= 0;

	/** Tipo de ranking: melhor pontuação, crescente. */
	public static final byte RANKING_TYPE_BEST_SCORE_CRESCENT	= 1;

	/** Tipo de ranking: pontuação cumulativa, decrescente. */
	public static final byte RANKING_TYPE_CUMULATIVE_DECRESCENT	= 2;

	/** Tipo de ranking: pontuação cumulativa, crescente. */
	public static final byte RANKING_TYPE_CUMULATIVE_CRESCENT	= 3;

	/** Tipo de ranking: média de pontos, decrescente. */
	public static final byte RANKING_TYPE_AVERAGE_DESCRESCENT	= 4;

	/** Tipo de ranking: média de pontos, crescente. */
	public static final byte RANKING_TYPE_AVERAGE_CRESCENT		= 5;


	/**
	 * Formata uma pontuação.
	 * @param index índice do ranking no jogo.
	 * @param score pontuação a ser formatada.
	 * @return string representando a pontuação de acordo com a formatação especificada.
	 * @see initLocalEntry
	 * @see getRankingType
	 */
	public String format( int index, long score );
	
	
	/**
	 * Inicializa uma entrada de ranking local.
	 * @param rankingIndex subtipo de ranking.
	 * @param entry entrada a ser formatada.
	 * @param entryIndex índice da entrada no ranking, sendo o índice zero a melhor pontuação.
	 * @see format
	 * @see getRankingType
	 */
	public void initLocalEntry( int rankingIndex, RankingEntry entry, int entryIndex );

	
	/**
	 * Indica o tipo de ranking. Os valores válidos estão definidos na classe RankingScreen.
	 *
	 * @param index índice do ranking no jogo.
	 * @return tipo do ranking, conforme definido na classe RankingScreen.
	 * @see RankingScreen
	 * @see format
	 * @see initLocalEntry
	 */
	public int getRankingType( int index );

}
