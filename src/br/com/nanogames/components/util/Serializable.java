/*
 * Serializable.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 *
 * @author peter
 */
public interface Serializable {
 
	/**
	 * Grava os dados do objeto num stream para recuperação posterior.
	 * @param output stream de dados onde serão gravados os dados do objeto. Não é necessário chamar o método flush().
	 */
	public abstract void write( DataOutputStream output ) throws Exception;
	
	
	/**
	 * Recupera o objeto a partir de um stream de dados salvo anteriormente.
	 * @param input stream de dados de onde serão lidos os dados do objeto.
	 */
	public abstract void read( DataInputStream input ) throws Exception;
}
 
