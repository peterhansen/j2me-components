/**
 * Logger.java
 * ©2008 Nano Games.
 *
 * Created on 07/03/2008 19:00:04.
 */

package br.com.nanogames.components.util;


import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import java.io.DataInputStream;
import java.io.DataOutputStream;


/**
 * 
 * @author Daniel L. Alves
 */
public final class Logger implements Serializable
{
	/** Objeto que irá armazenar o log enquanto o objeto existir */
	private static final StringBuffer sBuffer = new StringBuffer();
	
	/** Nome da base onde estamos salvando o log */
	private static String dbName;
	
	/** Índice da base onde estamos salvando o log */
	private static int dbIndex;
	
	//private static final String DATABASE_NAME = "log";
	
	/** Construtor privado para não deixar o usuário instanciar a classe */
	private Logger()
	{
	}
	
	
	public static final void setRMS( String databaseName, int index )
	{
		dbName = databaseName;
		dbIndex = index;
	}
	
	
	/** Acrescenta a string s ao log */
	public static final void log( String s )
	{
		sBuffer.append( s );
		sBuffer.append( "\n" );
		
		saveLog( dbName, dbIndex );
	}
		
	
	public static final void log( String s, String databaseName, int index )
	{
		setRMS( databaseName, index );
		log( s );
	}
	
	
	public static final boolean saveLog( String databaseName, int index )
	{
		try
		{
			AppMIDlet.saveData( databaseName, index, new Logger() );
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
//# 				AppMIDlet.log( ex , "80");
			//#endif
			
			return false;
		}
		return true;
	}
	
	
	public static final boolean loadLog( String databaseName, int index )
	{
		try
		{
			AppMIDlet.loadData( databaseName, index, new Logger() );
		}
		catch( Exception ex )
		{
			//#if DEBUG == "true"
//# 				AppMIDlet.log( ex , "81" );
			//#endif
				
			return false;
		}
		return true;
	}

	
	public final void write( DataOutputStream output ) throws Exception
	{
		if( sBuffer.length() > 0 )
			output.writeUTF( sBuffer.toString() );
	}
	

	public final void read( DataInputStream input ) throws Exception
	{
		sBuffer.delete( 0, sBuffer.length() );
		sBuffer.append( input.readUTF() );
	}
	
	
	public static final Drawable getLogScreen( int nextScreenIndex, ImageFont font ) throws Exception
	{
		return new BasicTextScreen( nextScreenIndex, font, sBuffer.toString(), false );
	}
}
