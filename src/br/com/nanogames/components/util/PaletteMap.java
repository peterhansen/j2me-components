/**
 * PaletteMap.java
 * ©2008 Nano Games.
 *
 * Created on 14/03/2008 18:28:31.
 */

package br.com.nanogames.components.util;


/**
 * 
 * @author Daniel L. Alves
 */
public final class PaletteMap
{
	/** Cor RGB original da paleta que irá ser modificada*/
	public byte srcR, srcG, srcB;
	
	/** Cor RGB que será utilizada para substituir a cor original */
	public byte newR, newG, newB;
	
	
	/** Construtor */
	public PaletteMap( int srcR, int srcG, int srcB, int newR, int newG, int newB )
	{
		this.srcR = ( byte )srcR;
		this.srcG = ( byte )srcG;
		this.srcB = ( byte )srcB;
		this.newR = ( byte )newR;
		this.newG = ( byte )newG;
		this.newB = ( byte )newB;
	}
	
	
	public PaletteMap( int sourceRGB, int newRGB ) {
		this( ( sourceRGB & 0x00ff0000 ) >> 16, ( sourceRGB & 0x0000ff00 ) >> 8, sourceRGB & 0x000000ff,
			  ( newRGB & 0x00ff0000 ) >> 16, ( newRGB & 0x0000ff00 ) >> 8, newRGB & 0x000000ff );
	}
}
