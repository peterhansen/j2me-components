/*
 * MediaPlayer.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;
//#if JAVA_VERSION == "ANDROID"
//# import android.content.Context;
//# import android.os.Vibrator;
//# import android.util.Log;
//# import javax.microedition.io.ResourceDictionary;
//# import br.com.nanogames.MIDP.MainActivity;
//#endif

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
//#if J2SE == "false"
import javax.microedition.lcdui.Display;
import javax.microedition.media.Control;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.PitchControl;
import javax.microedition.media.control.TempoControl;
import javax.microedition.media.control.VolumeControl;
//#else
//#endif

/**
 *
 * @author peter
 */
public final class MediaPlayer implements Serializable {
	
        //#if JAVA_VERSION == "ANDROID"
//#         private static Vibrator vibrator = ( Vibrator ) MainActivity.getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        //#endif
    
	/** Tempo em que ocorreu a última alteração nas opções de som. Este valor é utilizado para garantir um intervalo
	 * mínimo entre 2 mudanças de opção no som, evitando assim a quebra do aplicativo em alguns aparelhos (em especial
	 * aparelhos Samsung).
	 */
	private static long lastMuteToggledTime;
	
	/** intervalo mínimo em milisegundos entre 2 mudanças de opção do som */
	private static final short MIN_MUTE_TOGGLE_INTERVAL = 500;
	
	/** indica se o tocador de sons está mudo. Definir o tocador como mudo não altera o volume. */
	private static boolean muted;
	
	/** indica se a vibração está ativada */
	private static boolean vibration = true;
	 
	/** valor a ser passado para o tocador de som no caso de se tocar um som infinitas vezes seguidas */
	//#if SAMSUNG_API == "true"
//# 	public static final byte LOOP_INFINITE = 127;
	//#else
		public static final byte LOOP_INFINITE = -1;
	//#endif
	
	/** índice do último som tocado */
	private static byte lastPlayedIndex = -1;
	
	//#if J2SE == "false"
		/** referência para o Display (utilizado para vibrar) */
		private static Display display;

		/** tocador de som. Só há um tocador ativo a cada momento, devido a problemas em vários aparelhos ao se pré-alocar
		 * vários sons. */
		protected static Player player;

		/** Referência para o controle de volume do player. */
		private static VolumeControl volumeControl;
	//#endif
	
	/** endereços dos sons a serem tocados e seus respectivos tipos de mídia */
	protected static String[][] filenames;

	/***/
//	private static byte[][] sounds;

//	private static boolean[] soundsPreLoaded;
	
	protected static String databaseName;
	protected static int databaseIndex;
	
	public static final String MEDIA_TYPE_WAV			= "audio/x-wav";
	public static final String MEDIA_TYPE_AU			= "audio/basic";
	public static final String MEDIA_TYPE_MP3			= "audio/mpeg";
	public static final String MEDIA_TYPE_MID			= "audio/midi";
	public static final String MEDIA_TYPE_AMR			= "audio/amr";
	public static final String MEDIA_TYPE_TONE_SEQUENCE	= "audio/x-tone-seq";
	
	//#if J2SE == "false"
		private static final String[] SUPPORTED_TYPES = Manager.getSupportedContentTypes( null );
	//#else
//# 		private static final String[] SUPPORTED_TYPES = new String[] {}; // TODO J2SE
	//#endif
	
	private static final Hashtable extensionTable = new Hashtable();
	
	private static MediaPlayer instance;
	
	private static boolean vibrationSupported = true;
	
	/** Altura mínima da tela a partir da qual considera-se que todos os aparelhos suportam vibração. */
	private static final short VIBRATION_ON_MIN_HEIGHT = 240;

	/** Passo de aumento/decremento padrão do volume caso o usuário aperte a tecla de volume. */
	public static final byte VOLUME_STEP = 20;

	/** Nível de volume (mínimo de 0 e máximo de 100). */
	private static byte volume = 50;

	/***/
	private static boolean volumeControlSupported;
	
	/** Mutex utilizado para fazer a exclusão mútua de eventos de som. */
	private static final Mutex mutex = new Mutex();
	
	
	private MediaPlayer() {
		extensionTable.put( "wav", MEDIA_TYPE_WAV );
		extensionTable.put( "amr", MEDIA_TYPE_AMR );
		extensionTable.put( "mp3", MEDIA_TYPE_MP3 );
		extensionTable.put( "mp4", MEDIA_TYPE_MP3 );
		extensionTable.put( "mid", MEDIA_TYPE_MID );
		extensionTable.put( "midi", MEDIA_TYPE_MID );
	}
	
	
	/**
	 * Indica se o aparelho suporta determinado tipo de mídia.
	 * 
	 * @param mediaType tipo de mídia, no padrão MIME. Tipos comuns:
	 * <ul>
	 * <li>MEDIA_TYPE_WAV: "audio/x-wav"</li>
	 * <li>MEDIA_TYPE_AU: "audio/basic"</li>
	 * <li>MEDIA_TYPE_MP3: "audio/mpeg"</li>
	 * <li>MEDIA_TYPE_MID: "audio/midi"</li>
	 * <li>MEDIA_TYPE_AMR: "audio/amr"</li>
	 * <li>MEDIA_TYPE_TONE_SEQUENCE: "audio/x-tone-seq"</li>
	 * </ul>
	 * @return <i>true</i>, caso o tipo de mídia seja suportado, e <i>false</i>caso contrário.
	 */
	public static final boolean support( String mediaType ) {
		if ( SUPPORTED_TYPES != null ) {
			for ( int i = 0; i < SUPPORTED_TYPES.length; ++i ) {
				if ( SUPPORTED_TYPES[ i ].indexOf( mediaType ) >= 0 )
					return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Indica se o aparelho é capaz de vibrar. A chamada deste método não interrompe uma vibração ativa do aparelho.
	 * @return <i>true</i>, caso o aparelho seja capaz de vibrar, e <i>false</i> caso contrário.
	 * @see MediaPlayer#checkVibrationSupport
	 */
	public static final boolean isVibrationSupported() {
		return vibrationSupported;
	}

	
	/**
	 * Ativa a vibração.
	 * @param duration duração em milisegundos da vibração. O tempo exato e a forma como a vibração ocorre (contínua ou
	 * intermitente) são dependentes da implementação de cada aparelho. Para interromper a vibração, deve-se passar como
	 * argumento o valor 0 (zero).
	 * @return boolean indicando se o aparelho suporta vibração.
	 */
	public static final boolean vibrate( final int duration ) {
            
            if ( !vibrationSupported || !vibration )
                return false;
                
                
            //#if JAVA_VERSION == "ANDROID"
//#             vibrator.vibrate( duration );
//#             return true;
            //#else
	//#if J2SE == "false"
		if ( vibration ) {
			//#if DEBUG == "true"
//# 					if ( duration > 0 && duration < 300 ) {
//# 						System.out.println( "Warning: vibration time lower than 300ms may not work on some Motorola devices (" + duration + ")." );
//# 					}
			//#endif

			// normalmente não é necessário que a vibração esteja dentro de um bloco try/catch, mas alguns aparelhos
			// lançam exceção ou travam quando a vibração é ativada enquanto estão carregando, por exemplo.
			// observação quanto à duração: deve-se especificar bem seu valor, pois aparelhos como Samsung C420 vibram por
			// um tempo muito mais longo que o indicado, enquanto Motorola U9 parece só vibrar com tempo maior ou igual
			// que 300 milisegundos, por exemplo.
			try {
				//#if BLACKBERRY_API == "true"
//# 						final Thread t = new Thread() {
//# 							public final void run() {
//# 								display.vibrate( duration );
//# 							}
//# 						};
//# 						t.setPriority( Thread.MAX_PRIORITY );
//# 						t.start();
//# 						
//# 						return true;
				//#else
					return display.vibrate( duration );
				//#endif
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "90");
				//#endif

				return false;
			}			
		} // fim if ( vibration )
	//#endif
		
	return false;
            //#endif
	} // fim do método vibrate( int )
	 
	
	/**
	 * Define o estado da vibração.
	 * @param active estado da vibração: ligada (true) ou desligada (false).
	 */
	public static final void setVibration( boolean active ) {
		vibration = active;

		// pára alguma vibração que possa estar ativa no momento caso a vibração seja desativada
		if ( !active )
			vibrate( 0 );
	} // fim do método setVibration( boolean )

	
	/**
	 * Indica se a vibração está ligada.
	 * @return <i>true</i>, caso esteja ligada, e <i>false<i> caso contrário.
	 */
	public static final boolean isVibration() {
		return vibration;
	} // fim do método isVibration()
	 
	
	/**
	 * Inicializa o tocador de sons.
	 * @param databaseName 
	 * @param databaseIndex 
	 * @param soundsPaths endereços dos arquivos de som. A ordem dos arquivos define o índice a ser passado posteriormente
	 * ao método play. Por exemplo: para se tocar o arquivo "menu.mid" presente na lista de arquivos { "splash.mid", 
	 * "shot.wav", "menu.mid", "ending.mp3" }, deve-se passar o índice 2 (3º item da lista). A extensão do arquivo define
	 * o tipo de som a ser tocado.
	 * @throws java.lang.Exception caso haja erro ao carregar os sons
	 */
	public static final void init( String databaseName, int databaseIndex, String[] soundsPaths ) throws Exception {
		mutex.acquire();
		
		// TODO utilizar um único arquivo de som, e fazer chamadas de setMediaTime para tocar os diferentes sons? (registrando um Listener para saber quando parar)
		if ( instance == null )
			instance = new MediaPlayer();		
		
		//#if J2SE == "false"
			display = Display.getDisplay( AppMIDlet.getInstance() );

			// verifica se o aparelho possui suporte a vibração
			switch ( AppMIDlet.getVendor() ) {
//#if JAVA_VERSION != "ANDROID"                            
			case AppMIDlet.VENDOR_MOTOROLA:
			case AppMIDlet.VENDOR_SAMSUNG:
			case AppMIDlet.VENDOR_SONYERICSSON:
			case AppMIDlet.VENDOR_HTC:
			case AppMIDlet.VENDOR_SIEMENS:
			case AppMIDlet.VENDOR_SAGEM_GRADIENTE:
				// considera que todos possuem vibração - o teste é diferenciado para alguns fabricantes pois nos casos 
				// da Nokia (e possivelmente outros fabricantes), há aparelhos sem suporte a vibração (todos os S60 2nd 
				// Edition, além de alguns S40 2nd Edition), enquanto alguns aparelhos Samsung indicam que não suportam
				// vibração, quando na verdade possuem o recurso (D820, por exemplo)
				vibrationSupported = true;
			break;

			default:
				vibrationSupported = ScreenManager.SCREEN_HEIGHT >= VIBRATION_ON_MIN_HEIGHT || vibrate( 1 );                            
//#else
//#                             default:
//#                                 vibrationSupported = true;
//#endif                                
			}

	//		sounds = new byte[ soundsPaths.length ][];
	//		soundsPreLoaded = new boolean[ soundsPaths.length ];

			if ( soundsPaths != null && soundsPaths.length > 0 ) {
				filenames = new String[ soundsPaths.length ][ 2 ];
				for ( int i = 0; i < filenames.length; ++i ) {
					filenames[ i ][ 0 ] = soundsPaths[ i ];
					final char[] chars = soundsPaths[ i ].toCharArray();

					// obtém a extensão do som a ser tocado
					int begin = chars.length - 1;
					for ( ; begin >= 0; --begin ) {
					   if ( chars[ begin ] == '.' )
						   break;
					} 
					final String extension = soundsPaths[ i ].substring( begin + 1 ).toLowerCase();
					filenames[ i ][ 1 ] = ( String ) extensionTable.get( extension );
				}

				// verifica se o aparelho possui controle de volume
				Player p = null;
				try {
//#if JAVA_VERSION == "ANDROID"                     
//#                                         final InputStream input = ResourceDictionary.openResource( filenames[ 0 ][ 0 ] );
//#else
				final InputStream input = display.getClass().getResourceAsStream( filenames[ 0 ][ 0 ] );
//#endif                                        
                                        
					p = createPlayer( input, filenames[ 0 ][ 1 ] );
					try {
						p.realize();
					} catch ( Exception e ) {}
					try {
						p.prefetch();
					} catch ( Exception e ) {}

					final Control[] controls = p.getControls();
					for ( byte i = 0; i < controls.length; ++i ) {
						//#if DEBUG == "true"
//# 						System.out.println( "CONTROL SUPPORTED: " + controls[ i ] );
						//#endif

						if ( controls[ i ] instanceof VolumeControl ) {
							volumeControlSupported = true;

							//#if DEBUG == "false"
								// sem estar no modo debug, o único controle desejado é o de volume
								break;
							//#endif
						}
					}
				} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 						System.out.println( "VOLUME CONTROL NOT SUPPORTED" );
					//#endif
				} finally {
				if ( p != null ) {
					try {
						p.deallocate();
					} catch ( Exception e ) {}
					try {
						p.close();
					} catch ( Exception e ) {}

					p = null;
				}
			}
		} // fim if ( soundsPaths != null && soundsPaths.length > 0 )

		//#else
		//#endif			
		
		MediaPlayer.databaseName = databaseName;
		MediaPlayer.databaseIndex = databaseIndex;
		
		// tenta carregar as opções
		loadOptions();
		
		mutex.release();
	} // fim do método init( String[] )
	
	
	/**
	 * Libera os recursos utilizados pelo tocador de sons.
	 * 
	 * @param useMutex indica se deve-se utilizar a exclusão mútua (utilizar <code>false</code> caso esse método seja chamado
	 * dentro um bloco de código onde o mutex já está em uso, como no caso do método <code>play</code>).
	 * @see free()
	 */
	private static final void free( final boolean useMutex ) {
		if ( useMutex )
			mutex.acquire();		
		
		//#if J2SE == "false"

			if ( player != null ) {
				try {
					player.deallocate();
				} catch ( Exception e ) {
					//#if DEBUG == "true"
	//# 						AppMIDlet.log( e , "91");
					//#endif
				}			

				try {
					player.close();
				} catch ( Exception e ) {
					//#if DEBUG == "true"
	//# 						AppMIDlet.log( e , "92");
					//#endif
				}						

				player = null;
			}

		//#endif
		
		if ( lastPlayedIndex >= 0 ) {
//			if ( !soundsPreLoaded[ lastPlayedIndex ] )
//				unloadSound( lastPlayedIndex, false );
			
			lastPlayedIndex = -1;
		}

		AppMIDlet.gc();

		if ( useMutex )
			mutex.release();		
	}
	 
	
	/**
	 * Libera os recursos utilizados pelo tocador de sons.
	 */
	public static final void free() {
		free( true );
	} // fim do método free()
	
	
	/**
	 * Pára de tocar o som atual, sem desalocá-lo.
	 * @param useMutex indica se deve ser utilizada a exclusão mútua no acesso ao tocador de sons. Normalmente só <b>não</b>
	 * é utilizada a exclusão mútua no caso de chamada deste método ao suspender a aplicação, para evitar demora na resposta
	 * ao método <code>hideNotify</code> caso o evento ocorra dentro da região crítica do método <code>play</code>.
	 * @see #stop()
	 */
	public static final void stop( final boolean useMutex ) {
		if ( useMutex )
			mutex.acquire();

		//#if J2SE == "false"
			if ( player != null ) {
				try {
					player.stop();
				} catch ( Exception e ) {
					//#if DEBUG == "true"
	//# 					AppMIDlet.log( e , "93");
					//#endif
				}
			} // fim if ( player != null )
		//#endif

		if ( useMutex )
			mutex.release();
	}
	 
	
	/**
	 * Pára de tocar o som atual, sem desalocá-lo, e utilizando a exclusão mútua. Equivalente à chamada de <code>stop(true)</code>.
	 * @see #stop(boolean)
	 */
	public static final void stop() {
		stop( true );
	} // fim do método stop()
	
	
	/**
	 * Toca um som uma vez. Equivalente à chamada de <code>play(index, 1)</code>.
	 * 
	 * @param index índice do som a ser tocado. O índice é determinado pela ordem dos endereços passados ao método init.
	 */
	public static final void play( int index ) {
		play( index, 1 );
	}


	/**
	 * Toca um som.
	 * @param index índice do som a ser tocado. O índice é determinado pela ordem dos endereços passados ao método init.
	 * @param loopCount número de repetições do som a ser tocado.
	 */
	public static final void play( final int index, final int loopCount ) {
		play( index, loopCount, 0 );
	}


	/**
	 * Toca um som.
	 * @param index índice do som a ser tocado. O índice é determinado pela ordem dos endereços passados ao método init.
	 * @param loopCount número de repetições do som a ser tocado.
	 * @param tempo indicação do ritmo de execução do arquivo de som (MIDI). Caso seja menor ou igual a zero, não altera
	 * o valor original.
	 */
	public static final void play( final int index, final int loopCount, final int fp_tempoMultiplier ) {
//		final Thread thread = new Thread() {
//			public final void run() {
				mutex.acquire();
				
				if ( !muted ) {
					//#if J2SE == "false"
						// não pode utilizar mutex aqui, senão causa deadlock
						stop( false );

						InputStream input = null;

						try {
							if ( index != lastPlayedIndex || fp_tempoMultiplier > 0 ) {
								// caso o índice seja diferente do último som tocado, o Player anterior é desalocado para melhorar
								// a portabilidade
								// utilizar mutex na chamada deste método causa deadlock
								free( false );

								//#if SAMSUNG_API == "true"
	//# 							switch ( AppMIDlet.getVendor() ) {
	//#
	//# 								// Alguns aparelhos Samsung não tocam sons corretamente ao utilizar a MMAPI. Nesses casos,
	//# 								// utiliza-se a API própria da Samsung. A compilação deve ser específica pois, apesar da
	//# 								// identificação durante a execução garantir que somente aparelhos Samsung executarão códigos
	//# 								// que utilizem a API específica, ocorre erro na instalação (etapa de compilação) em aparelhos
	//# 								// LG caso a classe SamsungPlayer esteja presente.
	//# 								case AppMIDlet.VENDOR_SAMSUNG:
	//# 									player = new SamsungPlayer( filenames[ index ][ 0 ] );
	//# 								break;
	//#
	//# 								default:
								//#endif

	//							loadSound( index, false );

								// TODO pré-alocar sons melhorou desempenho pra tocá-los?
	//							final ByteArrayInputStream byteInput = new ByteArrayInputStream( sounds[ index ] );
	//							player = Manager.createPlayer( byteInput, filenames[ index ][ 1 ] );
								if ( fp_tempoMultiplier > 0 ) {
									final DynamicByteArray dynByteArray = new DynamicByteArray();
//#if JAVA_VERSION == "ANDROID"
//#									dynByteArray.readInputStream( ResourceDictionary.openResource( filenames[ index ][ 0 ] ) );
//#else                                                                        
								dynByteArray.readInputStream( display.getClass().getResourceAsStream( filenames[ index ][ 0 ] ) );
//#endif                                                                        
                                                                        
									final byte[] soundData = dynByteArray.getData();
									final int fp_previousTempo = NanoMath.toFixed( ( ( soundData[ 12 ] & 0xff ) << 8 ) | ( soundData[ 13 ] & 0xff ) );
									final byte[] newTempo = NanoMath.intToByteArray( NanoMath.toInt( NanoMath.mulFixed( fp_tempoMultiplier, fp_previousTempo ) ) );
									soundData[ 12 ] = newTempo[ 2 ];
									soundData[ 13 ] = newTempo[ 3 ];

									input = new ByteArrayInputStream( soundData );
									player = createPlayer( input, filenames[ index ][ 1 ] );
								} else {
//#if JAVA_VERSION == "ANDROID"                                                      
//# 									input = ResourceDictionary.openResource( filenames[ index ][ 0 ] );
//#else
								input = display.getClass().getResourceAsStream( filenames[ index ][ 0 ] );                                                                    
//#endif                                                                    

									player = createPlayer( input, filenames[ index ][ 1 ] );
								}

								//#if SAMSUNG_API == "true"
	//# 							}
								//#endif

								try { 
									player.realize(); 
								} catch ( Exception e ) {
									//#if DEBUG == "true"
	//# 							AppMIDlet.log( e , "94");
									//#endif
								}

								try { 
									player.prefetch(); 
								} catch ( Exception e ) {
									//#if DEBUG == "true"
	//# 							AppMIDlet.log( e , "95");
									//#endif
								}
							} else {
								// setMediaTime() pode causar problemas em alguns aparelhos. No Benq EL71, por exemplo, faz com
								// que sons não sejam mais tocados após algumas repetições, causando congelamentos ao tentar
								// tocar outro som ou desligar o tocador. Em outros aparelhos, por outro lado, não chamar
								// este método ao repetir o último som tocado (ou seja, sem realocar o som) pode fazer com que
								// o som comece a ser tocado do ponto onde parou na última vez, o que normalmente significa que
								// o som começa a tocar a partir do final (não é ouvido).
								switch ( AppMIDlet.getVendor() ) {
									case AppMIDlet.VENDOR_SIEMENS:
									break;

									default:
										try { 
											player.setMediaTime( 0 );
										} catch ( Exception e ) {
											//#if DEBUG == "true"
	//# 							AppMIDlet.log( e , "96");
											//#endif
										}						
									// fim default
								}
							}

							//#if SAMSUNG_API == "false"
								// pega o controle de volume associado ao player atual
								if ( isVolumeControlSupported() ) {
									volumeControl = ( VolumeControl ) player.getControl( "VolumeControl" );
									if ( volumeControl != null )
										volumeControl.setLevel( volume );
								}
							//#endif

							player.setLoopCount( loopCount );
							player.start();
							lastPlayedIndex = ( byte ) index;
						} catch ( Exception e ) {
							//#if DEBUG == "true"
	//# 						AppMIDlet.log( e , "97");
							//#endif
						} finally {
							// após o som ser carregado, podemos fechar o stream
							if ( input != null ) {
								try {
									input.close();
								} catch ( Exception e ) {
								}
							}
						}
						
					//#endif
				} // fim if ( !muted )
				
				mutex.release();
//			}
//		};
		//thread.setPriority( Thread.MAX_PRIORITY );
//		thread.start();
//		Thread.yield();
	} // fim do método play( int, int )

	
	public static final void setMute( boolean mute ) {
		mutex.acquire();
		
		// verifica se foi decorrido o tempo mínimo entre 2 alterações do estado do tocador de som
		final long time = System.currentTimeMillis();
		if ( time - lastMuteToggledTime >= MIN_MUTE_TOGGLE_INTERVAL ) {
			lastMuteToggledTime = time;
			
			MediaPlayer.muted = mute;

			// pára de tocar sons caso o tocador seja definido como mudo
			if ( mute )
				stop( false );
		}
		
		mutex.release();
	} // fim do método setMute( boolean )

	
	public static final boolean isMuted() {
		return muted;
	}


//	/**
//	 * Define se um som deve estar sempre pré-carregado na memória.
//	 * @param index índice do som.
//	 * @param preLoaded true, para mantê-lo pré-carregado, e false caso contrário.
//	 */
//	public static final void setSoundPreLoaded( int index, boolean preLoaded ) {
//		if ( preLoaded )
//			loadSound( index, true );
//		else
//			unloadSound( index, true );
//
//		soundsPreLoaded[ index ] = preLoaded;
//	}
//
//
//	/**
//	 *
//	 * @param index
//	 * @param useMutex
//	 */
//	private static final void loadSound( int index, boolean useMutex ) {
//		if ( sounds[ index ] == null ) {
//			try {
//				if ( useMutex )
//					mutex.acquire();
//				final DynamicByteArray d = new DynamicByteArray();
//				d.readInputStream( display.getClass().getResourceAsStream( filenames[ index ][ 0 ] ) );
//				sounds[ index ] = d.getData();
//			} catch ( IOException e ) {
//				//#if DEBUG == "true"
////# 					AppMIDlet.log( e );
//				//#endif
//			} finally {
//				if ( useMutex )
//					mutex.release();
//			}
//		}
//	}
//
//
//	/**
//	 *
//	 * @param index
//	 */
//	private static final void unloadSound( int index, boolean useMutex ) {
//		if ( useMutex )
//			mutex.acquire();
//
//		sounds[ index ] = null;
//
//		if ( useMutex )
//			mutex.release();
//	}
	
	
	/**
	 * Play back a tone as specified by a note and its duration. A note is given in the range of 0 to 127 inclusive. The 
	 * frequency of the note can be calculated from the following formula:
	 * <p>SEMITONE_CONST = 17.31234049066755 = 1/(ln(2^(1/12)))
	 * <br></br>note = ln(freq/8.176)*SEMITONE_CONST
	 * <br></br>The musical note A = MIDI note 69 (0x45) = 440 Hz.</p>
	 * <p>This call is a non-blocking call. Notice that this method may utilize CPU resources significantly on devices that 
	 * don't have hardware support for tone generation.</p>
	 *
	 * @param note Defines the tone of the note as specified by the above formula.
	 * @param duration The duration of the tone in milli-seconds. Duration must be positive.
	 * @param volume Audio volume range from 0 to 100. 100 represents the maximum volume at the current hardware level. 
	 * Setting the volume to a value less than 0 will set the volume to 0. Setting the volume to greater than 100 will 
	 * set the volume to 100.
	 */
	public static final void playTone( int note, int duration, int volume ) {
		if ( !muted ) {
			try {
				//#if J2SE == "false"
					Manager.playTone( note, duration, volume );
				//#endif
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "98");
				//#endif
			}
		}
	}


	/**
	 * 
	 */
	public static final void saveOptions() {
		try {
			AppMIDlet.saveData( databaseName, databaseIndex, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e , "99");
			//#endif
		}
	} // fim do método saveOptions()
	
	
	/**
	 * Carrega as opções de som e vibração armazenadas no RMS. Este método é chamado automaticamente no método <code>init()</code>.
	 * @see #init(String, int, String[])
	 */
	public static final void loadOptions() {
		try {
			AppMIDlet.loadData( databaseName, databaseIndex, instance );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e , "100!");
			//#endif
		}
	} // fim do método loadOptions()

	
	public final void write( DataOutputStream output ) throws Exception {
		output.writeBoolean( muted );
		output.writeBoolean( vibration );
		output.writeByte( volume );
	}

	
	public final void read( DataInputStream input ) throws Exception {
		muted = input.readBoolean();
		vibration = input.readBoolean();
		volume = input.readByte();
	}
	
	
	/**
	 * Indica se o tocador de som está ativo, ou seja, se é não-nulo e está no estado Player.STARTED.
	 * @return <i>true</i>, caso o tocador esteja ativo, e <i>false</i> caso contrário.
	 */
	public static final boolean isPlaying() {
		try {
			mutex.acquire();
		
			//#if J2SE == "false"
				return player != null && player.getState() == Player.STARTED;
			//#else
//# 				return false; // TODO isPlaying J2SE
			//#endif
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e , "101");
			//#endif
			
			return false;
		} finally {
			mutex.release();
		}
	}


	/**
	 * Indica se o controle de volume é suportado pelo aparelho.
	 * @return
	 */
	public static final boolean isVolumeControlSupported() {
//#if JAVA_VERSION == "ANDROID"            
//#             return false;
//#else
	return volumeControlSupported;                
//#endif                
	}


	/**
	 * Obtém o volume do tocador de sons. Os valores variam de 0 a 100.
	 * @return
	 */
	public static final byte getVolume() {
		return volume;
	}


	/**
	 * Define o volume do tocador de sons. Caso o controle de volume não seja suportado pelo aparelho, este método não realiza
	 * nenhuma ação. Importante notar que, no caso de aparelhos que suportem esse recurso, o volume é definido indepentemente
	 * de estar mudo ou não.
	 * @param v volume do tocador de sons. O valor mínimo é 0, e o máximo é 100.
	 */
	public static final void setVolume( int v ) {
		if ( isVolumeControlSupported() ) {
			volume = ( byte ) NanoMath.clamp( v, 0, 100 );
			
			//#if J2SE == "false"
				if ( volumeControl != null )
					volumeControl.setLevel( volume );
			//#endif
		}
	}


	//#if J2SE == "false"
		/**
		 * Cria um Player a partir do InputStream recebido e do seu tipo.
		 * 
		 * @param input
		 * @param type
		 * @return
		 * @throws java.lang.Exception
		 */
		public static final Player createPlayer( InputStream input, String type ) throws Exception {
			// TODO criar som a partir de byte[] ou InputStream nos Samsung
			return Manager.createPlayer( input, type );
		}


		public static final Player getPlayer() {
			return player;
		}
	//#endif
	
}
 
