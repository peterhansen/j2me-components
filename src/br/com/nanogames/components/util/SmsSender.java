package br.com.nanogames.components.util;
//#if JAVA_VERSION == "ANDROID"
//# import android.app.Activity;
//# import android.app.PendingIntent;
//# import android.content.BroadcastReceiver;
//# import android.content.Context;
//# import android.content.Intent;
//# import android.content.IntentFilter;
//# import android.telephony.gsm.SmsManager;
//# import br.com.nanogames.MIDP.MainActivity;
//# import br.com.nanogames.components.util.SmsSenderListener;
//# 
//# 
//# /**
//#  *
//#  * @author Daniel "Monty" Monteiro
//#  */
//# public class SmsSender {
//#     /**
//#      * Identificador de eventos.
//#      */
//#     public static final String ACTION_SMS_SENT = "br.com.nanogames.components.smssender.SMS_SENT_ACTION";
//#     /**
//#      * Classe cliente do envio de SMS
//#      */
//#     private static SmsSenderListener listener;    
//#     
//#     /**
//#      * Inicialização estática da classe.
//#      * Registra o listener de intent do evento de envio de SMS.
//#      */
//#     static  {
//#         try {
//#             MainActivity.getActivity().registerReceiver(new BroadcastReceiver() {
//# 
//#                 @Override
//#                 public void onReceive(Context context, Intent intent) {
//#                     String message = null;
//#                     boolean error = true;
//#                     switch (getResultCode()) {
//#                     case Activity.RESULT_OK:
//#                         SmsSender.listener.onSMSSent( 0, true );                    
//#                         break;
//#                     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//#                     case SmsManager.RESULT_ERROR_NO_SERVICE:
//#                     case SmsManager.RESULT_ERROR_NULL_PDU:
//#                     case SmsManager.RESULT_ERROR_RADIO_OFF:
//#                         SmsSender.listener.onSMSSent( 0, false );
//#                         break;
//#                     }
//#                 }
//#             }, new IntentFilter( ACTION_SMS_SENT ) );        
//#         } catch ( Throwable t ) {
//#             SmsSender.listener.onSMSSent( 0, false );
//#         }
//#     }
//#     
//#     
//#     /**
//#      * Envia uma mensagem de texto
//#      * @param number numero do telefone de destino
//#      * @param message a mensagem a ser enviada
//#      * @param listener classe cliente.
//#      */
//#     public void send(String number, String message, SmsSenderListener listener ) {
//#         SmsSender.listener = listener;
//#         try {
//#             SmsManager sms = SmsManager.getDefault();        
//#             sms.sendTextMessage( number, null, message, PendingIntent.getBroadcast( MainActivity.getActivity(), 0, new Intent( ACTION_SMS_SENT ), 0 ), null);            
//#         } catch ( Throwable t ) {
//#             SmsSender.listener.onSMSSent( 0, false );
//#         }
//#     }    
//# }
//#else
/**
 * SmsSender.java
 * ©2008 Nano Games.
 *
 * Created on 22/07/2008 18:06:46.
 */

/**
 * @author Daniel L. Alves
 */

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.io.Connector;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif

//#if SAMSUNG_API == "true"
//# import com.samsung.util.SM;
//# import com.samsung.util.SMS;
//#endif


/** Classe responsável pelo envio de mensagens SMS */
public final class SmsSender extends Thread
{
	/** Tempo máximo pelo qual esperamos a thread de envio de SMS terminar suas tarefas */
	public static final short SMS_SENDER_MAX_WAIT_TIME = 25000;
	
	/** Thread de envio de SMS deste objeto */
	private SmsThread smsThread;
	
	/** Objeto que receberá informações sobre o andamento do envio do SMS. Esta referência também é armazenada
	 * nesta classe (e não apenas em SmsThread) para que o mesmo objeto possa ser chamado pela thread de envio
	 * de SMS e pela thread de timeout da conexão
	 */
	private SmsSenderListener listener;
	
	/** Controla o acesso às regiões críticas da operação de envio de SMS */
	private final Mutex mutex = new Mutex();
	
	/** Retorna se o device suporta o envio de SMS */
	public static final boolean isSupported()
	{
		//#if SAMSUNG_API == "true"
//# 			// É importante lembrar que nem todos aparelhos da Samsung suportam a própria API da Samsung!
//# 			// A preferência é pela API WMA do J2ME, já que na bateria de testes mostrou ser implementada por
//# 			// um maior número de devices desta fabricante
//# 			if( !supportsDefault() )
//# 			{
//# 				try
//# 				{
//# 					return SMS.isSupported();
//# 				}
//# 				catch( Throwable t )
//# 				{
//# 					return false;
//# 				}
//# 			}
//# 			return true;
		//#elif BLACKBERRY_API == "true"
//# 			// em aparelhos antigos (e/ou com versões de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms não recebe os eventos de teclado corretamente, sumindo somente após a aplicação ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					return false;
//# 					
//# 				default:
//# 					return supportsDefault();
//# 			}
		//#else
			return supportsDefault();
		//#endif
	}

	
	/** Retorna se o device suporta o envio de SMS através das APIs opcionais do J2ME ( WMA - Wireless Messaging API )*/
	private static final boolean supportsDefault() {
		// resolve verification error em alguns aparelhos BlackBerry
		//#if BLACKBERRY_API == "true"
//# 			return true;
		//#else
			try
			{
				Class.forName( "javax.wireless.messaging.MessageConnection" );
				Class.forName( "javax.wireless.messaging.TextMessage" );
				return true;
			}
			catch( Throwable t )
			{
				return false;
			}
		//#endif
	}

	
	/** Envia uma mensagem SMS
	 * @param phoneNumber Telefone para o qual a mensagem será enviada
	 * @param smsText Texto da mensagem SMS
	 * @param listener Objeto que receberá informações sobre o andamento do envio do SMS
	 */
	public final void send( String phoneNumber, String smsText, SmsSenderListener listener )
	{
		mutex.acquire();

		// Termina alguma thread de envio de SMS que possa estar ativa
		killSmsThread();

		// Troca o listener apenas depois de chamar killSmsThread(), já que tal método pode querer invocar
		// o listener anterior
		this.listener = listener;
		
		// Cria uma thread para o envio do SMS. Além de ser uma melhor prática de programação, se torna 
		// OBRIGATÓRIO, pois alguns devices (i.e A1200) disparam uma exceção quando tentamos executar uma
		// operação bloqueante na thread principal da aplicação
		smsThread = new SmsThread( phoneNumber, smsText, listener, this, mutex );
		smsThread.setPriority( Thread.MAX_PRIORITY );
		smsThread.start();
		
		mutex.release();
	}
	
	/** Controla o timeout da conexão de envio de SMS. Se faz necessário pois em alguns devices (i.e. LG KG800,
	 * LG MG320c) a exceção de timeout da conexão não é disparada. Uma das razões para isso acontecer é que nem
	 * mesmo o padrão J2ME obriga o fabricante a fazê-lo (vide documentação da classe MessageConnection)
	 */
	public final void run()
	{
		//#if DEBUG == "true"
//# 		if( listener != null )
//# 			listener.onSMSLog( smsThread.getID(), "\n\nChamou o timer" );
		//#endif

		try
		{
			sleep( SMS_SENDER_MAX_WAIT_TIME );
		}
		catch( Exception ex )
		{
			// Pode acontecer de sleep() disparar uma exceção e acabarmos chamando killSmsThread() sem esperarmos
			// um tempo razoável para o envio do SMS. Como na prática o disparo de uma exceção por sleep() se
			// mostrou inexistente, ignoramos a possibilidade
		}

		mutex.acquire();
		
		if( killSmsThread() && ( listener != null ) )
		{
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "Erro: Forçou o fim da execução" );
			//#endif

			listener.onSMSSent( smsThread.getID(), false );
		}
		
		mutex.release();
	}
	

	/** Tenta forçar o término da thread de envio de SMS. O ideal seria se pudéssemos chamar Thread.interrupt()
	 * em todos os devices. Mas como tal método só está presente a partir do CLDC 1.1 e não conseguimos pegar
	 * a versão do CLDC em tempo de execução, tentamos contornar o problema forçando o fim da conexão. Nos
	 * aparelhos LG KG800 e MG320c foi descoberta a possibilidade de um erro: caso o usuário tente enviar um SMS
	 * e não possua créditos para fazê-lo, a thread de envio ficará eternamente na memória, já que o método
	 * MessageConnection.send() não dispara exceções por timeout e/ou conexão fechada.
	 * 
	 * O mutex é sempre utilizado por fora desse método
	 * 
	 * @return Se teve ou não que matar a thread
	 */
	private final boolean killSmsThread() {
		//#if DEBUG == "true"
//# 		if( listener != null )
//# 			listener.onSMSLog( smsThread.getID(), "\n\nkillSmsThread():" );
		//#endif

		if( ( smsThread != null ) && ( smsThread.isAlive() ) ) {
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "\n- Forçou o fechamento da conexão" );
			//#endif

			smsThread.closeSmsConnection();
			return true;
		}
		//#if DEBUG == "true"
//# 		else {
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "\n- Conexão já estava fechada" );
//# 		}
		//#endif
		return false;
	}
	
	/** Thread responsável pelo envio do SMS */
	private static final class SmsThread extends Thread {
		/** Telefone para o qual a mensagem será enviada */
		private final String phoneNumber;
			
		/** Texto da mensagem SMS */
		private final String smsText;

		/** Objeto que receberá informações sobre o andamento do envio do SMS */
		private final SmsSenderListener listener;
		
		/** Conexão através da qual o SMS será enviado. Mantém esta referência na classe para que seja possível
		 * fechar a conexão por fora do método que a criou. Só é utilizada em conjunto com a API WMA. A API
		 * da Samsung não faz uso deste atributo
		 */
		private MessageConnection smsConn;

		/** Timer que irá cancelar a conexão de envio de SMS caso o tempo limite seja atingido. Se faz necessário
		 * pois em alguns devices (i.e. LG KG800, LG MG320c) a exceção de timeout da conexão não é disparada. Uma
		 * das razões para isso acontecer é que nem mesmo o padrão J2ME obriga o fabricante a fazê-lo (vide
		 * documentação da classe MessageConnection). No entanto, só é utilizado em conjunto com a API WMA.
		 * A API da Samsung não faz uso deste atributo
		 */
		private final Thread timeoutThread;

		/** Controla o acesso às regiões críticas da operação de envio de SMS */
		private final Mutex mutex;
		
		/** Variável responsável por gerar os ids das threads de envio de SMS */
		private static int idCounter = Integer.MIN_VALUE;
		
		/** ID único desta thread */
		private final int id;
		
		/** Cria uma thread de envio de SMS
		 * @param phoneNumber Telefone para o qual a mensagem será enviada
		 * @param smsText Texto da mensagem SMS
		 * @param listener Objeto que receberá informações sobre o andamento do envio do SMS
		 * @param timeoutThread Thread que controlará o timeout da conexão
		 * @param mutex Controlador de acesso às regiões críticas da operação de envio de SMS
		 */
		public SmsThread( String phoneNumber, String smsText, SmsSenderListener listener, Thread timeoutThread, Mutex mutex ) {
			this.phoneNumber = phoneNumber;
			this.smsText = smsText;
			this.listener = listener;
			this.timeoutThread = timeoutThread;
			this.mutex = mutex;
			id = newID();
		}
		
		/** Retorna um ID único para o objeto chamador */
		private static synchronized final int newID() {
			return idCounter++;
		}
		
		/** Obtém o ID único da thread */
		public final int getID() {
			return id;
		}
		
		//#if SAMSUNG_API == "true"
//# 			public final void run() {
//# 				// Verifica qual método de execução deve ser chamado de acordo com as APIs suportadas pelo device
//# 				// É importante lembrar que nem todos aparelhos da Samsung suportam a própria API da Samsung!
//# 				// Ver método SmsSender.isSupported()
//# 				if( supportsDefault() )
//# 					runDefault();
//# 				else
//# 					runSamsung();
//# 			}
//# 
//# 			/** Envia um SMS utilizando a API da Samsung. Não precisa utilizar mutex!!! */
//# 			private void runSamsung() {
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n\nrunSamsung():" );
				//#endif
//# 			
//# 				boolean ret = true;
//# 
//# 				try {
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Criando a mensagem" );
					//#endif
//# 
//# 					final SM message = new SM();
//# 					message.setData( smsText );
//# 					message.setDestAddress( phoneNumber );
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Enviando sms" );
					//#endif
//# 
//# 					SMS.send( message );
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- SMS enviado" );
					//#endif
//# 				} catch( Throwable t ) {
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Disparou uma excecao ao tentar enviar o sms:" + t.getMessage() );
					//#endif
//# 					
//# 					ret = false;
//# 				}
//# 
//# 				if( listener != null )
//# 					listener.onSMSSent( getID(), ret );
//# 			}
		//#else
			public final void run() {
				runDefault();
			}
		//#endif
		
		/** Envia um SMS utilizando a API opcional WMA do J2ME */
		private final void runDefault() {
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( getID(), "\n\nrunDefault():" );
			//#endif
			
			boolean ret = true;

			try {
				mutex.acquire();

				smsConn = null;

				// Só queremos enviar
				//final String smsConnection = "sms://:" + midlet.getAppProperty( "SMS-Port" );
				final String smsConnection = "sms://" + phoneNumber;

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Abrindo a conexão" );
				//#endif

				smsConn = ( MessageConnection )Connector.open( smsConnection, Connector.WRITE, true );

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Criando a mensagem" );
				//#endif

				final TextMessage sms = ( TextMessage )smsConn.newMessage( MessageConnection.TEXT_MESSAGE );

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Determinando o texto da mensagem: " + smsText );
				//#endif

				sms.setPayloadText( smsText  );

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Setando o timer" );
				//#endif

				timeoutThread.start();

				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Enviando o sms" );
				//#endif
				
				mutex.release();

				// O método MessageConnection.send() pode causar alguns problemas:
				// - Samsung: Alguns Samsungs (i.e. D820) não disparam execeções quando o usuário tenta enviar 
				// um SMS sem possuir créditos. Logo, retornaremos que o SMS foi enviado sem isso ter ocorrido
				// de verdade
				// - LG: Caso o usuário tente enviar um SMS e não possua créditos para fazê-lo, a execução desta
				// thread ficará eternamente bloqueada dentro de send() em alguns devices (i.e. KG800, MG320c).
				// Ver SmsSender.killSmsThread() para maiores informações sobre o workaround utilizado
				smsConn.send( sms );
			} catch( Throwable t ) {
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Erro:" + t.getMessage() );
				//#endif
				
				ret = false;
			} finally {
				closeSmsConnection();
			}

			if( listener != null )
				listener.onSMSSent( getID(), ret );
			
			mutex.release();
		}
		
		/** Fecha a conexão de envio de SMS desta thread. Este método é público para que seja possível
		 * forçar o fechamento da conexão por fora do objeto
		 * 
		 * O mutex é sempre utilizado por fora desse método
		 */
		public final void closeSmsConnection() {
			if( smsConn != null ) {
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Fechando a conexão" );
				//#endif

				try {
					smsConn.close();

					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Conexão fechada" );
					//#endif

					smsConn = null;
					AppMIDlet.gc();
					yield();
				} catch( Throwable t ) {
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Erro ao fechar a conexão:" + t.getMessage() );
					//#endif
					
					// Pode não conseguir fechar a conexão e esssa ficar eternamente aberta, no entanto isso
					// não deveria acontecer
				}
			}
		}
	}
}
//#endif