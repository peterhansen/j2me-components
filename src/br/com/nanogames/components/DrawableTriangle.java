/**
 * DrawableTriangle.java
 * 
 * Created on 15/Dez/2009, 12:18:39
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Point;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//# import java.awt.Polygon;
//#endif

/**
 *
 * @author Peter
 */
public class DrawableTriangle extends Drawable {

	public final Point p1;
	public final Point p2;
	public final Point p3;

	protected int color = 0xffffff;

	
	public DrawableTriangle() {
		this( new Point(), new Point(), new Point() );
	}

	
	public DrawableTriangle( Point p1, Point p2, Point p3 ) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	

	protected void paint( Graphics g ) {
		//#if J2SE == "false"
			g.setColor( color );
			g.fillTriangle( translate.x + p1.x, translate.y + p1.y,
							translate.x + p2.x, translate.y + p2.y,
							translate.x + p3.x, translate.y + p3.y );
		//#else
//# 			g.setColor( new Color( color ) );
//# 			final Polygon p = new Polygon();
//# 			p.addPoint( translate.x + p1.x, translate.y + p1.y );
//# 			p.addPoint( translate.x + p2.x, translate.y + p2.y );
//# 			p.addPoint( translate.x + p3.x, translate.y + p3.y );
//# 			g.fillPolygon( p );
		//#endif
	}
	
	
	public final int getColor() {
		return color;
	}


	public void setColor( int color ) {
		this.color = color;
	}
	

}
