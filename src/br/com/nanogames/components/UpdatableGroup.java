/*
 * UpdatableGroup.java
 *
 * Created on April 11, 2007, 3:07 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
/**
 *
 * @author peter
 */
public class UpdatableGroup extends DrawableGroup implements Updatable {
	
	/** array que contém referências para todos os drawables atualizáveis, para otimizar a atualização do grupo */
	protected final Updatable[] updatables;
	
	/** quantidade de atualizáveis ativos no array */
	protected short activeUpdatables;
	
	
	/** 
	 * Creates a new instance of UpdatableGroup
	 * @param slots 
	 */
	public UpdatableGroup( int slots ) {
		super( slots );
		
		updatables = new Updatable[ slots ];		
	}
	

	public void update( int delta ) {
		for ( short i = 0; i < activeUpdatables; ++i )
			updatables[ i ].update( delta );		
	}
	
	
	/**
	 * Insere um drawable no próximo slot disponível.
	 * 
	 * @param drawable drawable a ser inserido no grupo.
	 * @return o índice onde o drawable foi inserido, ou -1 caso não haja mais slots disponíveis.
	 */
	public short insertDrawable( Drawable drawable ) {
		final short retValue = super.insertDrawable( drawable );
		
		// caso o drawable tenha sido inserido com sucesso e seja atualizável, o insere também no array de atualizáveis
		if ( retValue >= 0 && drawable instanceof Updatable )
			updatables[ activeUpdatables++ ] = ( Updatable ) drawable;
			
		return retValue;
	} // fim do método insertDrawable( Drawable )
	

    /**
     * Remove um drawable do grupo.
     * @param index índice do drawable a ser removido no grupo.
     * @return uma referência para o drawable removido, ou null caso o índice seja inválido.
     */
	public Drawable removeDrawable( int index ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif
				final Drawable removed = super.removeDrawable( index );

				// caso o drawable removido seja atualizável, o remove também da lista de atualizáveis
				if ( removed instanceof Updatable ) {
					// é necessário varrer a lista pois os índices dos drawables não necessariamente são os mesmos índices
					// dos atualizáveis
					for ( short i = 0; i < activeUpdatables; ++i ) {
						if ( updatables[ i ] == removed ) {
							final int LIMIT = updatables.length - 1;
							for ( int j = i; j < LIMIT; ++j )
								updatables[ j ] = updatables[ j + 1 ];

							// reduz o contador de atualizáveis
							--activeUpdatables;
							// anula a referência no array para o último elemento (caso contrário, haveria entradas
							// duplicadas no array, podendo causar futuros vazamentos de memória)
							updatables[ LIMIT ] = null;

							break;
						} // fim if ( updatables[ i ] == removed )
					} // fim for ( short i = 0; i < activeUpdatables; ++i )
				} // fim if ( removed instanceof Updatable )

				return removed;
		//#if DEBUG == "true"
//# 			} catch (Throwable th) {
//# 				AppMIDlet.log( th, "watching closely!" );
//# 				return null;
//# 			}
		//#endif
	} // fim do método removeDrawable( int )	
	
}
