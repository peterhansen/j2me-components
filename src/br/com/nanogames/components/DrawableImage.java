/*
 * DrawableImage.java
 *
 * Created on May 4, 2007, 10:02 AM
 *
 */

package br.com.nanogames.components;

//#if AD_MANAGER == "true"
import br.com.nanogames.components.online.ad.Resource;
//#endif

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.ImageLoader;
//#if J2SE == "false"
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//# import java.awt.Image;
//# import java.net.URL;
//# import javax.imageio.ImageIO;
//#endif

/**
 *
 * @author peter
 */
public class DrawableImage extends Drawable {
	
	protected final Image image;

	/** 
	 * Largura real da imagem - esse valor é independente do tamanho do drawable, utilizado para realizar as operações 
	 * de desenho no caso de transformações.
	 */
	protected final short IMAGE_WIDTH;

	/** 
	 * Altura real da imagem - esse valor é independente do tamanho do drawable, utilizado para realizar as operações 
	 * de desenho no caso de transformações.
	 */
	protected final short IMAGE_HEIGHT;
	
	/** Referência para um objeto da classe sprite, que serve para desenhar de forma otimizada no caso de serem aplicadas
	 * transformações na imagem. Utilizando-se apenas a imagem, ocorrem erros de desenho e/ou travamentos ao rotacionar 
	 * imagens em aparelhos Samsung e Siemens.
	 */
	protected final javax.microedition.lcdui.game.Sprite sprite;


	/** Cria uma nova instância de DrawableImage.
	 * @param image imagem a partir da qual o DrawableImage será criado.
	 * @throws java.lang.Exception 
	 * @see #DrawableImage(String)
	 * @see #DrawableImage(DrawableImage)	 
	 * @see #DrawableImage(DrawableImage, int)
	 */
	public DrawableImage( Image image ) {
		this.image = image;
		
		sprite = USE_MIDP2_SPRITE ? new javax.microedition.lcdui.game.Sprite( image ) : null;
		
		//#if J2SE == "false"
			IMAGE_WIDTH = ( short ) image.getWidth();
			IMAGE_HEIGHT = ( short ) image.getHeight();
		//#else
//# 			IMAGE_WIDTH = ( short ) image.getWidth( null );
//# 			IMAGE_HEIGHT = ( short ) image.getHeight( null );
		//#endif
		
		setSize( IMAGE_WIDTH, IMAGE_HEIGHT );
	} // fim do construtor DrawableImage( Image )


	//#if AD_MANAGER == "true"
		public DrawableImage( Resource resource ) throws Exception {
			this( ( DrawableImage ) resource.getData() );
		}
	//#endif


	public DrawableImage( InputStream input ) throws Exception {
		this( Image.createImage( input ) );
	}

	
	/**
	 * Cria uma nova instância de DrawableImage a partir de uma imagem presente no caminho indicado.
	 * @param imagePath caminho da imagem.
	 * @throws java.lang.Exception caso haja erro ao alocar a imagem, ou o endereço seja inválido.
	 * @see #DrawableImage(Image)
	 * @see #DrawableImage(DrawableImage)
	 * @see #DrawableImage(DrawableImage, int)
	 */
	public DrawableImage( String imagePath ) throws Exception {
		//#if J2SE == "false"
			this( ImageLoader.loadImage( imagePath ) );
		//#else
//# 			this( ImageIO.read( imagePath.getClass().getResourceAsStream( imagePath) ) );
		//#endif
		AppMIDlet.gc();
	}
	
	
	/**
	 * Cria uma cópia de um DrawableImage. Atributos como posição, tamanho, visibilidade e etc NÃO não copiados.
	 * @param dImage DrawableImage a ser copiado.
	 * @throws java.lang.Exception caso dImage seja inválido.
	 * @see #DrawableImage(String)
	 * @see #DrawableImage(Image)
	 * @see #DrawableImage(DrawableImage, int)
	 */
	public DrawableImage( DrawableImage dImage ) {
		this( dImage.image );
	}
	
	
	/**
	 * Cria uma cópia de um DrawableImage, já sofrendo uma transformação. Esse método criará uma nova imagem na memória,
	 * porém com a vantagem de poder ser desenhada diretamente sem a necessidade de executar operações que podem ser
	 * pesadas em questão de desempenho em tempo real. Atributos como posição, tamanho, visibilidade e etc NÃO não copiados.
	 * @param dImage DrawableImage a ser copiado.
	 * @param transform inteiro representando a transformação acumulada a ser aplicada na imagem.
	 * @throws java.lang.Exception caso dImage seja inválido.
	 * @see #DrawableImage(String)
	 * @see #DrawableImage(Image)
	 */
	public DrawableImage( DrawableImage dImage, int transform ) {
		//#if J2SE == "false"
			this( Image.createImage( dImage.image, 0, 0, dImage.image.getWidth(), dImage.image.getHeight(), getTransformMIDP( transform ) ) );
		//#else
//# 			this( new BufferedImage( ( ( BufferedImage ) dImage.image ).getColorModel(), ( ( BufferedImage ) dImage.image ).getRaster(), true, null ) );
		//#endif
	}

	
	protected void paint( Graphics g ) {
		// FIXME erro no posicionamento interno da imagem em algumas combinações de rotação e espelhamento
		// FIXME rotação de imagens e sprites não-quadrados apresenta erro de clip em aparelhos Samsung (mesmo utilizando Sprite MIDP2)
		//#if J2SE == "false"
			if ( USE_MIDP2_SPRITE ) {
	 			sprite.setTransform( transformMIDP );
	 			sprite.setPosition( translate.x, translate.y );
	 			sprite.paint( g );
			} else {
				if ( transform == TRANS_NONE )
					g.drawImage( image, translate.x, translate.y, 0 );
				else
					g.drawRegion( image, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, transformMIDP, translate.x, translate.y, 0 );
			}
		//#else
//# 			final Graphics2D g2 = ( Graphics2D ) g;
//# 			if ( transform == TRANS_NONE )
//# 				g2.drawImage( image, translate.x, translate.y, null );
//# 			else
//# 				g2.drawImage( image, translate.x, translate.y, null ); // TODO
		//#endif
	}


	public void defineReferencePixel( int refPixelX, int refPixelY ) {
		super.defineReferencePixel( refPixelX, refPixelY );

		if ( USE_MIDP2_SPRITE )
			sprite.defineReferencePixel( refPixelX, refPixelY );
	}


	/**
	 * 
	 * @return
	 */
	public final Image getImage() {
		return image;
	}
	
}
