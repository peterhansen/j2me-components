package br.com.nanogames.components.basic;

/*
 * AnimatedSoftkey.java
 * 
 * Created on December 6, 2007, 8:48 PM
 */



import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;

/**
 *
 * @author peter
 */
public final class BasicAnimatedSoftkey extends UpdatableGroup {

	public static final byte DIRECTION_VERTICAL		= 0;
	public static final byte DIRECTION_HORIZONTAL	= 1;

	private byte direction;

	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;

	private byte state;

	private Drawable nextDrawable;
	
	private short nextVisibleTime;
	
	/** tempo que o label permanece no estado STATE_SHOWN (caso seja negativo, indica que é pra permanecer visível */
	private short visibleTime;
	
	/** tempo padrão de visibilidade do softkey */
	public static final short DEFAULT_VISIBLE_TIME = 3000;

	/** Duração padrão da animação de transição. */
	public static final short DEFAULT_TRANSITION_TIME = 1200;

	/** Velocidade da animação de transição, em pixels por segundo. */
	private final MUV speed = new MUV();
	
	private final byte softkeyIndex;

	/** Duração da animação de transição, em milisegundos. */
	private short transitionTime = DEFAULT_TRANSITION_TIME;
	
	private boolean inTransition;


	/**
	 *
	 * @param softkeyIndex índice da softkey a ser definida. Os valores válidos são:
	 * <ul>
	 * <li>ScreenManager.SOFT_KEY_LEFT</li>
	 * <li>ScreenManager.SOFT_KEY_MID</li>
	 * <li>ScreenManager.SOFT_KEY_RIGHT</li>
	 * </ul>
	 * @throws java.lang.Exception
	 */
	public BasicAnimatedSoftkey( byte softkeyIndex ) throws Exception {
		this( softkeyIndex, DIRECTION_VERTICAL );
	}


	/**
	 * 
	 * @param softkeyIndex índice da softkey a ser definida. Os valores válidos são:
	 * <ul>
	 * <li>ScreenManager.SOFT_KEY_LEFT</li>
	 * <li>ScreenManager.SOFT_KEY_MID</li>
	 * <li>ScreenManager.SOFT_KEY_RIGHT</li>
	 * </ul>
	 * @throws java.lang.Exception
	 */
	public BasicAnimatedSoftkey( byte softkeyIndex, byte direction ) throws Exception {
		super( 1 );
		
		this.softkeyIndex = softkeyIndex;
		setDirection( direction );
	}


	/**
	 * Define o próximo drawable a ser utilizado como softkey.
	 * @param d próximo drawable a ser utilizado como softkey. Passar <i>null</i> remove o softkey atual.
	 * @param visibleTime tempo máximo que o softkey permanece visível. Utilize zero ou valores negativos para 
	 * que ele fique permanentemente visível.
	 * @param changeNow indica se a troca deve ser imediata, ou somente após o softkey atual expirar seu tempo
	 * de visibilidade.
	 */
	public synchronized final void setNextSoftkey( Drawable d, int visibleTime, boolean changeNow ) {
		nextDrawable = d;
		nextVisibleTime = ( short ) visibleTime;

		switch ( state ) {
			case STATE_HIDDEN:
				changeDrawables();
			break;
			
			case STATE_HIDING:
			case STATE_APPEARING:
			case STATE_SHOWN:
				if ( changeNow )
					setState( STATE_HIDING );
				else { 
					// se a troca não for imediata e o softkey anterior não tiver tempo limite, define o tempo
					// limite (caso contrário, a troca jamais ocorreria)
					if ( this.visibleTime <= 0 )
						this.visibleTime = DEFAULT_VISIBLE_TIME;
				}
					
			break;
		} // fim switch ( state )
	}


	public final void update( int delta ) {
		super.update( delta );

		final Drawable d = getDrawable( 0 );
		switch ( state ) {
			case STATE_APPEARING:
				if ( !inTransition ) {
					switch ( direction ) {
						case DIRECTION_HORIZONTAL:
							d.move( speed.updateInt( delta ), 0 );
							switch ( softkeyIndex ) {
								case ScreenManager.SOFT_KEY_LEFT:
									if ( d.getPosX() >= 0 )
										setState( STATE_SHOWN );
								break;

								case ScreenManager.SOFT_KEY_RIGHT:
									if ( d.getPosX() <= 0 )
										setState( STATE_SHOWN );
								break;
							}
						break;

						case DIRECTION_VERTICAL:
							d.move( 0, speed.updateInt( delta ) );
							if ( d.getPosY() <= 0 )
								setState( STATE_SHOWN );
						break;
					}
				}
			break;

			case STATE_SHOWN:
				if ( visibleTime > 0 ) {
					visibleTime -= delta;
					if ( visibleTime <= 0 )
						setState( STATE_HIDING );
				}
			break;

			case STATE_HIDING:
				switch ( direction ) {
					case DIRECTION_HORIZONTAL:
						d.move( speed.updateInt( delta ), 0 );
						switch ( softkeyIndex ) {
							case ScreenManager.SOFT_KEY_LEFT:
								if ( d.getPosX() <= -size.x )
									setState( STATE_HIDDEN );
							break;

							case ScreenManager.SOFT_KEY_RIGHT:
								if ( d.getPosX() >= size.x )
									setState( STATE_HIDDEN );
							break;
						}
					break;

					case DIRECTION_VERTICAL:
						d.move( 0, speed.updateInt( delta ) );
						if ( d.getPosY() >= size.y )
							setState( STATE_HIDDEN );
					break;
				}

			break;
		} // fim switch ( state )
	}


	private final void changeDrawables() {
		removeDrawable( 0 );

		if ( nextDrawable == null ) {
			setState( STATE_HIDDEN );
		} else {
			visibleTime = nextVisibleTime;
			
			setSize( nextDrawable.getSize() );
			ScreenManager.getInstance().setSoftKey( softkeyIndex, this );

			insertDrawable( nextDrawable );
			nextDrawable = null;
			
			setState( STATE_APPEARING );
		}
	}


	/**
	 * 
	 * @param newState
	 */
	public final void setState( byte newState ) {
		if ( activeDrawables > 0 ) {
			final Drawable d = getDrawable( 0 );

			switch ( newState ) {
				case STATE_HIDDEN:
					if ( nextDrawable != null ) {
						changeDrawables();
						return;
					}
					setVisible( false );
				break;

				case STATE_APPEARING:
					setVisible( !inTransition );

					switch ( direction ) {
						case DIRECTION_HORIZONTAL:
							switch ( softkeyIndex ) {
								case ScreenManager.SOFT_KEY_LEFT:
									speed.setSpeed( size.x * 1000 / transitionTime );
									d.setPosition( -size.x, 0 );
								break;

								case ScreenManager.SOFT_KEY_RIGHT:
									speed.setSpeed( -size.x * 1000 / transitionTime );
									d.setPosition( size.x, 0 );
								break;
							}
						break;

						case DIRECTION_VERTICAL:
							speed.setSpeed( -size.y * 1000 / transitionTime );
							d.setPosition( 0, size.y );
						break;
					}
				break;

				case STATE_SHOWN:
					d.setPosition( 0, 0 );
				break;

				case STATE_HIDING:
					switch ( direction ) {
						case DIRECTION_HORIZONTAL:
							switch ( softkeyIndex ) {
								case ScreenManager.SOFT_KEY_LEFT:
									speed.setSpeed( -size.x * 1000 / transitionTime );
								break;

								case ScreenManager.SOFT_KEY_RIGHT:
									speed.setSpeed( size.x * 1000 / transitionTime );
								break;
							}
						break;

						case DIRECTION_VERTICAL:
							speed.setSpeed( size.y * 1000 / transitionTime );
						break;
					}
				break;
			} // fim switch ( state )			
		}

		state = newState;
	}
	

	/**
	 * 
	 * @param inTransition
	 */
	public final void setInTransition( boolean inTransition ) {
		this.inTransition = inTransition;
		
		setVisible( !inTransition && state != STATE_HIDDEN );
	}


	public final boolean contains( int x, int y ) {
		return drawables[ 0 ] != null && super.contains( x, y );
	}


	/**
	 * 
	 * @param direction
	 */
	public final void setDirection( byte direction ) {
		this.direction = direction;

		setState( state );
	}


	/**
	 * 
	 * @param time
	 */
	public final void setTransitionTime( int time ) {
		transitionTime = ( short ) time;
	}
	

}
