/*
 * BasicSplashBrand.java
 * ©2007 Nano Games
 *
 * Created on October 3, 2007, 12:01 PM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;


/**
 *
 * @author peter
 */
public final class BasicSplashBrand extends DrawableGroup implements Updatable, KeyListener
{
	
	private static final byte TOTAL_ITEMS = 3;
	
	/** tempo total que a tela da marca é exibida */
	private int transitionTime = 3000;
	
	/** Índice da próxima tela a ser exibida. */
	private final int nextScreenIndex;
	

	/**
	 * Aloca uma nova tela básica de splash de marca.
	 * 
	 * @param nextScreenIndex índica da tela a ser exibida após a tela de splash.
	 * @param bkgColor cor sólida de preenchimento do fundo. Para não utilizar cor de preenchimento, basta passar valores
	 * negativos.
	 * @param splashPath caminho (diretório) da imagem e descritor da fonte utilizada nos créditos da marca. Nesse diretório
	 * é necessário que existam os seguintes arquivos:
	 * <ul>
	 * <li>font_credits.png</li>
	 * <li>font_credits.dat</li>
	 * </ul>
	 * @param logoPath caminho completo da imagem utilizada como logo da marca.
	 * @param creditsTextIndex índice do texto de créditos da marca.
	 * @throws java.lang.Exception
	 */
	public BasicSplashBrand( int nextScreenIndex, int bkgColor, String splashPath, String logoPath, int creditsTextIndex ) throws Exception {
		super( TOTAL_ITEMS );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		this.nextScreenIndex = nextScreenIndex;
		
		if ( bkgColor >= 0 ) {
			final Pattern bkg = new Pattern( null );
			bkg.setSize( size );
			bkg.setFillColor( bkgColor );
			insertDrawable( bkg );
		}
		
		final ImageFont font = ImageFont.createMultiSpacedFont( splashPath + "font_credits" );
		
		final DrawableImage logo = new DrawableImage( logoPath );
		logo.setPosition( ( size.x - logo.getWidth() ) >> 1, ( size.y - logo.getHeight() ) >> 1 );
		insertDrawable( logo );
		
		final RichLabel label = new RichLabel( font, AppMIDlet.getText( creditsTextIndex ) );
		label.setSize( size.x * 9 / 10, 0 );
		label.setSize( size.x * 9 / 10, label.getTextTotalHeight() );
		label.defineReferencePixel( label.getWidth() >> 1, label.getHeight() );
		label.setRefPixelPosition( size.x >> 1, size.y );
		
		insertDrawable( label );
		
		// ajusta a posição da logo, caso ela esteja sendo coberta pelo texto
		if ( logo.getPosY() + logo.getHeight() > label.getPosY() ) {
			// se a logo for maior que o espaço livre, apenas a posiciona no topo da tela
			if ( logo.getHeight() >= label.getPosY() )
				logo.setPosition( logo.getPosX(), 0 );
			else
				logo.setPosition( logo.getPosX(), ( label.getPosY() - logo.getHeight() ) >> 1 );
		}		
		
	}

	
	public final void update( int delta ) {
		transitionTime -= delta;
		
		if ( transitionTime <= 0 ) {
			transitionTime = Integer.MAX_VALUE;
			AppMIDlet.setScreen( nextScreenIndex );
		}
	}


	public void keyPressed( int key )
	{
		switch( key )
		{
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
				AppMIDlet.exit();
				break;

			// Os splashs não podem ser acelerados pelo usuário
			//#if DEBUG == "true"
//# 			default:
//# 				AppMIDlet.setScreen( nextScreenIndex );
			//#endif
		}
	}

	
	public final void keyReleased( int key ) {
	}
}
