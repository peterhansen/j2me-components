/**
 * CheckBox.java
 * 
 * Created on 6/Nov/2008, 17:16:15
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public class CheckBox extends Container {

	private boolean checked;

	private final FormMarqueeLabel label;

	private final Button button;


	public CheckBox( Button checkButton, ImageFont font, String text ) throws Exception {
		this( checkButton, font, text, false );
	}


	public CheckBox( Button checkButton, ImageFont font, String text, boolean checked ) throws Exception {
		this( checkButton, new FormMarqueeLabel( font, text ), checked );
	}


	public CheckBox( Button checkButton, FormMarqueeLabel label ) {
		this( checkButton, label, false );
	}


	public CheckBox( Button checkButton, final FormMarqueeLabel label, boolean checked ) {
		super( 2, new FlowLayout( FlowLayout.AXIS_HORIZONTAL, ANCHOR_VCENTER | ANCHOR_LEFT ) );

		// define o espaçamento padrão entre o botão e o label
		( ( FlowLayout ) getLayout() ).gap.x = 6;

		this.button = checkButton;
		button.setText( " X " );
		button.setText( "   ", false );
		insertDrawable( checkButton );
		
		this.label = label;
		// define o modo de scroll do texto
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		// a frequência só é ativada no caso de o botão estasr em foco
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_NONE );
		insertDrawable( label );

		setKeyListener( button );

		//#if TOUCH == "true"
//			setPointerListener( button );
//			label.setPointerListener( button );
		//#endif

		button.addEventListener( new EventListener() {
			public final void eventPerformed( Event evt ) {
				if ( evt.source == button ) {
					switch ( evt.eventType ) {
						case Event.EVT_BUTTON_CONFIRMED:
							if ( isEnabled() ) {
								setChecked( !isChecked() );
								evt.consume();
							}
						break;

						case Event.EVT_FOCUS_GAINED:
							// a frequência só é ativada no caso de o botão estar em foco
							label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
						break;

						case Event.EVT_FOCUS_LOST:
							// a frequência só é ativada no caso de o botão estar em foco
							label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_NONE );
							label.setTextOffset( 0 );
						break;
					}
				}
			}
		} );

		setChecked( checked );
	}
	
	
	public final void setChecked( boolean c ) {
		setChecked( c, true );
	}


	public void setChecked( boolean c, boolean dispatchEvent ) {
		if ( checked != c ) {
			checked = c;
			button.setText( c ? " X " : "", false );
			
			if ( dispatchEvent )
				dispatchActionEvent( new Event( this, Event.EVT_CHECKBOX_STATE_CHANGED ) );
		}
	}


	public boolean isChecked() {
		return checked;
	}

	
	public String getUIID() {
		return "checkbox";
	}


	public final Button getButton() {
		return button;
	}


	public final FormMarqueeLabel getLabel() {
		return label;
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		// TODO código necessário para evitar que texto fique sobre o botão (problema com FlowLayout, calcPreferredSize, etc...)
		int gapX = 0;
		if ( getLayout() instanceof FlowLayout )
			gapX = ( ( FlowLayout ) getLayout() ).gap.x;

		label.setSize( width - ( button.getPosX() + button.getWidth() + gapX ), label.getHeight() );
	}


}
