/**
 * Button.java
 * 
 * Created on 6/Nov/2008, 17:15:41
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;

/**
 *
 * @author Peter
 */
public class Button extends FormMarqueeLabel {

	/***/
	public static final byte STATE_DEFAULT	= 0;

	/***/
	public static final byte STATE_PRESSED	= 1;

	/***/
	public static final byte STATE_ROLLOVER = 2;

	/***/
	protected byte state;


	public Button( Button b ) throws Exception {
		this( b.label.getFont(), b.getText() );

		setScrollMode( b.getScrollMode() );
		setScrollFrequency( b.getScrollFrequency() );
		setFocusable( b.isFocusable() );
		setBorder( b.getBorder().getCopy() );
	}


	public Button( ImageFont font, String text ) throws Exception {
		super( font, text );

		// define o modo de scroll do texto
		setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		// a frequência só é ativada no caso de o botão estar em foco
		setScrollFrequency( MarqueeLabel.SCROLL_FREQ_NONE );

		setFocusable( true );
	}

	
	public String getUIID() {
		return "button";
	}


	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				if ( isEnabled() ) {
					setState( STATE_PRESSED );
				}
			break;

			default:
				dispatchActionEvent( new Event( this, Event.EVT_KEY_PRESSED, new Integer( key ) ) );
		}
	}


	public void keyReleased( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				if ( isEnabled() ) {
					setState( STATE_ROLLOVER );
					dispatchActionEvent( new Event( this, Event.EVT_BUTTON_CONFIRMED ) );
				}
			break;

			default:
				dispatchActionEvent( new Event( this, Event.EVT_KEY_RELEASED, new Integer( key ) ) );
		}
	}


//	public final boolean contains( int x, int y ) {
//		Border b = getBorder();
//
//		if( b != null )
//			return ( y > getBorder().getPosY() && y < getBorder().getPosY() + getBorder().getHeight() ) && ( x > getBorder().getPosX() && x < getBorder().getPosX() + getBorder().getWidth() );
//		else
//			return ( y > getPosY() && y < getPosY() + getHeight() ) && ( x > getPosX() && x < getPosX() + getWidth() );
//	}


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			if ( pointerListener == null ) {
				if ( isEnabled() ) {
					if ( contains( x, y ) ) {
						setState( STATE_PRESSED );
					} else {
						setState( STATE_ROLLOVER );
					}
				}
			} else {
				pointerListener.onPointerDragged( x - position.x, y - position.y );
			}
		}


		public void onPointerPressed( int x, int y ) {
			if ( pointerListener == null ) {
				if ( isEnabled() && contains( x, y ) ) {
					setState( STATE_PRESSED );
				}
			} else {
				pointerListener.onPointerPressed( x - position.x, y - position.y );
			}
		}


		public void onPointerReleased( int x, int y ) {
			if ( pointerListener == null ) {
				if ( isEnabled() ) {
					switch ( state ) {
						case STATE_PRESSED:
							setState( STATE_ROLLOVER );
							if ( contains( x, y ) ) {
								dispatchActionEvent( new Event( this, Event.EVT_BUTTON_CONFIRMED ) );
							}
						break;
					}
				}
			} else {
				pointerListener.onPointerReleased( x - position.x, y - position.y );
			}
		}
	//#endif


	public void setFocus( boolean focus ) {
		super.setFocus( focus );

		if ( focus ) {
			setState( STATE_ROLLOVER );
		} else {
			setState( STATE_DEFAULT );
		}
	}


	protected void setState( int state ) {
		this.state = ( byte ) state;

		if ( border != null ) {
			switch ( state ) {
				case STATE_DEFAULT:
					border.setState( Border.STATE_UNFOCUSED );
				break;

				case STATE_PRESSED:
					border.setState( Border.STATE_PRESSED );
				break;

				case STATE_ROLLOVER:
					border.setState( Border.STATE_FOCUSED );
				break;
			}
		}
	}


	public final byte getState() {
		return state;
	}


	public void setBorder( Border border, boolean fitSize ) {
		super.setBorder( border, fitSize );
		setState( state );
	}

}
