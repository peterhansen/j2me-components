/**
 * DrawableComponent.java
 * 
 * Created on 19/Dez/2008, 17:21:58
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.util.Point;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif
	

/**
 *
 * @author Peter
 */
public class DrawableComponent extends Component {

	public final Drawable d;
	
	/***/
	protected boolean resizeInternal;
	
	
	/**
	 * 
	 * @param d
	 */
	public DrawableComponent( Drawable d ) {
		this( d, true );
	}
	
	
	public DrawableComponent( Drawable d, boolean resizeInternal ) {
		super( 1, d.getSize() );
		
		this.d = d;
		
		setResizeInternal( resizeInternal );
		
		insertDrawable( d );

		if ( d instanceof KeyListener )
			setKeyListener( ( KeyListener ) d );

		//#if TOUCH == "true"
			if ( d instanceof PointerListener )
				setPointerListener( ( PointerListener ) d );
		//#endif

		setSize( d.getSize() );
	}
	

	public String getUIID() {
		return "drawablecomponent";
	}


	public Point calcPreferredSize( Point maximumSize ) {
		final Point borderSize = ( border == null ? new Point() : new Point( border.getBorderSize() ) );
		if ( maximumSize == null )
			return new Point( d.getWidth() + borderSize.x, d.getHeight() + borderSize.y );
		else
			return new Point( Math.min( d.getWidth() + borderSize.x, maximumSize.x ), Math.min( d.getHeight() + borderSize.y, maximumSize.y ) );
	}


	public void setSize( int width, int height ) {
		setSize( width, height, resizeInternal );
	}

	
	public void setSize( int width, int height, boolean resizeInternal ) {
		super.setSize( width, height );
		
		if ( resizeInternal ) {
			final Point borderSize = ( border == null ? new Point() : new Point( border.getBorderSize() ) );
			d.setSize( width - borderSize.x, height - borderSize.y );
		}
	}
	
	
	public final void setSize( Point size, boolean resizeInternal ) {
		setSize( size.x, size.y, resizeInternal );
	}
	
	
	/**
	 * Atualiza o tamanho do componente de forma a comportar o drawable interno. Se o componente tiver bordas,
	 * as dimensões da borda são automaticamente adicionadas às dimensões totais.
	 */
	public void resizeToFit() {
		if ( border == null )
			setSize( d.getSize() );
		else
			setSize( d.getSize().add( border.getBorderSize() ) );
		
		setAutoCalcPreferredSize( true );
	}


	public final boolean isResizeInternal() {
		return resizeInternal;
	}


	public void setResizeInternal( boolean resizeInternal ) {
		this.resizeInternal = resizeInternal;
	}

	
}
