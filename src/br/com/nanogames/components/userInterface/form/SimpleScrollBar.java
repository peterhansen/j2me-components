/**
 * SimpleScrollBar.java
 * 
 * Created on 11/Nov/2008, 15:05:42
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.Pattern;

/**
 *
 * @author Peter
 */
public class SimpleScrollBar extends ScrollBar {
	
	/** Dimensão mínima (altura ou largura) da barra de scroll. */
	public static final byte MIN_DIMENSION = 5;

	/***/
	public static final int COLOR_PAGE = 0x8888bb;

	/***/
	public static final int COLOR_FULL = 0xbbbbcc;
	

	public SimpleScrollBar( byte type ) throws Exception {
		super( 3, type );
		
		barFull = new Pattern( COLOR_FULL );
		insertDrawable( barFull );
		
		barPage = new Pattern( COLOR_PAGE );
		insertDrawable( barPage );
		
		switch ( type ) {
			case TYPE_HORIZONTAL:
				barPage.setSize( 0, 8 );
				barFull.setSize( 0, 8 );
			break;
			
			case TYPE_VERTICAL:
				barPage.setSize( 8, 0 );
				barFull.setSize( 8, 0 );
			break;
		}
		size.set( barFull.getWidth(), barFull.getHeight() );
	}
	

	/**
	 *
	 * @param currentValue
	 * @param currentPosition
	 * @param maxValue
	 */
	public void refreshScroll( int currentValue, int currentPosition, int maxValue ) {
		switch ( type ) {
			case TYPE_HORIZONTAL:
				barPage.setSize( Math.max( MIN_DIMENSION, ( currentValue * getWidth() / maxValue ) + 1 ), barPage.getHeight() );
				barPage.setPosition( currentPosition * getWidth() / maxValue, barPage.getPosY() );
			break;
			
			case TYPE_VERTICAL:
				barPage.setSize( barPage.getWidth(), Math.max( MIN_DIMENSION, ( currentValue * getHeight() / maxValue ) + 1 ) );
				barPage.setPosition( barPage.getPosX(), currentPosition * getHeight() / maxValue );
			break;
		}
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		switch ( type ) {
			case TYPE_HORIZONTAL:
				barPage.setSize( width, barPage.getHeight() );
				barFull.setSize( width, barFull.getHeight() );
			break;
			
			case TYPE_VERTICAL:
				barPage.setSize( barPage.getWidth(), height );
				barFull.setSize( barFull.getWidth(), height );
			break;
		}
	}
	
	
	public String getUIID() {
		return "simplescrollbar";
	}
	

}
