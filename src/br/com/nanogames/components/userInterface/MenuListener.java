/*
 * MenuListener.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 *
 * @author peter
 */
public interface MenuListener {
 
	
	/**
	 * Método chamado quando uma opção de um menu é selecionada.
	 * 
	 * @param menu referência para o menu cuja opção atual foi selecionada.
	 * @param id identificação do menu.
	 * @param index índice da opção selecionada.
	 */
	public abstract void onChoose( Menu menu, int id, int index );
	
	
	/**
	 * Método chamado quando o item selecionado atualmente no menu é trocado.
	 * 
	 * @param menu referência para o menu cujo item atual foi trocado.
	 * @param id identificação do menu.
	 * @param index índice do novo item.
	 */
	public abstract void onItemChanged( Menu menu, int id, int index );
	
	
}
 
