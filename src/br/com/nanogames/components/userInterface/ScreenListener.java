/*
 * ScreenListener.java
 *
 * Created on August 23, 2007, 3:02 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 * Interface que recebe eventos relativos à tela, repassados por ScreenManager.
 *
 * @author peter
 * @see ScreenManager
 * @see #hideNotify(boolean)
 * @see #showNotify(boolean)
 * @see #sizeChanged(int,int)
 */
public interface ScreenListener {

	/**
	 * Evento chamado quando o aparelho recebe um evento de suspend. É importante notar que, dependendo do aparelho,
	 * esse método não é chamado para todos os possíveis eventos (ligação, SMS, fechar flip, etc).
	 * @param deviceEvent indica se o evento foi gerado pelo aparelho, ou somente pelo ScreenManager. Caso tenha sido
	 * gerado pelo ScreenManager (deviceEvent = false), isso indica que a tela atual tornou-se o Drawable ativo. Caso
	 * contrário (deviceEvent = true), é algum evento capturado do aparelho (ligação, SMS, flip, slide, etc.)
	 *
	 * @see #showNotify(boolean)
	 */	
	public void hideNotify( boolean deviceEvent );
	
	
	/**
	 * Evento chamado quando o aparelho volta de um evento de suspend. É importante notar que, dependendo do aparelho,
	 * esse método não é chamado para todos os possíveis eventos (ligação, SMS, abrir flip, etc).
	 * @param deviceEvent indica se o evento foi gerado pelo aparelho, ou somente pelo ScreenManager. Caso tenha sido
	 * gerado pelo ScreenManager (deviceEvent = false), isso indica que a tela atual tornou-se o Drawable ativo. Caso
	 * contrário (deviceEvent = true), é algum evento capturado do aparelho (ligação, SMS, flip, slide, etc.)
	 *
	 * @see #hideNotify(boolean)
	 */	
	public void showNotify( boolean deviceEvent );
	
	
	/**
	 * Evento chamado quando as dimensões da tela sofrem alteração.
	 */
	public void sizeChanged( int width, int height );
	
}
